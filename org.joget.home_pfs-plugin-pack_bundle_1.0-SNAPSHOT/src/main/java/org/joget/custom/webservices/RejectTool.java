/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.util.List;
import java.util.Map;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;
import org.joget.plugin.base.DefaultApplicationPlugin;
import org.joget.workflow.model.WorkflowAssignment;
import org.joget.workflow.model.WorkflowVariable;
import org.joget.workflow.model.service.WorkflowManager;

/**
 *
 * @author Jahan Zaib
 */
public class RejectTool extends DefaultApplicationPlugin {

    @Override
    public Object execute(Map map) {
        WorkflowAssignment wfAssignment = (WorkflowAssignment) map.get("workflowAssignment");
        String activityId = wfAssignment.getActivityId();
        String processId = wfAssignment.getProcessId();
        String activityName = wfAssignment.getActivityName();
        System.out.println("" + wfAssignment.getActivityDefId());
        System.out.println("" + wfAssignment.getProcessDefId());

        System.out.println("" + wfAssignment.getParticipant());
        WorkflowManager wfManager = (WorkflowManager) AppUtil.getApplicationContext().getBean("workflowManager");

        String processName = wfAssignment.getProcessName();

        System.out.println("\n\n\nactivityId : " + activityId);
        System.out.println("processId : " + processId);
        System.out.println("processName : " + processName);
        System.out.println("activityName : " + activityName);

        List<WorkflowVariable> vars = wfAssignment.getProcessVariableList();

        for (WorkflowVariable var : vars) {
            System.out.println("" + var.getId() + " = " + var.getName() + " = " + var.getVal());
        }

        String status;
        String id;
        String contractId = null;
        String originalAmount = null;
        String total = null;
        String totalVO = "0";
        String projectId = null;
        String formId;
        String tableOrganize;
        String formIdOrganize;
        String type;
        String assignTo;
        String contractType = null;
        Boolean isContractAmendData = false;
        Boolean isContractAmendBOQ = false;

        String parentContractId = null;

        if (processName.contains("Process 1")) {
            contractType = AppUtil.processHashVariable("#form.pfs_contract.type#", wfAssignment, null, null);

            if ("amend_data".equalsIgnoreCase(contractType)) {
                parentContractId = AppUtil.processHashVariable("#form.pfs_contract.parent_contract_id#", wfAssignment, null, null);

                isContractAmendData = true;
            } else if ("amend_boq".equalsIgnoreCase(contractType)) {
                parentContractId = AppUtil.processHashVariable("#form.pfs_contract.parent_contract_id#", wfAssignment, null, null);

                isContractAmendBOQ = true;
            }
            status = AppUtil.processHashVariable("#form.pfs_contract.status#", wfAssignment, null, null);
            originalAmount = AppUtil.processHashVariable("#form.pfs_contract.original_amount#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_contract.project_id#", wfAssignment, null, null);
            contractId = id = AppUtil.processHashVariable("#form.pfs_contract.id#", wfAssignment, null, null);
            type = "contract";
            formId = "contractForm";
            tableOrganize = "app_fd_approval_cntrct_flow";

        } else if ("VO Process".equalsIgnoreCase(processName)) {
            status = AppUtil.processHashVariable("#form.pfs_vo.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_vo.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_vo.contract_id#", wfAssignment, null, null);
            total = AppUtil.processHashVariable("#form.pfs_vo.total#", wfAssignment, null, null);

            type = "vo";
            formId = "voForm";
            tableOrganize = "app_fd_approval_flow_vos";
        } else {
            status = AppUtil.processHashVariable("#form.pfs_claim.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_claim.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_claim.contract_id#", wfAssignment, null, null);
            total = AppUtil.processHashVariable("#form.pfs_claim.total#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_claim.project_id#", wfAssignment, null, null);
            totalVO = AppUtil.processHashVariable("#form.pfs_claim.total_vo_claim#", wfAssignment, null, null);

            type = "claim";
            formId = "claimForm";
            tableOrganize = "app_fd_approval_flow_claims";
        }

        System.out.println("Status : " + status);
        System.out.println("Id: " + id);
        System.out.println("Type: " + type);
        System.out.println("isContractAmendData: " + isContractAmendData);
        Commons common = new Commons();
        if ("contract".equals(type)) {
            if (!isContractAmendData) {
                if (projectId != null && originalAmount != null) {
                    String sql = "UPDATE app_fd_pfs_project "
                            + "SET c_contracted_amount = (c_contracted_amount+" + originalAmount + ") "
                            + "WHERE c_project_id= '" + projectId + "'";

                    //System.out.println(" SQL Update Project: " + sql);
                    //System.out.println(" Result : " + common.executeDatabaseQuery(sql));
                }

            }
            if (isContractAmendData) {

                System.out.println("contractId: " + contractId);
                FormRowSet rowSetContractOrignal = common.getFormRows("contractForm", "parent_contract_id", contractId);
                System.out.println("rowSetContractOrignal: " + rowSetContractOrignal.size());
                if (rowSetContractOrignal.size() > 0) {
                    FormRow rowContractUpdate = rowSetContractOrignal.get(0);

                    rowContractUpdate.setProperty("status", "complete");
                    rowContractUpdate.setProperty("status_integration", "complete");
                    System.out.println("Contract Amend Update : " + common.insertForm("contractForm", rowContractUpdate));

                }
                String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                        + "SET c_archive='true'"
                        + " WHERE id= '" + contractId + "'";
                System.out.println(" SQL Update Contract: " + sqlUpdateProject);

                System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));

            } else if (isContractAmendBOQ) {
                if (parentContractId != null) {
                    System.out.println("parentContractId: " + parentContractId);
                    FormRowSet rowSetContractOrignal = common.getFormRows("contractForm", "parent_contract_id", parentContractId);
                    System.out.println("rowSetContractOrignal: " + rowSetContractOrignal.size());
                    if (rowSetContractOrignal.size() > 0) {
                        FormRow rowContractUpdate = rowSetContractOrignal.get(0);

                        rowContractUpdate.setProperty("status", "complete");
                        rowContractUpdate.setProperty("status_integration", "complete");
                        System.out.println("Contract Amend Update : " + common.insertForm("contractForm", rowContractUpdate));

                    }
                    String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                            + "SET c_archive='true'"
                            + " WHERE id= '" + contractId + "'";
                    System.out.println(" SQL Update Contract: " + sqlUpdateProject);

                    System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
                }
            }
        } else if ("claim".equals(type)) {
            FormRowSet rowSetClaimBoQ = common.getFormRows("boqEditForm", "claim_id", id);
            for (FormRow rowClaimBoQ : rowSetClaimBoQ) {
                contractId = rowClaimBoQ.getProperty("contract_id");
                String itemId = rowClaimBoQ.getProperty("item_id");
                String quantity = rowClaimBoQ.getProperty("quantity");

                String sql = "UPDATE app_fd_pfs_boq "
                        + "SET c_quantity_available = (c_quantity_available+" + quantity + "), "
                        + "c_quantity_last_month = (c_quantity_last_month-" + quantity + ") "
                        + "WHERE c_contract_id='" + contractId + "' AND "
                        + "c_item_id= '" + itemId + "'";

                System.out.println(" SQL : " + sql);
                System.out.println(" Result : " + common.executeDatabaseQuery(sql));

            }

            // Update VO Boq
            FormRowSet rowSetClaimVoBoQ = common.getFormRows("boqClaimVoForm", "claim_id", id);
            for (FormRow rowClaimVOBoQ : rowSetClaimVoBoQ) {
                String voId = rowClaimVOBoQ.getProperty("vo_id");
                String itemId = rowClaimVOBoQ.getProperty("item_id");
                String quantity = rowClaimVOBoQ.getProperty("quantity");

                String sql = "UPDATE app_fd_pfs_boq_vo "
                        + "SET c_quantity_available = (c_quantity_available+" + quantity + "), "
                        + "c_quantity_last_month = (c_quantity_last_month-" + quantity + ") "
                        + "WHERE c_vo_id='" + voId + "' AND "
                        + "c_item_id= '" + itemId + "'";

                System.out.println(" SQL VO: " + sql);
                System.out.println(" Result : " + common.executeDatabaseQuery(sql));

            }

            if (contractId != null && total != null) {
                if (totalVO == null || totalVO.isEmpty()) {
                    totalVO = "0";
                }
                String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                        + "SET c_total_claim_amount = (c_total_claim_amount-" + total + ") "
                        + ", c_total_vo_amount = (c_total_vo_amount-" + totalVO + ") "
                        + "WHERE id= '" + contractId + "'";

                System.out.println(" SQL Update Contract Claim: " + sqlUpdateProject);
                System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
            }
        } else if ("vo".equals(type)) {
            if (contractId != null && total != null) {
                String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                        + "SET c_total_vo_amount = (c_total_vo_amount-" + total + ") "
                        + "WHERE id= '" + contractId + "'";

                System.out.println(" SQL Update Contract VO: " + sqlUpdateProject);
                System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
            }

        }
        return null;
    }

    public String getName() {
        return "Reject Tool";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "Reject Tool;";
    }

    public String getLabel() {
        return "Reject Tool";
    }

    public String getClassName() {
        return this.getClass().getName();
    }

    public String getPropertyOptions() {
        return AppUtil.readPluginResource(getClass().getName(), "/properties/WizardWebServiceTool.json", null, true, "message/WizardWebServiceTool");
    }
}
