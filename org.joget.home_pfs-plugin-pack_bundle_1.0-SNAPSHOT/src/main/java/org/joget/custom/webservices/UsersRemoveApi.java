/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.*;
import java.io.IOException;
import org.joget.common.Commons;

/**
 *
 * @author User
 */
public class UsersRemoveApi extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "Users Remove Api Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "Users Remove Api Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "Users Remove Api Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        performOperation(request, response);

    }

    private void performOperation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (PrintWriter out = response.getWriter()) {
            JSONObject responseObject = new JSONObject();

            /*String authHeader = request.getHeader(AUTHORIZATION_HEADER_NAME);
            
            if (authHeader != null) {
                authHeader = authHeader.replaceFirst(BASIC_AUTHENTICATION_REGEX, EMPTY_STRING);
                authHeader = new String(Base64.getDecoder().decode(authHeader));

                if (StringUtils.countMatches(authHeader, SEPARATOR) != 1) {
                    {
                        responseObject.put("status", false);
                        responseObject.put("message", "API Login Fail");
                    }
                    out.println(responseObject);
                    return;
                }
            } else {
                {
                    responseObject.put("status", false);
                    responseObject.put("message", "API Login Fail");
                }
                out.println(responseObject);
                return;
            }*/
            StringBuilder jb = new StringBuilder();

            String line = null;
            try {
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null) {
                    jb.append(line);
                }
            } catch (Exception ex) {
                Logger.getLogger(UsersRemoveApi.class.getName()).log(Level.SEVERE, null, ex);
                {
                    responseObject.put("status", false);
                    responseObject.put("message", "Invalid JSON or Request");
                }
                out.println(responseObject);
            }
            Logger.getLogger(UsersRemoveApi.class.getName()).log(Level.INFO, "********************{0}", jb.toString());

            JSONObject jsonObject = new JSONObject(jb.toString());

            JSONArray usersJsonArray = null;

            Commons comm = new Commons();
            String requestIncompleteMessage = null;

            if (jsonObject.has("users")) {
                usersJsonArray = jsonObject.getJSONArray("users");
            } else {
                requestIncompleteMessage = "users array is required";
            }

            JSONArray responseArray = new JSONArray();

            if (requestIncompleteMessage == null && usersJsonArray != null) {

                for (int i = 0; i < usersJsonArray.length(); i++) {
                    JSONObject responseObjectArray = new JSONObject();

                    JSONObject jsonUserObject = usersJsonArray.getJSONObject(i);
                    String userName = jsonUserObject.getString("userName");

                    Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.INFO, "username : {0}", userName);
                    if (comm.removeUser(userName)) {
                        responseObjectArray.put("userStatus", true);

                    } else {
                        responseObjectArray.put("userStatus", false);

                    }
                    responseObjectArray.put("userName", userName);

                    responseArray.put(responseObjectArray);
                }
                responseObject.put("status", true);
            } else {
                responseObject.put("status", false);
                responseObject.put("message", requestIncompleteMessage);
            }
            responseObject.put("users", responseArray);
            out.println(responseObject);

        } catch (JSONException ex) {

            Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
