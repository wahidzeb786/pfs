/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.BOQObject;
import org.joget.common.CSVHelper;
import org.joget.common.Commons;
import org.joget.commons.util.SetupManager;
import org.joget.plugin.base.DefaultApplicationPlugin;
import org.joget.plugin.base.PluginManager;

/**
 *
 * @author Jahan Zaib
 */
public class CSVProcessorTool extends DefaultApplicationPlugin {

    @Override
    public Object execute(Map map) {

        PluginManager pluginManager = (PluginManager) map.get("pluginManager");

        String formDataTable = (String) map.get("formDataTable");
        String formDefId = (String) map.get("formDefId");
        String recordId = (String) map.get("recordId");

        Iterator iter = map.keySet().iterator();
        while (iter.hasNext()) {
            System.out.println("-->" + iter.next());
        }

        System.out.println("RecordId : " + recordId);
        Commons common = new Commons();
        FormRowSet rowSet = common.getFormRows("contractAmendFullForm", "id", recordId);
        if (rowSet.size() > 0) {
            FormRow row = rowSet.get(0);

            String boqFile = row.getProperty("boq_file_amend");
            System.out.println("boq_file_amend : " + boqFile);
            if (boqFile != null) {
                String absolutePath = SetupManager.getBaseDirectory()
                        + "/app_formuploads/pfs_contract/" + "/" + recordId + "/" + boqFile;

                CSVHelper csvHelper = new CSVHelper(absolutePath);
                ArrayList<BOQObject> list = csvHelper.getCSVToObject();

                System.out.println("" + list.toString());
                for (BOQObject boq : list) {
                    if (!boq.getId().isEmpty()) {
                        FormRowSet rowSetBoq = common.getFormRows("boqForm", "id", boq.getId());
                        if (rowSetBoq.size() > 0) {
                            FormRow rowBoq = rowSetBoq.get(0);
                            String unitPrice = rowBoq.getProperty("unit_price");
                            rowBoq.setProperty("unit_price", boq.getPrice());
                            rowBoq.setProperty("old_unit_price", unitPrice);
                            common.insertForm("boqForm", rowBoq);
                        }
                    }
                }
            }
        }
        return null;
    }

    public String getName() {
        return "CSV Processor Tool";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "CSV Processor Tool";
    }

    public String getLabel() {
        return "CSV Processor Tool";
    }

    public String getClassName() {
        return this.getClass().getName();
    }

    public String getPropertyOptions() {
        return AppUtil.readPluginResource(getClass().getName(), "/properties/WizardWebServiceTool.json", null, true, "message/WizardWebServiceTool");
    }
}
