/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.apps.form.model.Element;
import org.joget.apps.form.model.FormData;
import org.joget.common.Commons;
import org.joget.commons.util.SetupManager;
import org.joget.plugin.base.PluginWebSupport;

/**
 *
 * @author fahmi
 */
public class DownloadCustomFile extends Element implements PluginWebSupport {

    //private static final Logger logger = Logger.getLogger(DownloadCustomFile.class.getName());
    @Override
    public String renderTemplate(FormData formData, Map dataModel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getName() {
        return " Download Custom File";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String getDescription() {
        return " Download Custom File";
    }

    @Override
    public String getLabel() {
        return "Download Custom File";
    }

    @Override
    public String getClassName() {
        return getClass().getName();
    }

    @Override
    public String getPropertyOptions() {
        return null;
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Commons common = new Commons();

        if (common.performAPIAuthorization(request)) {//|| common.performAuthenticate(request)) {

        } else {
            //response.sendError(401);
            //return;
        }
        String id = request.getParameter("id");
        String formId = request.getParameter("formId");
        String fileName = request.getParameter("fileName");

        if (!"".equals(formId) && !"".equals(fileName) && !"".equals(id)) {

            String tableName = common.getFormTableName(formId);

            // reads input file from an absolute path
            String absolutePath = SetupManager.getBaseDirectory()
                    + "/app_formuploads/" + tableName + "/" + "/" + id + "/" + fileName;

            File downloadFile = new File(absolutePath);
            OutputStream outStream;
            try (FileInputStream inStream = new FileInputStream(downloadFile)) {
                FileNameMap fileNameMap = URLConnection.getFileNameMap();
                String mimeType = fileNameMap.getContentTypeFor(absolutePath);
                if (mimeType == null) {
                    // set to binary type if MIME mapping not found
                    mimeType = "application/octet-stream";
                }
                System.out.println("MIME type: " + mimeType);
                // modifies response
                response.setContentType(mimeType);
                response.setContentLength((int) downloadFile.length());
                // forces download
                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
                response.setHeader(headerKey, headerValue);
                // obtains response's output stream
                outStream = response.getOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                while ((bytesRead = inStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }
            }
            outStream.close();
        }
    }
}
