/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.joget.workflow.model.WorkflowAssignment;
import org.joget.apps.app.service.AppUtil;
import org.joget.common.Commons;
import org.joget.directory.model.User;

/**
 *
 * @author Asyraf
 * @Changes Jahan Zaib Yousaf Sheikh (wahid.zeb@gmail.com)
 */
public class CustomEmail {

    private String emailType;
    private String status;
    private String emailTemplate;
    private WorkflowAssignment wfAssignment;
    private User recipient;


    /*
        Main Type(as in RI) 
            - Contract
                - ContractApproval
                - ContractReject
                - ContractApprovedReject
                - ContractApproved
                - ContractInfo
            - Claim 
                - ClaimApproval
                - ClaimReject
                - ClaimApprovedReject
                - ClaimApproved
                - ClaimInfo
            - VO 
                - VOApproval
                - VOReject
                - VOApprovedReject
                - VOApproved
                - VOInfo
     */
    public void SendCustomEmail(String userId, String email, String type, String template,
            WorkflowAssignment wfAssignment, String projectId, boolean amend, String status) {
        SendCustomEmail(userId, email, type, template, wfAssignment, projectId, amend, status, false);
    }

    public void SendCustomEmail(String userId, String email, String type, String template,
            WorkflowAssignment wfAssignment, String projectId, boolean amend, String status, boolean skipURL) {
        System.out.println("\n\n\n---> Sending to " + userId + "<----\n\n\n");

        if (email == null && email.isEmpty()) {
            Logger.getLogger(CustomEmail.class.getName()).log(Level.INFO, "Empty Email Address");
            return;
        }

        if (type == null && type.isEmpty()) {
            Logger.getLogger(CustomEmail.class.getName()).log(Level.INFO, "Empty Email Type");
            return;
        }

        if (template == null && template.isEmpty()) {
            Logger.getLogger(CustomEmail.class.getName()).log(Level.INFO, "Empty Template Name");
            return;
        }

        // Commons common = new Commons();
        Commons commons = new Commons();
        this.recipient = commons.getUser(userId);
        this.status = status;
        this.emailType = (type.equalsIgnoreCase("vo")) ? "VO" : type.substring(0, 1).toUpperCase() + type.substring(1);
        this.emailTemplate = this.emailType + template;
        this.wfAssignment = wfAssignment;

        String subject = getEmailSubject();
        String message = getEmailBody();

        String lineSeparator = System.getProperty("line.separator");
        //Logger.getLogger(CustomEmail.class.getName()).log(Level.INFO, "========================== Email ============================ {0}subject ----> {1}{2} Message ----> {3}{4}======================== End Email ==========================", new Object[]{lineSeparator, subject, lineSeparator, message, lineSeparator});

        // LogUtil.info("Send Email to : ", email);
        // LogUtil.info("Subject : ", subject);
        // LogUtil.info("Message : ", message);
        // sending the email
        // Commons.sendEmail("muhammad.asyraf@reveronconsulting.com", subject, message); // for testing purpose, will send to one email
        String url = "";
        //if (!"reject".equals(subject)) {
        switch (type) {
            case "contract":
                if (amend == true) {
                    url = "/contract_amend_data_inbox?_mode=assignment&embed=true&activityId=";
                } else {
                    url = "/contract_inbox?_mode=assignment&embed=true&activityId=";
                }
                break;
            case "claim":
                url = "/claim_inbox?_mode=assignment&embed=true&activityId=";
                break;
            case "vo":
                url = "/vo_inbox?_mode=assignment&embed=true&activityId=";
                break;
            default:
                url = "";
                break;
        }
        if (!"".equals(url)) {
            url += getNextActivityId();
        }
        //}
        if (skipURL) {
            url = "";
        }
        commons.sendNotification(userId, email, subject, message, type, url, projectId, status);
    }

    private String getEmailSubject() {
        String emailSubjectEnvVar = "emailSubject" + this.emailTemplate;
        String emailSubject = getEmailContent(emailSubjectEnvVar);

        return emailSubject;
    }

    private String getEmailBody() {
        String emailBodyEnvVar = "emailBody" + this.emailTemplate;
        String emailBody = getEmailContent(emailBodyEnvVar);

        return emailBody;
    }

    private String getEmailContent(String envVarId) {
        // String emailContentRaw = AppUtil.processHashVariable("#envVariable."+envVarId+"#", this.wfAssignment, null, null);
        Commons commons = new Commons();

        //Logger.getLogger(CustomEmail.class.getName()).log(Level.INFO, "envVarId :{0}", envVarId);
        String emailContentRaw = commons.getEnvVariables("pfs", envVarId);
        String emailContent = processParameter(emailContentRaw);

        return emailContent;
    }

    private String processParameter(String str) {
        String envRiLink = getRiLink();
        String activityId = getNextActivityId();
        String projectIdNo = getProjectIDNumber();
        String processId = this.wfAssignment.getProcessId();
        String process = this.emailType;
        String recordId = getRecordId();
        String recipientName = this.recipient.getFirstName() + " " + this.recipient.getLastName();

        String ret = str.replace("{ENV_RI_LINK}", envRiLink).replace("{ACTIVITY_ID}", activityId).replace("{PROJECT_ID}", projectIdNo).replace("{PROCESS_ID}", processId).replace("{ACTIVITY}", process).replace("{RECORD_ID}", recordId).replace("{RECIPIENT}", recipientName);
        // need to process first hash variable for mobile notification
        return AppUtil.processHashVariable(ret, wfAssignment, null, null);
    }

    private String getProjectIDNumber() {
        String projectId = "";
        switch (this.emailType) {
            case "Contract":
                projectId = AppUtil.processHashVariable("#form.pfs_contract.project_id#", this.wfAssignment, null, null);
                break;
            case "Claim":
                projectId = AppUtil.processHashVariable("#form.pfs_claim.project_id#", this.wfAssignment, null, null);
                break;
            case "VO":
                projectId = AppUtil.processHashVariable("#form.pfs_vo.project_id#", this.wfAssignment, null, null);
                break;
        }

        String ret = "";
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_project_id_number from app_fd_pfs_project where id = '" + projectId + "'";
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    if (rSet.next()) {
                        ret = rSet.getString("c_project_id_number");
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    private String getRecordId() {
        String recordId = "";
        switch (this.emailType) {
            case "Contract":
                recordId = AppUtil.processHashVariable("#form.pfs_contract.id#", this.wfAssignment, null, null);
                break;
            case "Claim":
                recordId = AppUtil.processHashVariable("#form.pfs_claim.id#", this.wfAssignment, null, null);
                break;
            case "VO":
                recordId = AppUtil.processHashVariable("#form.pfs_vo.id#", this.wfAssignment, null, null);
                break;
        }
        return recordId;
    }

    private String getNextActivityId() {
        // might break if flow change
        String curr = this.wfAssignment.getActivityId();
        String activityName = this.wfAssignment.getActivityName();
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Current Activity Id: {0}", curr);
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Current Activity: {0}", activityName);

        String[] split = curr.split("_");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < split.length; i++) {
            if (i == 0) {
                if ("info".equals(status)) {
                    sb.append(Integer.parseInt(split[i]) + 2);
                } else {
                    if ("WorkFlow Brain".equals(activityName)) {
                        sb.append(Integer.parseInt(split[i]) + 3);
                    } else {
                        sb.append(Integer.parseInt(split[i]) + 1);
                    }
                }
                sb.append("_");
            } else if (i == split.length - 1) {
                if ("info".equals(status)) {
                    sb.append("activityMoreInfoNeeded");
                } else {
                    sb.append("activityForm");
                }
            } else {
                sb.append(split[i]);
                sb.append("_");
            }
        }
        return sb.toString();
    }

    private String getRiLink() {
        // String link = AppUtil.processHashVariable("#envVariable.ri_link#", this.wfAssignment, null, null);
        Commons commons = new Commons();
        String link = commons.getEnvVariables("pfs", "ri_link");
        return link;
    }
}
