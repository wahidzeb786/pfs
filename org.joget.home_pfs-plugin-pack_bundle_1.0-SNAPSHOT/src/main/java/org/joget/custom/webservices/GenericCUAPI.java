/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.apps.app.dao.FormDefinitionDao;
import org.joget.apps.app.model.AppDefinition;
import org.joget.apps.app.model.FormDefinition;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.dao.FormDataDao;
import org.joget.apps.form.model.Form;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.service.FormService;
import org.joget.common.Commons;
import org.joget.commons.util.SetupManager;
import org.joget.commons.util.UuidGenerator;

import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * /**
 *
 * @author User
 */
public class GenericCUAPI extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "Generic CU Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "Generic CU Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "Generic CU Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Commons common = new Commons();

        PrintWriter out = response.getWriter();

        if (common.performAPIAuthorization(request)) {//|| common.performAuthenticate(request)) {

        } else {
            response.sendError(401);
            return;
        }

        String formId = request.getParameter("formId");
        StringBuilder jb = new StringBuilder();

        String line = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null) {
                jb.append(line);
            }
        } catch (IOException e) {
            /*report an error*/
            System.out.println(" Some Error in parsing json" + jb.toString());
            out.println("{\"status\":\"Invalid JSON\"}");
        }

        try {
            JSONObject responseObject = new JSONObject();

            Logger.getLogger(GenericCUAPI.class.getName()).log(Level.INFO, "\n\n\n****form {0}****\npayload{1}****\n\n\n", new Object[]{formId, jb.toString()});

            JSONObject jsonObject = new JSONObject(jb.toString());
            FormRow row = new FormRow();
            String id;
            if (jsonObject.has("id")) {
                id = jsonObject.getString("id");
            } else {
                UuidGenerator uuid = UuidGenerator.getInstance();
                id = uuid.getUuid();

            }
            row.setId(id);

            Iterator<String> iter = jsonObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                if (!"id".equalsIgnoreCase(key)) {
                    row.put(key, jsonObject.getString(key));
                }
            }

            if (jsonObject.has("images")) {
                JSONArray imageArray = jsonObject.getJSONArray("images");
                for (int i = 0; i < imageArray.length(); i++) {
                    JSONObject imageObject = imageArray.getJSONObject(i);
                    String name = imageObject.getString("name");
                    String bson = imageObject.getString("base64");
                    dumpImage(getFormTableName(formId), id, bson, name);
                }
            }

            if ("userNotificationForm".equals(formId)) {
                //common.getFormRows(formId, "fcm", row.getProperty("fcm"));
                common.delateFormData(formId, "fcm", row.getProperty("fcm"));
            }
            if ("projectForm".equals(formId)) {
                String contractee = row.getProperty("contractee");
                String contractor = row.getProperty("contractor");
                String consultant = row.getProperty("consultant");
                if (contractee != null && !contractee.isEmpty()) {
                    String contracteeDisplayName = common.getOrganizationName(contractee);

                    if (!"".equals(contracteeDisplayName)) {
                        row.put("contractee_name", contracteeDisplayName);
                    }
                }

                if (contractor != null && !contractor.isEmpty()) {
                    String contractorDisplayName = common.getOrganizationName(contractor);

                    if (!"".equals(contractorDisplayName)) {
                        row.put("contractor_name", contractorDisplayName);
                    }
                }

                if (consultant != null && !consultant.isEmpty()) {
                    String consultantDisplayName = common.getOrganizationName(consultant);

                    if (!"".equals(consultantDisplayName)) {
                        row.put("consultant_name", consultantDisplayName);
                    }
                }
            }
            responseObject.put("status", common.insertForm(formId, row));
            responseObject.put("id", id);
            out.print(responseObject);

        } catch (JSONException ex) {
            Logger.getLogger(GenericCUAPI.class.getName()).log(Level.INFO, jb.toString());

            Logger.getLogger(GenericCUAPI.class.getName()).log(Level.SEVERE, null, ex);
            out.println("{\"status\":\"Invalid JSON\"}");
        }
    }

    public static byte[] decodeImage(String imageDataString) {
        return Base64.decodeBase64(imageDataString);
    }

    private void dumpImage(String tableName, String pId, String imageStringValue, String fileName) throws IOException {

        String table = tableName.replaceFirst("app_fd_", "").trim();
        // Converting a Base64 String into Image byte array
        byte[] imageByteArray = decodeImage(imageStringValue);

        // Write a image byte array into file system
        String absolutePath = SetupManager.getBaseDirectory() + "/app_formuploads/" + table + File.separator + pId;
        System.out.println(" Path " + absolutePath);

        File file = new File(absolutePath);
        if (!file.exists()) {
            if (file.mkdirs()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }

        absolutePath += File.separator + fileName;

        try (FileOutputStream imageOutFile = new FileOutputStream(absolutePath)) {
            imageOutFile.write(imageByteArray);
        }

    }

    String getFormTableName(String formId) {
        AppDefinition appDef = AppUtil.getCurrentAppDefinition();
        FormDefinitionDao formDefinitionDao = (FormDefinitionDao) AppUtil.getApplicationContext().getBean("formDefinitionDao");
        FormService formService = (FormService) AppUtil.getApplicationContext().getBean("formService");
        FormDataDao formDataDao = (FormDataDao) AppUtil.getApplicationContext().getBean("formDataDao");

        //ListService listService = (ListService) AppUtil.getApplicationContext().getBean("listService");
        List<String> columnNames = null;

        FormDefinition formDef = formDefinitionDao.loadById(formId, appDef);
        Form form = null;
        if (formDef != null) {
            String formJson = formDef.getJson();

            if (formJson != null) {
                form = (Form) formService.createElementFromJson(formJson);
            }

            if (form != null) {
                return formDataDao.getFormTableName(form).replaceFirst("app_fd_", "").trim();
            }
        }
        return null;
    }
}
