/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.apps.app.dao.DatalistDefinitionDao;
import org.joget.apps.app.dao.EnvironmentVariableDao;
import org.joget.apps.app.dao.FormDefinitionDao;
import org.joget.apps.app.model.AppDefinition;
import org.joget.apps.app.model.DatalistDefinition;
import org.joget.apps.app.model.EnvironmentVariable;
import org.joget.apps.app.model.FormDefinition;
import org.joget.apps.app.service.AppService;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.datalist.model.DataList;
import org.joget.apps.datalist.model.DataListCollection;
import org.joget.apps.datalist.model.DataListColumn;
import org.joget.apps.datalist.model.DataListFilter;
import org.joget.apps.datalist.model.DataListFilterQueryObject;
import org.joget.apps.datalist.service.DataListService;
import org.joget.apps.form.dao.FormDataDao;
import org.joget.apps.form.model.Element;
import org.joget.apps.form.model.Form;
import org.joget.apps.form.model.FormContainer;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.service.FormService;
import org.joget.apps.form.service.FormUtil;
import org.joget.apps.list.service.ListService;
import org.joget.common.Commons;
import org.joget.commons.util.LogUtil;
import org.joget.directory.model.User;
import org.joget.directory.model.service.ExtDirectoryManager;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author
 */
public class JsonApiPlugin extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "List JSON API Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "List JSON API Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "List Data Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Commons common = new Commons();

        if (common.performAPIAuthorization(request)) {//|| common.performAuthenticate(request)) {

        } else {
            //   response.sendError(401);
            //   return;
        }
        String action = request.getParameter("action");
        Date date = new Date(System.currentTimeMillis());
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
        String dateFormatted = formatter.format(date);
        System.out.println(" ---> " + action + "  : " + dateFormatted);
        // response.getWriter().println(" ---> " + action);

        if ("userList".equals(action)) {
            String orgId = request.getParameter("orgId");
            System.out.println(" ---> " + orgId);
            handleUserList(response.getWriter(), orgId);
        } else if ("formFields".equals(action)) {
            String formId = request.getParameter("formId");
            System.out.println(" ---> " + formId);
            handleFormFields(response.getWriter(), formId);
        } else if ("list".equals(action)) {
            try {
                String listId = request.getParameter("listId");
                String appId = request.getParameter("appId");
                String filter1Column = request.getParameter("filter1Column");
                String filter1Value = request.getParameter("filter1Value");
                String filter2Column = request.getParameter("filter2Column");
                String filter2Value = request.getParameter("filter2Value");
                String filter3Column = request.getParameter("filter3Column");
                String filter3Value = request.getParameter("filter3Value");
                String filter1NotIn = request.getParameter("filter1NotIn");
                String filter1In = request.getParameter("filter1In");
                String filter1Like = request.getParameter("filter1Like");
                if ("true".equalsIgnoreCase(filter1Like)) {
                    filter1Value = "%" + filter1Value + "%";
                }
                boolean filter1InCheck = false;
                if (filter1In != null) {
                    if (filter1In.equalsIgnoreCase("true")) {
                        filter1InCheck = true;
                    }
                }

                boolean filter1NotInCheck = false;
                if (filter1NotIn != null) {
                    if (filter1NotIn.equalsIgnoreCase("true")) {
                        filter1NotInCheck = true;
                    }
                }

                String filter2NotIn = request.getParameter("filter2NotIn");
                boolean filter2NotInCheck = false;
                if (filter2NotIn != null) {
                    if (filter2NotIn.equalsIgnoreCase("true")) {
                        filter2NotInCheck = true;
                    }
                }

                String filter3NotIn = request.getParameter("filter3NotIn");
                boolean filter3NotInCheck = false;
                if (filter3NotIn != null) {
                    if (filter3NotIn.equalsIgnoreCase("true")) {
                        filter3NotInCheck = true;
                    }
                }
                String imageUrl = request.getParameter("imageUrl");
                String formId = request.getParameter("formId");
                String start = request.getParameter("start");
                String end = request.getParameter("recotd");

                String ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");

                if (ipAddress == null) {
                    ipAddress = request.getRemoteAddr();
                }
                System.out.println(" ListName " + listId + "\t App Name " + appId + "\t IP" + ipAddress);

                viewDataList(response.getWriter(), listId, appId, filter1Column, filter1Value, filter2Column, filter2Value, filter3Column, filter3Value, imageUrl, start, end, filter1NotInCheck, filter2NotInCheck, filter3NotInCheck, filter1InCheck);
            } catch (Exception ex) {
                response.getWriter().println(" ---> " + ex.getMessage());
            }
        }
    }

    private void handleUserList(Writer writer, String orgId) {
        if ("null".equals(orgId) || "".equals(orgId)) {
            orgId = null;
        }

        try {
            JSONArray jsonArray = new JSONArray();

            ApplicationContext ac = AppUtil.getApplicationContext();
            ExtDirectoryManager directoryManager = (ExtDirectoryManager) ac.getBean("directoryManager");

            Collection<User> userList = directoryManager.getUsers(null, orgId, null, null, null, null, null, "firstName", false, null, null);

            for (User u : userList) {
                Map<String, String> option = new HashMap<String, String>();
                option.put("value", u.getUsername());
                option.put("label", u.getFirstName() + " " + u.getLastName() + "(" + u.getUsername() + ")");
                jsonArray.put(option);
            }

            jsonArray.write(writer);
        } catch (Exception ex) {
            LogUtil.error(this.getClass().getName(), ex, "Get User List Error!");
        }
    }

    private void viewDataList(Writer writer, String listId, String appId, String filter1Column,
            String filter1Value, String filter2Column, String filter2Value, String filter3Column,
            String filter3Value, String imageUrlReq, String start, String end, boolean filter1NotInCheck,
            boolean filter2NotInCheck, boolean filter3NotInCheck, boolean filter1InCheck) throws Exception {
        AppService appService = (AppService) AppUtil.getApplicationContext().getBean("appService");
        DataListService dataListService = (DataListService) AppUtil.getApplicationContext().getBean("dataListService");
        DatalistDefinitionDao datalistDefinitionDao = (DatalistDefinitionDao) AppUtil.getApplicationContext().getBean("datalistDefinitionDao");

        boolean getImageUrl = false;
        if (imageUrlReq != null) {
            if (imageUrlReq.equalsIgnoreCase("yes")) {
                getImageUrl = true;
            }
        }

        Long appVersion = appService.getPublishedVersion(appId);
        //writer.write(" " + appVersion);
        AppDefinition appDef = appService.getAppDefinition(appId, appVersion.toString());
        if (appDef == null) {
            writer.write(" app def null " + appVersion);
        }

        DatalistDefinition datalistDefinition = datalistDefinitionDao.loadById(listId, appDef);

        if (datalistDefinition != null) {
            //retrieve the datalist object
            DataList list = dataListService.fromJson(datalistDefinition.getJson());

            JSONObject object = new JSONObject(datalistDefinition.getJson());
            Map<String, Object> mapBinder = list.getBinder().getProperties();

            String formDefId = null;
            ArrayList<String> listImageFields = new ArrayList<String>();
            if (mapBinder.containsKey("formDefId") && getImageUrl) {
                formDefId = (String) mapBinder.get("formDefId");
                listImageFields = getFormImageFields(formDefId);
            }

            //  DataListColumn[] cols = list.getColumns();
            DataListFilterQueryObject queryObject = new DataListFilterQueryObject();
            //String value = "s";
            //String name = "accountName";
            boolean filter1 = false, filter2 = false, filter3 = false;
            if (list != null && list.getBinder() != null && filter1Column != null && filter1Value != null) {
                String query = "";
                if (!filter1Column.isEmpty()) {
                    filter1 = true;
                }

                if (filter2Column != null && !filter2Column.isEmpty()) {
                    filter2 = true;
                }
                if (filter3Column != null && !filter3Column.isEmpty()) {
                    filter3 = true;
                }

                if (filter1) {
                    if (filter1NotInCheck) {
                        query = "lower(" + list.getBinder().getColumnName(filter1Column) + ") NOT like lower(?)";

                    }
                    if (filter1InCheck) {
                        for (int k = 0; k < filter1Value.split(";").length; k++) {
                            if (k == 0) {
                                query = "( lower(" + list.getBinder().getColumnName(filter1Column) + ") like lower(?)";
                            } else {
                                query += " OR lower(" + list.getBinder().getColumnName(filter1Column) + ") like lower(?)";
                            }
                        }
                        query += ")";
                    } else {
                        query = "lower(" + list.getBinder().getColumnName(filter1Column) + ") like lower(?)";
                    }
                }
                if (filter2) {
                    if (filter2NotInCheck) {
                        query += " AND lower(" + list.getBinder().getColumnName(filter2Column) + ") NOT like lower(?)";

                    } else {
                        query += " AND lower(" + list.getBinder().getColumnName(filter2Column) + ") like lower(?)";
                    }
                }
                if (filter3) {
                    if (filter3NotInCheck) {
                        query += " AND lower(" + list.getBinder().getColumnName(filter3Column) + ") NOT like lower(?)";

                    } else {
                        query += " AND lower(" + list.getBinder().getColumnName(filter3Column) + ") like lower(?)";
                    }
                }
                //System.out.println("" + query);
                //queryObject.setQuery("lower(" + list.getBinder().getColumnName(name) + ") like lower(?) and " + list.getBinder().getColumnName("address") + " like lower(?) ");
                if (filter1) {
                    queryObject.setQuery(query);
                }
                if (filter3) {
                    queryObject.setValues(new String[]{filter1Value, filter2Value, filter3Value});
                } else if (filter2) {
                    if (filter1InCheck) {
                        String[] theArray = filter1Value.split(";");
                        //int i = theArray.length;
                        //int n = ++i;

                        //String[] newArray = new String[n];
                        //System.out.println("1 " + theArray.length);
                        List<String> a = new ArrayList<String>();
                        a.addAll(Arrays.asList(theArray));
                        //System.out.println("2 " + a.size() + " " + a.toString());
                        //System.out.println("2a " + filter2Value);
                        a.add(filter2Value.toString());
                        //System.out.println("3 " + a.size() + " " + a.toString());

                        //String[] newArray = (String[]) a.toArray();
                        Object arr3[] = a.toArray();
                        String common[] = new String[arr3.length];

                        for (int i = 0; i < arr3.length; i++) {
                            common[i] = (String) arr3[i];
                        }
                        /*for (int cnt = 0; cnt < theArray.length; cnt++) {
                            newArray[cnt] = theArray[cnt];
                        }
                        newArray[n] = filter2Value;
                        System.out.println("" + newArray);
                         */
                        //System.out.println("" + common.toString());
                        queryObject.setValues(common);
                    } else {
                        queryObject.setValues(new String[]{filter1Value, filter2Value});
                    }
                } else if (filter1) {
                    if (filter1InCheck) {
                        queryObject.setValues(filter1Value.split(";"));
                    } else {
                        queryObject.setValues(new String[]{filter1Value});
                    }
                }
//                queryObject.setValues(new String[]{'%' + value + '%', '%' + value + '%'});
                //   
                if (filter1) {
                    list.addFilterQueryObject(queryObject);
                }
            }

            /*  for (int j = 0; j < cols.length; j++) {
             writer.write(cols[j].getName() + " : " + cols[j].getClass().getName());
             }*/
//            DataListFilter[] filter = null;
            //          filter = new DataListFilter[1];
            // filter[0]
            int startInt = 0;
            int endInt = list.getSize();
            if (start != null) {
                try {
                    startInt = Integer.parseInt(start);
                } catch (NumberFormatException e) {
                    startInt = 0;
                }
            }
            if (end != null) {
                try {
                    endInt = Integer.parseInt(end);
                } catch (NumberFormatException e) {
                    endInt = list.getSize();
                }
            }

            DataListCollection rows = list.getRows(endInt, startInt);


            /* for (int k = 0; k < rows.size(); k++) {
             writer.write(rows.get(k) + " ");
             }*/
            //writer.write(rows.toString());
            JSONArray jsonArray = new JSONArray();

            boolean write = true;
            for (int kk = 0; kk < rows.size(); kk++) {
                JSONObject jsonObject = new JSONObject();

                if (rows.get(kk).getClass().toString().contains("FormRow")) {
                    FormRow row = (FormRow) rows.get(kk);
                    Set<Object> rowSet = row.keySet();

                    Iterator iter = rowSet.iterator();
                    while (iter.hasNext()) {
                        Object column = (iter.next());
                        Object value = row.get(column);

                        if (listImageFields.contains((String) column) && formDefId != null) {
                            if (value == null || ((String) value).isEmpty()) {
                                jsonObject.put((String) column, value);
                            } else {
                                String idVal = row.getId();
                                EnvironmentVariableDao environmentVariableDao = (EnvironmentVariableDao) AppUtil.getApplicationContext().getBean("environmentVariableDao");
                                EnvironmentVariable evServerURL = environmentVariableDao.loadById("serverURL", appDef);
                                EnvironmentVariable evServerImagePlugin = environmentVariableDao.loadById("imagePlugin", appDef);
                                if (evServerImagePlugin == null) {
                                    evServerImagePlugin.setValue("webservice.JsonImagePlugin");
                                }
                                if (evServerURL == null) {
                                    jsonObject.put((String) column, value);
                                } else {

                                    String imageUrl = null;
                                    String[] valueArray = ((String) value).split(";");
                                    for (String valueObj : valueArray) {
                                        if (imageUrl == null) {
                                            imageUrl = evServerURL.getValue() + "/jw/web/json/plugin/" + evServerImagePlugin.getValue() + "/service?formId=" + formDefId + "&fileType=image&fileName=" + valueObj + "&id=" + idVal;

                                        } else {
                                            imageUrl += ";" + evServerURL.getValue() + "/jw/web/json/plugin/" + evServerImagePlugin.getValue() + "/service?formId=" + formDefId + "&fileType=image&fileName=" + valueObj + "&id=" + idVal;
                                        }
                                    }
                                    jsonObject.put((String) column, imageUrl);
                                }
                            }
                        } else {
                            jsonObject.put((String) column, value);
                        }
                    }
                    jsonArray.put(jsonObject);
                } else {
                    write = true;
                    HashMap row = (HashMap) rows.get(kk);
                    Set<Object> rowSet = row.keySet();

                    Iterator iter = rowSet.iterator();
                    while (iter.hasNext()) {

                        Object column = (iter.next());
                        Object value = row.get(column);
                        jsonObject.put(((String) column).toLowerCase(), value);
                    }
                    jsonArray.put(jsonObject);
                }
            }
            if (write) {
                //writer.write("Size : " + rows.size());
                // writer.write("Sizeb : " + list.getTotal());
                writer.write(jsonArray.toString());
            } else {
                writer.write(rows.toString());
            }


            /*            writer.write(" -- > " + rows.size());
             writer.write(" -- > " + datalistDefinition.getJson());
             writer.write(list.toString());
             */
        }
        //    writer.write(" finish ");
    }

    private ArrayList<String> getFormImageFields(String formId) {
        ArrayList<String> list = new ArrayList<String>();

        AppDefinition appDef = AppUtil.getCurrentAppDefinition();
        FormDefinitionDao formDefinitionDao = (FormDefinitionDao) AppUtil.getApplicationContext().getBean("formDefinitionDao");
        FormService formService = (FormService) AppUtil.getApplicationContext().getBean("formService");
        FormDataDao formDataDao = (FormDataDao) AppUtil.getApplicationContext().getBean("formDataDao");

        //ListService listService = (ListService) AppUtil.getApplicationContext().getBean("listService");
        List<String> columnNames = null;

        FormDefinition formDef = formDefinitionDao.loadById(formId, appDef);
        Form form = null;
        if (formDef != null) {
            String formJson = formDef.getJson();

            if (formJson != null) {
                form = (Form) formService.createElementFromJson(formJson);
            }

            if (form != null) {
                String tableName = formDataDao.getFormTableName(form);
                columnNames = new ArrayList(formDataDao.getFormDefinitionColumnNames(tableName));
                Collections.sort(columnNames);

                try {

                    for (String columnName : columnNames) {
                        Element element = FormUtil.findElement(columnName, form, null, true);
                        if (element != null && !(element instanceof FormContainer)) {
                            String id = element.getPropertyString(FormUtil.PROPERTY_ID);
                            String type = element.getClass().toString();//PropertyString(FormUtil.PROPERTY_PROPERTIES);
                            if (id != null && !id.isEmpty()) {
                                if (type != null) {
                                    if (type.contains("ImageUpload")) {
                                        list.add(id);
                                    } else if (type.contains("FileUpload")) {
                                        list.add(id);
                                    } else {
                                        //System.out.println(" --> " + type);
                                    }

                                }
                            }
                        }
                    }

                } catch (Exception ex) {
                    LogUtil.error(this.getClass().getName(), ex, "Get form fields Error!");
                }
            }
        }
        return list;
    }

    private void handleFormFields(Writer writer, String formId) {
        AppDefinition appDef = AppUtil.getCurrentAppDefinition();
        FormDefinitionDao formDefinitionDao = (FormDefinitionDao) AppUtil.getApplicationContext().getBean("formDefinitionDao");
        FormService formService = (FormService) AppUtil.getApplicationContext().getBean("formService");
        FormDataDao formDataDao = (FormDataDao) AppUtil.getApplicationContext().getBean("formDataDao");

        //ListService listService = (ListService) AppUtil.getApplicationContext().getBean("listService");
        List<String> columnNames = null;

        FormDefinition formDef = formDefinitionDao.loadById(formId, appDef);
        Form form = null;
        if (formDef != null) {
            String formJson = formDef.getJson();

            if (formJson != null) {
                form = (Form) formService.createElementFromJson(formJson);
            }

            if (form != null) {
                String tableName = formDataDao.getFormTableName(form);
                columnNames = new ArrayList(formDataDao.getFormDefinitionColumnNames(tableName));
                Collections.sort(columnNames);

                try {
                    JSONArray jsonArray = new JSONArray();

                    for (String columnName : columnNames) {
                        Element element = FormUtil.findElement(columnName, form, null, true);
                        if (element != null && !(element instanceof FormContainer)) {
                            String id = element.getPropertyString(FormUtil.PROPERTY_ID);
                            String label = element.getPropertyString(FormUtil.PROPERTY_LABEL);
                            String type = element.getClass().toString();//PropertyString(FormUtil.PROPERTY_PROPERTIES);
                            if (id != null && !id.isEmpty()) {
                                if (label == null || label.isEmpty()) {
                                    label = id;
                                }

                                Map<String, String> option = new HashMap<String, String>();
                                option.put("id", id);
                                option.put("label", label);
                                option.put("type", type);
                                jsonArray.put(option);
                            }
                        }
                    }

                    jsonArray.write(writer);
                    writer.write(tableName);
                } catch (Exception ex) {
                    LogUtil.error(this.getClass().getName(), ex, "Get form fields Error!");
                }
            }
        }
    }
}
