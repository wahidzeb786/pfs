/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;
import org.joget.common.UserWorkflowHelper;
import org.joget.common.UserWorkflowInfo;
import org.joget.directory.model.User;
import org.joget.plugin.base.DefaultApplicationPlugin;
import org.joget.workflow.model.WorkflowAssignment;
import org.joget.workflow.model.WorkflowVariable;
import org.joget.workflow.model.service.WorkflowManager;

/**
 *
 * @author Jahan Zaib
 */

/*

            String assignToUpdated = "";
            String[] assignToAll = nextUser.split(";");
            for (String assignToOne : assignToAll) {
                if (workflowHelper.isUserPartOfProject(projectId, assignToOne)) {
                    if (assignToUpdated.isEmpty()) {
                        assignToUpdated = assignToOne;
                    } else {
                        assignToUpdated += ";" + assignToOne;
                    }
                }
            }
            if (assignToUpdated.isEmpty()) {
                row.setProperty("status", "reject");
                row.setProperty("status_integration", "reject");
                wfManager.activityVariable(activityId, "status", "reject");
                
            }
 */
public class ProcessBeginTool extends DefaultApplicationPlugin {

    @Override
    public Object execute(Map map) {
        WorkflowAssignment wfAssignment = (WorkflowAssignment) map.get("workflowAssignment");
        String activityId = wfAssignment.getActivityId();
        String processId = wfAssignment.getProcessId();
        String activityName = wfAssignment.getActivityName();
        Logger.getLogger(getClass().getName()).log(Level.INFO, wfAssignment.getActivityDefId());
        Logger.getLogger(getClass().getName()).log(Level.INFO, wfAssignment.getProcessDefId());

        Logger.getLogger(getClass().getName()).log(Level.INFO, wfAssignment.getParticipant());
        WorkflowManager wfManager = (WorkflowManager) AppUtil.getApplicationContext().getBean("workflowManager");

        String processName = wfAssignment.getProcessName();

        Logger.getLogger(getClass().getName()).log(Level.INFO, "\n\n\nactivityId : {0}", activityId);
        Logger.getLogger(getClass().getName()).log(Level.INFO, "processId : {0}", processId);
        Logger.getLogger(getClass().getName()).log(Level.INFO, "processName : {0}", processName);
        Logger.getLogger(getClass().getName()).log(Level.INFO, "activityName : {0}", activityName);

        List<WorkflowVariable> vars = wfAssignment.getProcessVariableList();
        /*New Fix For Delay Inbox*/
        //wfManager.activityVariable(activityId, "assign_to", "");

        for (WorkflowVariable var : vars) {
            Logger.getLogger(getClass().getName()).log(Level.INFO, "{0} = {1} = {2}", new Object[]{var.getId(), var.getName(), var.getVal()});
        }

        String status;
        String id;
        String contractId = null;
        String voId = null;
        String formId;
        String tableOrganize;
        String formIdOrganize;
        String type;
        UserWorkflowInfo userWorkflowInfo = null;
        String projectId = null;
        String contractType = null;
        Boolean isContractAmendData = false;
        String assignTo = null;
        if (processName.contains("Process 1")) {
            contractType = AppUtil.processHashVariable("#form.pfs_contract.type#", wfAssignment, null, null);
            if ("amend_data".equalsIgnoreCase(contractType)) {
                isContractAmendData = true;
            }
            status = AppUtil.processHashVariable("#form.pfs_contract.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_contract.id#", wfAssignment, null, null);
            type = "contract";
            formId = "contractForm";
            tableOrganize = "app_fd_approval_cntrct_flow";
            projectId = AppUtil.processHashVariable("#form.pfs_contract.project_id#", wfAssignment, null, null);

        } else if ("VO Process".equalsIgnoreCase(processName)) {
            status = AppUtil.processHashVariable("#form.pfs_vo.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_vo.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_vo.contract_id#", wfAssignment, null, null);
            voId = AppUtil.processHashVariable("#form.pfs_vo.vo_id#", wfAssignment, null, null);

            type = "vo";
            formId = "voForm";
            tableOrganize = "app_fd_approval_flow_vos";
        } else {
            status = AppUtil.processHashVariable("#form.pfs_claim.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_claim.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_claim.contract_id#", wfAssignment, null, null);

            type = "claim";
            formId = "claimForm";
            tableOrganize = "app_fd_approval_flow_claims";
        }

        Logger.getLogger(getClass().getName()).log(Level.INFO, "Status : {0}", status);
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Id: {0}", id);
        if ("contract".equals(type)) {
            userWorkflowInfo = organizeAllUserForContract(id);;
            if (userWorkflowInfo != null) {
                assignTo = userWorkflowInfo.getUsername();
                wfManager.activityVariable(activityId, "duration", userWorkflowInfo.getDuration());
            }
            organizeAllUserForClaims(id);
            organizeAllUserForVO(id);
        } else if ("vo".equals(type)) {
            userWorkflowInfo = organizeAllUserForVO(contractId, id);
            if (userWorkflowInfo != null) {
                assignTo = userWorkflowInfo.getUsername();
                wfManager.activityVariable(activityId, "duration", userWorkflowInfo.getDuration());
            }
        } else {
            userWorkflowInfo = organizeAllUserForClaim(contractId, id);
            if (userWorkflowInfo != null) {
                assignTo = userWorkflowInfo.getUsername();
                wfManager.activityVariable(activityId, "duration", userWorkflowInfo.getDuration());
            }
        }

        Commons commons = new Commons();

        FormRow row = new FormRow();
        row.setId(id);
        row.setProperty("username", "");
        row.setProperty("fullname", "");
        row.setProperty("process_id", processId);

        if (assignTo != null) {
            row.setProperty("status", "New");
            row.setProperty("status_integration", "New");

            wfManager.activityVariable(activityId, "status", "New");
            /*New Fix For Delay Inbox*/
            wfManager.activityVariable(activityId, "assign_to", assignTo);
            row.setProperty("assign_to", assignTo);
            if ("contract".equals(type)) {
                UserWorkflowHelper workflowHelper = new UserWorkflowHelper();

                if (userWorkflowInfo != null && ("Project Manager".equalsIgnoreCase(userWorkflowInfo.getRole())
                        || workflowHelper.isUserProjectManager(projectId, userWorkflowInfo.getUsername()))) {
                    row.setProperty("type_approval", "Project Owner");
                } else {
                    row.setProperty("type_approval", "");
                }
            }
            // send email for approval
            try {
                String[] usersList = assignTo.split(";");
                System.out.println("\n\n\n---> " + assignTo + "<----\n\n\n");

                for (String username : usersList) {

                    System.out.println("\n\n\n---> " + username + "<----\n\n\n");

                    User user = commons.getUser(username.trim());

                    if (user != null) {
                        //System.out.println("\n\n\n---> Sending to " + username + "<----\n\n\n");

                        String email = user.getEmail();
                        CustomEmail customEmail = new CustomEmail();
                        customEmail.SendCustomEmail(username, email, type, "Approval",
                                wfAssignment, projectId, isContractAmendData, "New");
                    } else {
                        System.out.println("\n\n\n---> " + username + " is null <----\n\n\n");

                    }
                }
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, e.getMessage());
            }
        } else {
            row.setProperty("status", "reject");
            row.setProperty("status_integration", "reject");
            wfManager.activityVariable(activityId, "status", "reject");
        }
        if (assignTo != null) {
            /*New Fix For Delay Inbox*/
            //wfManager.activityVariable(activityId, "assign_to", assignTo);
        }
        commons.insertForm(formId, row);

//        wfManager.activityVariable(activityId, "companyName", dao.getVendorCompanyName());
        //      wfManager.activityVariable(activityId, "assignTo", "HelpDesk");
        return null;
    }

    private UserWorkflowInfo organizeAllUserForContract(String contractId) {
        UserWorkflowInfo assignTo = null;
        Commons commons = new Commons();

        boolean isFirstUser = true;

        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_sequence, c_username, c_role, id, c_option, c_project_id, c_duration "
                    + "from app_fd_approval_cntrct_flow where c_contract_id='" + contractId + "' order by c_sequence asc";
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        String sequence = rSet.getString("c_sequence");
                        String usernames = rSet.getString("c_username");
                        String role = rSet.getString("c_role");
                        String id = rSet.getString("id");
                        String projectId = rSet.getString("c_project_id");
                        String duration = rSet.getString("c_duration");
                        System.out.println(sequence + " - " + usernames + ", " + role + ", " + projectId);
                        FormRow row = new FormRow();
                        row.setId(id);
                        if (!"".equals(role) && (usernames == null || "".equals(usernames))) {
                            usernames = getAllUsersForRole(role, projectId);
                            if (usernames != null) {
                                row.setProperty("username", usernames);
                            }
                        }
                        if (usernames != null && usernames.contains("(__USER__REMOVED__)")) {
                            String[] usersList = usernames.split(";");
                            String processedUserNames = "";
                            for (String username : usersList) {
                                if (username.contains("(__USER__REMOVED__)")) {
                                } else {
                                    if (processedUserNames.isEmpty()) {
                                        processedUserNames = username;
                                    } else {
                                        processedUserNames += ";" + username;
                                    }
                                }
                            }
                            usernames = processedUserNames;
                        }

                        if (usernames == null || "".equals(usernames)) {
                            row.setProperty("status", "Skip");
                        } else {
                            row.setProperty("username", usernames);
                            if (isFirstUser) {
                                assignTo = new UserWorkflowInfo(usernames, role, duration);
                                row.setProperty("status", "Assign");
                                isFirstUser = false;
                            } else {
                                row.setProperty("status", "New");
                            }
                        }
                        commons.insertForm("approvalFlowContractForm", row);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return assignTo;
    }

    private UserWorkflowInfo organizeAllUserForVO(String contractId, String voId) {
        UserWorkflowInfo assignTo = null;
        Commons commons = new Commons();
        boolean isFirstUser = true;

        FormRowSet rowSet = commons.getFormRows("approvalFlowVOContractForm", "contract_id", new String[]{contractId}, "sequence");
        commons.delateFormData("approvalFlowVOVOForm", "vo_id", voId);
        for (FormRow row : rowSet) {
            String sequence = row.getProperty("sequence");
            String usernames = row.getProperty("username");
            String duration = row.getProperty("duration");

            Logger.getLogger(getClass().getName()).log(Level.INFO, "{0} - {1}", new Object[]{sequence, usernames});
            row.setId(null);

            if (usernames != null && usernames.contains("(__USER__REMOVED__)")) {
                System.out.println("Processing User Removed");
                String[] usersList = usernames.split(";");
                String processedUserNames = "";
                for (String username : usersList) {
                    if (username.contains("(__USER__REMOVED__)")) {
                    } else {
                        if (processedUserNames.isEmpty()) {
                            processedUserNames = username;
                        } else {
                            processedUserNames += ";" + username;
                        }
                    }
                }
                usernames = processedUserNames;
            }

            if (usernames == null || "".equals(usernames)) {
                row.setProperty("status", "Skip");
            } else {
                row.setProperty("username", usernames);
                if (isFirstUser) {
                    assignTo = new UserWorkflowInfo(usernames, null, duration);
                    row.setProperty("status", "Assign");
                    isFirstUser = false;
                } else {
                    row.setProperty("status", "New");
                }
            }

            row.setProperty("vo_id", voId);
            System.out.println("row: " + row);
            commons.insertForm("approvalFlowVOVOForm", row);
        }
        return assignTo;
    }

    private UserWorkflowInfo organizeAllUserForClaim(String contractId, String claimId) {
        Commons commons = new Commons();
        boolean isFirstUser = true;

        UserWorkflowInfo assignTo = null;
        commons.delateFormData("approvalFlowClaimClaimForm", "claim_id", new String[]{claimId});
        FormRowSet rowSet = commons.getFormRows("approvalFlowClaimContractForm", "contract_id", new String[]{contractId}, "sequence");
        for (FormRow row : rowSet) {
            String sequence = row.getProperty("sequence");
            String usernames = row.getProperty("username");
            String duration = row.getProperty("duration");

            Logger.getLogger(getClass().getName()).log(Level.INFO, sequence + " - " + usernames);
            row.setId(null);

            if (usernames != null && usernames.contains("(__USER__REMOVED__)")) {
                String[] usersList = usernames.split(";");
                String processedUserNames = "";
                for (String username : usersList) {
                    if (username.contains("(__USER__REMOVED__)")) {
                    } else {
                        if (processedUserNames.isEmpty()) {
                            processedUserNames = username;
                        } else {
                            processedUserNames += ";" + username;
                        }
                    }
                }
                usernames = processedUserNames;
            }

            if (usernames == null || "".equals(usernames)) {
                row.setProperty("status", "Skip");
            } else {
                row.setProperty("username", usernames);
                if (isFirstUser) {
                    assignTo = new UserWorkflowInfo(usernames, "", duration);
                    row.setProperty("status", "Assign");
                    isFirstUser = false;
                } else {
                    row.setProperty("status", "New");
                }
            }
            row.setProperty("claim_id", claimId);

            commons.insertForm("approvalFlowClaimClaimForm", row);
        }
        return assignTo;
    }

    private void organizeAllUserForClaims(String contractId) {
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        Commons commons = new Commons();

        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_sequence, c_username, c_role, id, c_option, c_project_id "
                    + "from app_fd_approval_flow_claims where c_contract_id='" + contractId + "' order by c_sequence";
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        String sequence = rSet.getString("c_sequence");
                        String usernames = rSet.getString("c_username");
                        String role = rSet.getString("c_role");
                        String id = rSet.getString("id");
                        String projectId = rSet.getString("c_project_id");
                        Logger.getLogger(getClass().getName()).log(Level.INFO, sequence + " - " + usernames + ", " + role + ", " + projectId);
                        FormRow row = new FormRow();
                        row.setId(id);
                        if (!"".equals(role) && (usernames == null || "".equals(usernames))) {
                            usernames = getAllUsersForRole(role, projectId);
                            if (usernames != null) {
                                row.setProperty("username", usernames);
                            }
                        }

                        if (usernames == null || "".equals(usernames)) {
                            row.setProperty("status", "Skip");
                        } else {
                            row.setProperty("status", "New");
                        }
                        commons.insertForm("approvalFlowClaimContractForm", row);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void organizeAllUserForVO(String contractId) {
        Commons common = new Commons();
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_sequence, c_username, c_role, id, c_option, c_project_id "
                    + "from app_fd_approval_flow_vos where c_contract_id='" + contractId + "' order by c_sequence";
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        String sequence = rSet.getString("c_sequence");
                        String usernames = rSet.getString("c_username");
                        String role = rSet.getString("c_role");
                        String id = rSet.getString("id");
                        String projectId = rSet.getString("c_project_id");
                        Logger.getLogger(getClass().getName()).log(Level.INFO, sequence + " - " + usernames + ", " + role + ", " + projectId);
                        FormRow row = new FormRow();
                        row.setId(id);
                        if (!"".equals(role) && (usernames == null || "".equals(usernames))) {
                            usernames = getAllUsersForRole(role, projectId);
                            if (usernames != null) {
                                row.setProperty("username", usernames);
                            }
                        }

                        if (usernames == null || "".equals(usernames)) {
                            row.setProperty("status", "Skip");
                        } else {
                            row.setProperty("status", "New");
                        }

                        common.insertForm("approvalFlowVOContractForm", row);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }

    }

    private String getAllUsersForRole(String roleName, String projectId) {
        String assignTo = null;

        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_username "
                    + "from app_fd_user_dep_project where c_project_id='" + projectId + "' and c_department='" + roleName + "'";
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        String username = rSet.getString("c_username");
                        if (assignTo == null) {
                            assignTo = username;
                        } else {
                            assignTo += ";" + username;
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(getClass().getName()).log(Level.INFO, "For RoleName {0},projectId {1} Found {2}", new Object[]{roleName, projectId, assignTo});
        return assignTo;
    }

    @Override
    public String getName() {
        return "Process Begin Tool";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public String getDescription() {
        return "Process Begin Tool;";
    }

    @Override
    public String getLabel() {
        return "Process Begin Tool";
    }

    @Override
    public String getClassName() {
        return this.getClass().getName();
    }

    @Override
    public String getPropertyOptions() {
        return AppUtil.readPluginResource(getClass().getName(), "/properties/WizardWebServiceTool.json", null, true, "message/WizardWebServiceTool");
    }
}
