/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.util.List;
import java.util.Map;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;
import org.joget.plugin.base.DefaultApplicationPlugin;
import org.joget.workflow.model.WorkflowAssignment;
import org.joget.workflow.model.WorkflowVariable;
import org.joget.workflow.model.service.WorkflowManager;

/**
 *
 * @author Jahan Zaib
 */
public class ApprovedTool extends DefaultApplicationPlugin {

    @Override
    public Object execute(Map map) {

        return null;
    }

    public String getName() {
        return "Approved Tool";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "Approved Tool;";
    }

    public String getLabel() {
        return "Approved Tool";
    }

    public String getClassName() {
        return this.getClass().getName();
    }

    public String getPropertyOptions() {
        return AppUtil.readPluginResource(getClass().getName(), "/properties/WizardWebServiceTool.json", null, true, "message/WizardWebServiceTool");
    }
}
