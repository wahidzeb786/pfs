/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.apache.commons.lang.StringUtils;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.model.FormRow;
import org.joget.common.Commons;
import org.joget.common.UserWorkflowHelper;
import org.joget.common.UserWorkflowInfo;
import org.joget.directory.model.User;
import org.joget.plugin.base.DefaultApplicationPlugin;
import org.joget.workflow.model.WorkflowAssignment;
import org.joget.workflow.model.WorkflowVariable;
import org.joget.workflow.model.service.WorkflowManager;

/**
 *
 * @author Jahan Zaib
 */
public class WorkflowBrain extends DefaultApplicationPlugin {

    @Override
    public Object execute(Map map) {
        WorkflowAssignment wfAssignment = (WorkflowAssignment) map.get("workflowAssignment");
        String activityId = wfAssignment.getActivityId();
        String processId = wfAssignment.getProcessId();
        String activityName = wfAssignment.getActivityName();
        System.out.println("" + wfAssignment.getActivityDefId());
        System.out.println("" + wfAssignment.getProcessDefId());

        System.out.println("" + wfAssignment.getParticipant());
        WorkflowManager wfManager = (WorkflowManager) AppUtil.getApplicationContext().getBean("workflowManager");

        String processName = wfAssignment.getProcessName();

        System.out.println("\n\n\n-activityId : " + activityId);
        System.out.println("processId : " + processId);
        System.out.println("processName : " + processName);
        System.out.println("activityName : " + activityName);

        Commons commons = new Commons();

        List<WorkflowVariable> vars = wfAssignment.getProcessVariableList();

        for (WorkflowVariable var : vars) {
            System.out.println("" + var.getId() + " = " + var.getName() + " = " + var.getVal());
        }

        String status;
        String id;
        String contractId = null;
        String createdBy = null;
        String formId;
        String tableOrganize;
        String formIdOrganize;
        String type;
        String assignTo;
        String projectId = null;
        String contractType = null;
        Boolean isContractAmendData = false;
        /*New Fix For Delay Inbox*/
        //wfManager.activityVariable(activityId, "assign_to", "");
        if (processName.contains("Process 1")) {
            contractType = AppUtil.processHashVariable("#form.pfs_contract.type#", wfAssignment, null, null);

            if ("amend_data".equalsIgnoreCase(contractType)) {
                isContractAmendData = true;
            }
            status = AppUtil.processHashVariable("#form.pfs_contract.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_contract.id#", wfAssignment, null, null);
            createdBy = AppUtil.processHashVariable("#form.pfs_contract.created_by#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_contract.project_id#", wfAssignment, null, null);

            type = "contract";
            formId = "contractForm";
            tableOrganize = "app_fd_approval_cntrct_flow";

        } else if ("VO Process".equalsIgnoreCase(processName)) {
            status = AppUtil.processHashVariable("#form.pfs_vo.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_vo.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_vo.contract_id#", wfAssignment, null, null);
            createdBy = AppUtil.processHashVariable("#form.pfs_vo.created_by#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_vo.project_id#", wfAssignment, null, null);

            type = "vo";
            formId = "voForm";
            tableOrganize = "app_fd_approval_flow_vos";
        } else {
            status = AppUtil.processHashVariable("#form.pfs_claim.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_claim.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_claim.contract_id#", wfAssignment, null, null);
            createdBy = AppUtil.processHashVariable("#form.pfs_claim.created_by#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_claim.project_id#", wfAssignment, null, null);

            type = "claim";
            formId = "claimForm";
            tableOrganize = "app_fd_approval_flow_claims";
        }

        System.out.println("Status : " + status);
        System.out.println("Id: " + id);
        System.out.println("CreatedBy: " + createdBy);

        String nextUser = null;
        UserWorkflowInfo userWorkflowInfo = null;

        // get by status, must be called before current user status = complete
        ArrayList<String> approvedByUsers = getCompletedApprovedUser(id, type);

        if ("info".equals(status)) {
            if (!StringUtils.isEmpty(createdBy)) {
                nextUser = createdBy;
            }
        } else {
            if ("contract".equalsIgnoreCase(type)) {
                updateAssignContract(id);
                userWorkflowInfo = getNextUserForContract(id);
                if (userWorkflowInfo != null) {
                    nextUser = userWorkflowInfo.getUsername();
                    wfManager.activityVariable(activityId, "duration", userWorkflowInfo.getDuration());
                }
            } else if ("vo".equalsIgnoreCase(type)) {
                updateAssignVO(id);
                userWorkflowInfo = getNextUserForVO(id);
                if (userWorkflowInfo != null) {
                    nextUser = userWorkflowInfo.getUsername();
                    wfManager.activityVariable(activityId, "duration", userWorkflowInfo.getDuration());
                }
            } else {
                updateAssignClaim(id);
                userWorkflowInfo = getNextUserForClaim(id);
                if (userWorkflowInfo != null) {
                    nextUser = userWorkflowInfo.getUsername();
                    wfManager.activityVariable(activityId, "duration", userWorkflowInfo.getDuration());
                }
            }
        }
        System.out.println(" next user " + nextUser);

        CustomEmail customEmail = new CustomEmail();

        FormRow row = new FormRow();
        row.setId(id);
        row.setProperty("username", "");
        row.setProperty("fullname", "");

        if (nextUser != null) {
            /*New Fix For Delay Inbox*/
            wfManager.activityVariable(activityId, "assign_to", nextUser);

            row.setProperty("remarks", "");
            row.setProperty("assign_to", nextUser);
            if ("reject".equals(status)) {
                try {
                    // if reject send to createdBy and previously approvedBy;
                    User createUser = commons.getUser(createdBy);
                    if (createUser != null) {
                        String createUserEmail = createUser.getEmail();
                        if (createUserEmail != null) {
                            customEmail.SendCustomEmail(createdBy, createUserEmail, type,
                                    "Reject", wfAssignment, projectId, isContractAmendData, status);
                        }
                    }
                    for (String approvedBy : approvedByUsers) {
                        User abUser = commons.getUser(approvedBy);
                        if (abUser != null) {
                            String abUserEmail = abUser.getEmail();
                            if (abUserEmail != null) {
                                customEmail.SendCustomEmail(approvedBy, abUserEmail, type,
                                        "ApprovedReject", wfAssignment, projectId,
                                        isContractAmendData, status);
                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.getLogger(getClass().getName()).log(Level.INFO, e.getMessage());
                }
                row.setProperty("status", "reject");
                row.setProperty("status_integration", "reject");
                row.setProperty("assign_to", "");
            } else {
                row.setProperty("status", "In Progress");
                row.setProperty("status_integration", "In Progress");
                // send email for approval if status is not info

                if (!"info".equals(status)) {
                    try {
                        String[] usersList = nextUser.split(";");
                        for (String username : usersList) {
                            User user = commons.getUser(username);
                            if (user != null) {
                                String email = user.getEmail();
                                if (email != null) {
                                    customEmail.SendCustomEmail(username, email, type,
                                            "Approval", wfAssignment, projectId,
                                            isContractAmendData, status);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Logger.getLogger(getClass().getName()).log(Level.INFO, e.getMessage());
                    }
                } else if ("info".equals(status)) {
                    // if status is more infomation need send email as well
                    try {
                        String[] usersList = nextUser.split(";");
                        for (String username : usersList) {
                            User user = commons.getUser(username);
                            if (user != null) {
                                String email = user.getEmail();
                                if (email != null) {
                                    customEmail.SendCustomEmail(username, email, type, "Info",
                                            wfAssignment, projectId, isContractAmendData, status);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Logger.getLogger(getClass().getName()).log(Level.INFO, e.getMessage());
                    }
                }
            }
            if ("contract".equalsIgnoreCase(type)) {
                UserWorkflowHelper workflowHelper = new UserWorkflowHelper();
                System.out.println("User WorkFlow  : " + workflowHelper);
                System.out.println(" projectId " + projectId);

                if (userWorkflowInfo != null
                        && ("Project Manager".equalsIgnoreCase(userWorkflowInfo.getRole())
                        || workflowHelper.isUserProjectManager(projectId, userWorkflowInfo.getUsername()))) {

                    row.setProperty("type_approval", "Project Owner");
                } else {
                    row.setProperty("type_approval", "");
                }
            }
        } else {
            User createUser = commons.getUser(createdBy);
            if ("reject".equals(status)) {
                try {
                    if (createUser != null) {
                        String createUserEmail = createUser.getEmail();
                        if (createUserEmail != null) {
                            customEmail.SendCustomEmail(createdBy, createUserEmail, type, "Reject",
                                    wfAssignment, projectId, isContractAmendData, status);
                        }
                    }
                    for (String approvedBy : approvedByUsers) {
                        User abUser = commons.getUser(approvedBy);
                        if (abUser != null) {
                            String abUserEmail = abUser.getEmail();
                            if (abUserEmail != null) {
                                customEmail.SendCustomEmail(approvedBy, abUserEmail, type,
                                        "ApprovedReject", wfAssignment, projectId, isContractAmendData, status);
                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.getLogger(getClass().getName()).log(Level.INFO, e.getMessage());
                }
                wfManager.activityVariable(activityId, "status", "reject");
                row.setProperty("status", "reject");
                row.setProperty("status_integration", "reject");

            } else {
                wfManager.activityVariable(activityId, "status", "complete");
                row.setProperty("status_integration", "complete");

                // notice user approval complete
                System.out.println("Notify user that it is approved");
                if (createUser != null) {
                    String createUserEmail = createUser.getEmail();
                    if (createUserEmail != null) {
                        customEmail.SendCustomEmail(createdBy, createUserEmail, type, "Approved", wfAssignment,
                                projectId, isContractAmendData, status, true);

                    }
                }

            }
            row.setProperty("assign_to", "");
        }
        if ("info".equals(status)) {
            row.setProperty("status_integration", "info");
        }
        /*New Fix For Delay Inbox*/
        if (nextUser != null) {
            //wfManager.activityVariable(activityId, "assign_to", nextUser);
        }

        commons.insertForm(formId, row);

        return null;
    }

    private void updateAssignVO(String voId) {
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "UPDATE app_fd_approval_flow_vo_vo set c_status='Complete' where c_vo_id='" + voId + "' AND c_status='Assign'";
            try (Statement stmt = con.createStatement()) {

                System.out.println(" Update VO Result : " + stmt.executeUpdate(query));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowBrain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateAssignClaim(String claimId) {
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "UPDATE app_fd_approval_flow_claima set c_status='Complete' where c_claim_id='" + claimId + "' AND c_status='Assign'";
            try (Statement stmt = con.createStatement()) {

                System.out.println(" Update Claim Result : " + stmt.executeUpdate(query));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowBrain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateAssignContract(String contractId) {
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "UPDATE app_fd_approval_cntrct_flow set c_status='Complete' where c_contract_id='" + contractId + "' AND c_status='Assign'";
            try (Statement stmt = con.createStatement()) {
                System.out.println(" Update Contract Result : " + stmt.executeUpdate(query));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowBrain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private UserWorkflowInfo getNextUserForContract(String contractId) {
        UserWorkflowInfo assignTo = null;
        Commons commons = new Commons();

        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_sequence, c_username, c_role, id, c_option, c_duration "
                    + "from app_fd_approval_cntrct_flow where c_contract_id='" + contractId + "' and c_status='New' order by c_sequence";
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        String sequence = rSet.getString("c_sequence");
                        String usernames = rSet.getString("c_username");
                        String role = rSet.getString("c_role");
                        String duration = rSet.getString("c_duration");
                        String id = rSet.getString("id");
                        FormRow row = new FormRow();
                        row.setId(id);
                        if (usernames.contains("(__USER__REMOVED__)")) {

                            String[] usersList = usernames.split(";");
                            String processedUserNames = "";
                            for (String username : usersList) {
                                if (username.contains("(__USER__REMOVED__)")) {
                                } else {
                                    if (processedUserNames.isEmpty()) {
                                        processedUserNames = username;
                                    } else {
                                        processedUserNames += ";" + username;
                                    }
                                }
                            }
                            usernames = processedUserNames;
                            if (usernames.isEmpty()) {
                                row.setProperty("status", "SKIP");
                                commons.insertForm("approvalFlowContractForm", row);
                                continue;
                            }
                        }
                        assignTo = new UserWorkflowInfo(usernames, role, duration);
                        row.setProperty("status", "Assign");

                        commons.insertForm("approvalFlowContractForm", row);
                        break;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return assignTo;
    }

    private UserWorkflowInfo getNextUserForVO(String voId) {
        UserWorkflowInfo assignTo = null;
        Commons commons = new Commons();

        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_sequence, c_username, c_role, id, c_option, c_duration "
                    + "from app_fd_approval_flow_vo_vo where c_vo_id='" + voId + "' and c_status='New' order by c_sequence";
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        String sequence = rSet.getString("c_sequence");
                        String usernames = rSet.getString("c_username");
                        String id = rSet.getString("id");
                        String duration = rSet.getString("c_duration");
                        String role = rSet.getString("c_role");

                        FormRow row = new FormRow();

                        row.setId(id);

                        if (usernames.contains("(__USER__REMOVED__)")) {

                            String[] usersList = usernames.split(";");
                            String processedUserNames = "";
                            for (String username : usersList) {
                                if (username.contains("(__USER__REMOVED__)")) {
                                } else {
                                    if (processedUserNames.isEmpty()) {
                                        processedUserNames = username;
                                    } else {
                                        processedUserNames += ";" + username;
                                    }
                                }
                            }
                            usernames = processedUserNames;
                            if (usernames.isEmpty()) {
                                row.setProperty("status", "SKIP");
                                commons.insertForm("approvalFlowVOVOForm", row);
                                continue;
                            }
                        }
                        /*if (usernames.contains("(__USER__REMOVED__)")) {
                            if (!usernames.contains(";")) {
                                row.setProperty("status", "SKIP");
                                commons.insertForm("approvalFlowVOVOForm", row);
                                continue;
                            }
                        }*/
                        assignTo = new UserWorkflowInfo(usernames, role, duration);
                        row.setProperty("status", "Assign");

                        commons.insertForm("approvalFlowVOVOForm", row);
                        break;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return assignTo;
    }

    private UserWorkflowInfo getNextUserForClaim(String claimId) {
        UserWorkflowInfo assignTo = null;
        Commons commons = new Commons();

        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_sequence, c_username, c_role, id, c_option, c_duration "
                    + "from app_fd_approval_flow_claima where c_claim_id='" + claimId + "' and c_status='New' order by c_sequence";
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        String sequence = rSet.getString("c_sequence");
                        String usernames = rSet.getString("c_username");
                        String id = rSet.getString("id");
                        FormRow row = new FormRow();
                        row.setId(id);
                        if (usernames.contains("(__USER__REMOVED__)")) {

                            String[] usersList = usernames.split(";");
                            String processedUserNames = "";
                            for (String username : usersList) {
                                if (username.contains("(__USER__REMOVED__)")) {
                                } else {
                                    if (processedUserNames.isEmpty()) {
                                        processedUserNames = username;
                                    } else {
                                        processedUserNames += ";" + username;
                                    }
                                }
                            }
                            usernames = processedUserNames;
                            if (usernames.isEmpty()) {
                                row.setProperty("status", "SKIP");
                                commons.insertForm("approvalFlowClaimClaimForm", row);
                                continue;
                            }
                        }
                        String duration = rSet.getString("c_duration");
                        String role = rSet.getString("c_role");

                        assignTo = new UserWorkflowInfo(usernames, role, duration);
                        row.setProperty("status", "Assign");

                        commons.insertForm("approvalFlowClaimClaimForm", row);
                        break;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return assignTo;
    }

    private ArrayList<String> getCompletedApprovedUser(String recId, String type) {
        ArrayList<String> users = new ArrayList<String>();
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "";
            switch (type) {
                case "contract":
                    query = "SELECT c_sequence, c_username, c_role, id, c_option "
                            + "from app_fd_approval_cntrct_flow where c_contract_id='" + recId + "' and c_status='Complete' order by c_sequence";
                    break;
                case "vo":
                    query = "SELECT c_sequence, c_username, c_role, id, c_option "
                            + "from app_fd_approval_flow_vo_vo where c_vo_id='" + recId + "' and c_status='Complete' order by c_sequence";
                case "claim":
                    query = "SELECT c_sequence, c_username, c_role, id, c_option "
                            + "from app_fd_approval_flow_claima where c_claim_id='" + recId + "' and c_status='Complete' order by c_sequence";
                    break;
            }
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        users.add(rSet.getString("c_username"));
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }

    public String getName() {
        return "Workflow Brain Tool";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "Workflow Brain Tool;";
    }

    public String getLabel() {
        return "Workflow Brain Tool";
    }

    public String getClassName() {
        return this.getClass().getName();
    }

    public String getPropertyOptions() {
        return AppUtil.readPluginResource(getClass().getName(), "/properties/WizardWebServiceTool.json", null, true, "message/WizardWebServiceTool");
    }
}
