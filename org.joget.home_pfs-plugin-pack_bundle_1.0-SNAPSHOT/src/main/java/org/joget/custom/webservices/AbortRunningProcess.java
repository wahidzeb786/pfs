/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.joget.apps.app.service.AppUtil;
import org.joget.common.Commons;
import org.joget.workflow.model.WorkflowActivity;
import org.joget.workflow.model.WorkflowAssignment;
import org.joget.workflow.model.WorkflowProcess;
import org.joget.workflow.model.service.WorkflowManager;
import org.joget.workflow.model.service.WorkflowUserManager;

/**
 *
 * @author User
 */
public class AbortRunningProcess extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "Abort Running Process Api Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "Abort Running Process Api Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "Abort Running Process Api Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        performOperation(request, response);

    }

    private void performOperation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (PrintWriter out = response.getWriter()) {
            JSONObject responseObject = new JSONObject();

            String processId = request.getParameter("processId");

            //WorkflowUserManager wum = (WorkflowUserManager) AppUtil.getApplicationContext().getBean("workflowUserManager");
//username of the assignee who should complete the assignment.
            //String username = "admin";
            WorkflowManager workflowManager = (WorkflowManager) AppUtil.getApplicationContext().getBean("workflowManager");

//switch user
            //String currentUser = wum.getCurrentThreadUser();
            //System.out.println("-----> " + currentUser);
            //wum.setCurrentThreadUser(username);
            Commons comm = new Commons();

            try {
                responseObject.put("status", "true");
                workflowManager.removeProcessInstance(processId);
                //getActivityList
                //WorkflowActivity workflowActivity = workflowManager.getActivityByProcess(processId, null);
                //System.out.println(" " + workflowActivity.getActivityDefId());
                Collection<WorkflowActivity> list = workflowManager.getActivityList(processId, null, null, null, null);
                for (WorkflowActivity workflowActivity : list) {
                    System.out.println(" getId : " + workflowActivity.getId());
                    System.out.println(" getActivityDefId : " + workflowActivity.getActivityDefId());
                    System.out.println(" state : " + workflowActivity.getState());
                    System.out.println(" Users : " + workflowActivity.getAssignmentUsers());

                    if ("open.not_running.not_started".equals(workflowActivity.getState())) {
                        HashMap<String, String> mapStatus = new HashMap<>();
                        Map<String, String> variables = new HashMap<String, String>();
                        variables.put("status", "reject");

                        workflowManager.processVariable(processId, "status", "reject");

                        workflowManager.reevaluateAssignmentsForProcess(processId);
                        //workflowManager.assignmentVariable(workflowActivity.getId(), "status", "reject");
                        //workflowManager.assignmentComplete(workflowActivity.getId());//, mapStatus);
                        System.out.println("\n\n\n");
                        /*
                        WorkflowProcess workflowProcess = workflowManager.getProcess(processId);
                        if (workflowProcess != null) {
                            System.out.println("--" + workflowProcess.getTimeConsumingFromDateCreated());
                        }
                         */
                        WorkflowAssignment assignement = workflowManager.getAssignmentByProcess(processId);
                        if (assignement != null) {
                            System.out.println("-->" + assignement.getAssigneeName());
                        }

                    }
                }

            } catch (JSONException ex) {
                Logger.getLogger(AbortRunningProcess.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                //switch back the user
                //wum.setCurrentThreadUser(currentUser);
            }
            out.println(responseObject);

        }
    }
}
