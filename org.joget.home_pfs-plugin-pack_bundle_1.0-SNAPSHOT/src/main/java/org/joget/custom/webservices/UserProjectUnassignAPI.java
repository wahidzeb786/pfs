/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.*;
import java.io.IOException;
import org.apache.commons.lang.StringUtils;
import org.joget.apps.form.model.FormRow;
import org.joget.common.Commons;

/**
 *
 * @author User
 */
public class UserProjectUnassignAPI extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "User Project unassign API Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "User Project Unassign API Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "User Project unassign API Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        performOperation(request, response);

    }

    private void performOperation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (PrintWriter out = response.getWriter()) {
            JSONObject responseObject = new JSONObject();

            StringBuilder jb = new StringBuilder();

            String line = null;
            try {
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null) {
                    jb.append(line);
                }
            } catch (Exception ex) {
                Logger.getLogger(UserProjectUnassignAPI.class.getName()).log(Level.SEVERE, null, ex);
                {
                    responseObject.put("status", false);
                    responseObject.put("message", "Invalid JSON or Request");
                }
                out.println(responseObject);
            }

            JSONObject jsonObject = new JSONObject(jb.toString());

            String userName = null;
            String projectId = null;
            String userRole = null;

            Commons comm = new Commons();
            String requestIncompleteMessage = null;
            while (true) {

                if (jsonObject.has("userName")) {
                    userName = jsonObject.getString("userName");
                } else {
                    requestIncompleteMessage = "userName is required";
                    break;
                }

                if (jsonObject.has("projectId")) {
                    projectId = jsonObject.getString("projectId");
                } else {
                    requestIncompleteMessage = "projectId is required";
                    break;
                }

                if (jsonObject.has("userRole")) {
                    userRole = jsonObject.getString("userRole");
                } else {
                    requestIncompleteMessage = "userRole is required";
                    break;
                }
                break;
            }
            if (requestIncompleteMessage == null) {

                String departmentId = StringUtils.deleteWhitespace(userRole);//.replaceAll("\\s", "");
                String uniqueId = projectId + userName + departmentId;
                Logger.getLogger(UserProjectUnassignAPI.class.getName()).log(Level.INFO, "departmentId : " + departmentId);
                Logger.getLogger(UserProjectUnassignAPI.class.getName()).log(Level.INFO, "username : " + userName);

                responseObject.put("userStatus", comm.unassignDepartmentToUser(userName, departmentId, uniqueId));

                responseObject.put("message", "User Updated");

                comm.delateFormData("userDepartmentSelectionForm", new String[]{uniqueId});
            } else {
                responseObject.put("status", false);
                responseObject.put("message", requestIncompleteMessage);
            }
            out.println(responseObject);

        } catch (JSONException ex) {
            Logger.getLogger(UserProjectUnassignAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String BASIC_AUTHENTICATION_REGEX = "Basic\\s";
    private static final String EMPTY_STRING = "";
    private static final String SEPARATOR = ":";
}
