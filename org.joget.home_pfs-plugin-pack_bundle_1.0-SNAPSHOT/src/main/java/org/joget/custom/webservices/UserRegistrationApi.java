/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.*;
import java.io.IOException;
import org.joget.common.Commons;

/**
 *
 * @author User
 */
public class UserRegistrationApi extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "User Registrstion Api Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "User Registrstion Api Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "User Registrstion Api Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Commons common = new Commons();

        if (common.performAPIAuthorization(request)){// || common.performAuthenticate(request)) {

        } else {
            response.sendError(401);
            return;
        }

        performOperation(request, response);

    }

    private void performOperation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (PrintWriter out = response.getWriter()) {
            JSONObject responseObject = new JSONObject();

            /*String authHeader = request.getHeader(AUTHORIZATION_HEADER_NAME);
            
            if (authHeader != null) {
                authHeader = authHeader.replaceFirst(BASIC_AUTHENTICATION_REGEX, EMPTY_STRING);
                authHeader = new String(Base64.getDecoder().decode(authHeader));

                if (StringUtils.countMatches(authHeader, SEPARATOR) != 1) {
                    {
                        responseObject.put("status", false);
                        responseObject.put("message", "API Login Fail");
                    }
                    out.println(responseObject);
                    return;
                }
            } else {
                {
                    responseObject.put("status", false);
                    responseObject.put("message", "API Login Fail");
                }
                out.println(responseObject);
                return;
            }*/
            StringBuilder jb = new StringBuilder();

            String line = null;
            try {
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null) {
                    jb.append(line);
                }
            } catch (Exception ex) {
                Logger.getLogger(UserRegistrationApi.class.getName()).log(Level.SEVERE, null, ex);
                {
                    responseObject.put("status", false);
                    responseObject.put("message", "Invalid JSON or Request");
                }
                out.println(responseObject);
            }
            Logger.getLogger(UserRegistrationApi.class.getName()).log(Level.INFO, "********************{0}", jb.toString());

            JSONObject jsonObject = new JSONObject(jb.toString());

            String userName = null;
            String password = null;
            String firstName = null;
            String lastName = null;
            String email = null;
            String orgDesc = null;
            String orgId = null;
            String orgName = null;
            String userStatus = null;
            //String userRole = null;
            //String userManager = null;
            Commons comm = new Commons();
            boolean isUpdate = false;
            String requestIncompleteMessage = null;
            while (true) {

                if (jsonObject.has("userName")) {
                    userName = jsonObject.getString("userName");
                } else {
                    requestIncompleteMessage = "userName is required";
                    break;
                }
                isUpdate = comm.userExist(userName);
                System.out.println("isupdate " + isUpdate);

                if (jsonObject.has("password")) {
                    password = jsonObject.getString("password");
                } else {
                    if (!isUpdate) {
                        requestIncompleteMessage = "password is required";
                        break;
                    }
                }

                if (jsonObject.has("firstName")) {
                    firstName = jsonObject.getString("firstName");
                } else {
                    if (!isUpdate) {
                        requestIncompleteMessage = "firstName is required";
                        break;
                    }
                }

                if (jsonObject.has("lastName")) {
                    lastName = jsonObject.getString("lastName");
                } else {
                    if (!isUpdate) {
                        requestIncompleteMessage = "lastName is required";
                        break;
                    }
                }

                if (jsonObject.has("email")) {
                    email = jsonObject.getString("email");
                } else {
                    if (!isUpdate) {
                        requestIncompleteMessage = "email is required";
                        break;
                    } else {
                        email = null;
                    }
                }

                if (jsonObject.has("userStatus")) {
                    userStatus = jsonObject.getString("userStatus");
                } else {
                    if (!isUpdate) {
                        requestIncompleteMessage = "userStatus is required";
                        break;
                    }
                }
                if (jsonObject.has("orgId")) {
                    orgId = jsonObject.getString("orgId");
                } else {
                    if (!isUpdate) {
                        requestIncompleteMessage = "orgId is required";
                        break;
                    }
                }

                if (jsonObject.has("orgDesc")) {
                    orgDesc = jsonObject.getString("orgDesc");
                } else {
                    if (!isUpdate) {
                        requestIncompleteMessage = "orgDesc is required";
                        break;
                    }
                }
                if (jsonObject.has("orgName")) {
                    orgName = jsonObject.getString("orgName");
                } else {
                    if (!isUpdate) {
                        requestIncompleteMessage = "orgName is required";
                        break;
                    }
                }

                /*
                if (jsonObject.has("userRole")) {
                    userRole = jsonObject.getString("userRole");
                } else {
                    requestIncompleteMessage = "userRole is required";
                    break;
                }

                if (jsonObject.has("userManager")) {
                    userManager = jsonObject.getString("userManager");
                } else {
                    if (userRole.equals("engineer")) {
                        requestIncompleteMessage = "userManager is required";
                    }
                break;
                }
                 */
                break;
            }
            if (requestIncompleteMessage == null) {
                responseObject.put("userStatus", comm.createJogetUser(userName, email, password, firstName, lastName, userStatus, isUpdate));
                if (orgId != null) {
                    responseObject.put("orgStatus", comm.addOrganization(orgId, orgName, orgDesc));
                    comm.addEmployment(userName, orgId);
                }
                if (!isUpdate) {
                    responseObject.put("message", "User Created");
                } else {
                    responseObject.put("message", "User Updated");
                }

                /*FormRow row = new FormRow();
                row.setId(userName);
                if (userManager != null && !userManager.isEmpty()) {
                    row.setProperty("user_manager", userManager);
                }
                comm.insertForm("userBaseForm", row);*/
            } else {
                responseObject.put("status", false);
                responseObject.put("message", requestIncompleteMessage);
            }
            out.println(responseObject);

        } catch (JSONException ex) {
            Logger.getLogger(UserRegistrationApi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String BASIC_AUTHENTICATION_REGEX = "Basic\\s";
    private static final String EMPTY_STRING = "";
    private static final String SEPARATOR = ":";
}
