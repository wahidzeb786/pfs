/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;

/**
 *
 * @author User
 */
public class MobileReportApi extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "Mobile Report Api Plugin";
    }

    public String getVersion() {
        return "1.1";
    }

    public String getDescription() {
        return "Mobile Report Api Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "Mobile Report Api Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        performOperation(request, response);

    }

    private void performOperation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Commons common = new Commons();

        if (common.performAPIAuthorization(request)) {//|| common.performAuthenticate(request)) {

        } else {
            //response.sendError(401);
            //return;
        }
        try (PrintWriter out = response.getWriter()) {
            JSONObject responseObject = new JSONObject();

            String userId = request.getParameter("userId");
            String projectId = request.getParameter("projectId");
            String completed = request.getParameter("isComplete");
            Logger.getLogger(MobileReportApi.class.getName()).log(Level.INFO, "********************{0}", projectId);
            Logger.getLogger(MobileReportApi.class.getName()).log(Level.INFO, "********************{0}", userId);
            Logger.getLogger(MobileReportApi.class.getName()).log(Level.INFO, "********************{0}", completed);
            boolean isCompleted = false;

            if (completed != null && "true".equals(completed)) {
                isCompleted = true;
            }

            Logger.getLogger(MobileReportApi.class.getName()).log(Level.INFO, "********************complete = {0}", isCompleted);

            Commons comm = new Commons();
            int projectCount = 0, claimCount = 0, contractCount = 0, voCount = 0;

            if (userId != null && projectId != null) {
                Logger.getLogger(MobileReportApi.class.getName()).log(Level.INFO, "Using User Id & Project Id");
                FormRowSet projectFormRows = comm.getFormRows("projectForm", "project_id", projectId);
                projectCount = projectFormRows.size();
                String sqlContractCount = "SELECT Count(*) from app_fd_pfs_contract where c_assign_to LIKE '%" + userId + "%'";
                if (isCompleted) {
                    sqlContractCount += " and c_status='complete'";
                }
                String sqlClaimCount = "SELECT Count(*) from app_fd_pfs_claim where c_assign_to LIKE '%" + userId + "%'";
                String sqlVOCount = "SELECT Count(*) from app_fd_pfs_vo where c_assign_to LIKE '%" + userId + "%'";
                Logger.getLogger(MobileReportApi.class.getName()).log(Level.INFO, "********************contract query = {0}", sqlContractCount);

                DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
                try (Connection con = ds.getConnection()) {
                    try (Statement stmt = con.createStatement()) {
                        try (ResultSet rSet = stmt.executeQuery(sqlContractCount)) {
                            while (rSet.next()) {
                                contractCount = rSet.getInt(1);
                            }
                        }
                        try (ResultSet rSet = stmt.executeQuery(sqlClaimCount)) {
                            while (rSet.next()) {
                                claimCount = rSet.getInt(1);
                            }
                        }
                        try (ResultSet rSet = stmt.executeQuery(sqlVOCount)) {
                            while (rSet.next()) {
                                voCount = rSet.getInt(1);
                            }
                        }
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(MobileReportApi.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (projectId != null) {

                Logger.getLogger(MobileReportApi.class.getName()).log(Level.INFO, "Using Project Id");

                //FormRowSet contractFormRows = comm.getFormRows("contractForm", "project_id", projectId);
                FormRowSet projectFormRows = comm.getFormRows("projectForm", "project_id", projectId);
                FormRowSet claimFormRows = comm.getFormRows("claimForm", "project_id", projectId);
                FormRowSet voFormRows = comm.getFormRows("voForm", "project_id", projectId);
                projectCount = projectFormRows.size();
                claimCount = claimFormRows.size();
                //contractCount = contractFormRows.size();
                voCount = voFormRows.size();

                String sqlContractCount = "SELECT Count(*) from app_fd_pfs_contract where c_project_id = '" + projectId + "'";
                if (isCompleted) {
                    sqlContractCount += " and c_status='complete'";
                }
                Logger.getLogger(MobileReportApi.class.getName()).log(Level.INFO, "********************contract query = {0}", sqlContractCount);

                DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
                try (Connection con = ds.getConnection()) {
                    try (Statement stmt = con.createStatement()) {
                        try (ResultSet rSet = stmt.executeQuery(sqlContractCount)) {
                            while (rSet.next()) {
                                contractCount = rSet.getInt(1);
                            }
                        }
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(MobileReportApi.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            responseObject.put("projectCount", projectCount);
            responseObject.put("claimCount", claimCount);
            responseObject.put("contractCount", contractCount);
            responseObject.put("voCount", voCount);
            out.println(responseObject);

        } catch (JSONException ex) {

            Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
