/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.*;
import java.io.IOException;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.joget.apps.form.model.FormRow;
import org.joget.common.Commons;
import org.joget.common.UserWorkflowHelper;

/**
 *
 * @author User
 */
public class UserProjectAssignAPI extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "User Project Assign API Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "User Project Assign API Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "User Project Assign API Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        session.setMaxInactiveInterval(60);
        Commons common = new Commons();

        if (common.performAPIAuthorization(request)) {// || common.performAuthenticate(request)) {

        } else {
            response.sendError(401);
            return;
        }

        performOperation(request, response);

    }

    private void performOperation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (PrintWriter out = response.getWriter()) {

            JSONObject responseObject = new JSONObject();

            StringBuilder jb = new StringBuilder();

            String line;
            try {
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null) {
                    jb.append(line);
                }
            } catch (IOException ex) {
                Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.SEVERE, null, ex);
                {
                    responseObject.put("status", false);
                    responseObject.put("message", "Invalid JSON or Request");
                }
                out.println(responseObject);
            }

            Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.INFO, "********************{0}", jb.toString());

            JSONObject jsonObject = new JSONObject(jb.toString());

            JSONArray usersJsonArray = null;
            String projectId = null;

            Commons comm = new Commons();
            String requestIncompleteMessage = null;
            while (true) {

                if (jsonObject.has("users")) {
                    usersJsonArray = jsonObject.getJSONArray("users");
                } else {
                    requestIncompleteMessage = "users array is required";
                    break;
                }

                if (jsonObject.has("projectId")) {
                    projectId = jsonObject.getString("projectId");
                } else {
                    requestIncompleteMessage = "projectId is required";
                    break;
                }
                break;
            }
            JSONArray responseArray = new JSONArray();
            UserWorkflowHelper userWorkflowHelper = new UserWorkflowHelper();
            if (requestIncompleteMessage == null) {

                for (int i = 0; i < usersJsonArray.length(); i++) {
                    JSONObject responseObjectArray = new JSONObject();

                    JSONObject jsonUserObject = usersJsonArray.getJSONObject(i);
                    String userName = jsonUserObject.getString("userName");
                    String userRole = jsonUserObject.getString("userRole");
                    String action = jsonUserObject.getString("action");

                    String departmentId = StringUtils.deleteWhitespace(userRole);
                    String uniqueId = projectId + userName + departmentId;
                    Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.INFO, "departmentId : {0}", departmentId);
                    Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.INFO, "username : {0}", userName);
                    if ("create".equalsIgnoreCase(action)) {
                        String organizationId = comm.getUserOrganization(userName);
                        boolean userStatusFlag = comm.addDepartmentToUser(uniqueId, userName, departmentId, organizationId);
                        //comm.assignDepartmentToUser(userName, departmentId, uniqueId);

                        responseObjectArray.put("userStatus", userStatusFlag);
                        responseObjectArray.put("message", "User Updated");

                        FormRow row = new FormRow();
                        row.setId(uniqueId);
                        row.setProperty("project_id", projectId);
                        row.setProperty("username", userName);
                        row.setProperty("department", userRole);

                        comm.insertForm("userDepartmentSelectionForm", row);
                    } else if ("delete".equalsIgnoreCase(action)) {
                        if (userWorkflowHelper.isUserPartOfRunningWorkflow(userName, projectId) && userWorkflowHelper.isProjectPartOfRunningWorkflow(projectId)) {
                            responseObjectArray.put("userStatus", false);

                            responseObjectArray.put("message", "User is part of Running flow");
                        } else {
                            boolean userStatusFlag = comm.deleteDepartmentToUser(uniqueId);
                            comm.unassignDepartmentToUser(userName, departmentId, uniqueId);

                            comm.updateWorkflowProjectVariables(userName, projectId);
                            responseObjectArray.put("userStatus", userStatusFlag);

                            responseObjectArray.put("message", "User Updated");

                            comm.delateFormData("userDepartmentSelectionForm", new String[]{uniqueId});
                        }
                    } else {
                        responseObjectArray.put("userStatus", false);

                        responseObjectArray.put("message", "Invalid Action Specified");

                    }
                    responseObjectArray.put("userName", userName);

                    responseArray.put(responseObjectArray);
                }
            } else {
                responseObject.put("status", false);
                responseObject.put("message", requestIncompleteMessage);
            }
            responseObject.put("users", responseArray);
            out.println(responseObject);

        } catch (JSONException ex) {

            Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String BASIC_AUTHENTICATION_REGEX = "Basic\\s";
    private static final String EMPTY_STRING = "";
    private static final String SEPARATOR = ":";
}
