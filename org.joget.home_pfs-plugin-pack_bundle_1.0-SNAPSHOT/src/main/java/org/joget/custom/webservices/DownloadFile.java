/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.apps.form.model.Element;
import org.joget.apps.form.model.FormData;
import org.joget.common.ContractCSVGenerator;
import org.joget.commons.util.SetupManager;
import org.joget.plugin.base.PluginWebSupport;

/**
 *
 * @author fahmi
 */
public class DownloadFile extends Element implements PluginWebSupport {

    private static final Logger logger = Logger.getLogger(DownloadFile.class.getName());

    @Override
    public String renderTemplate(FormData formData, Map dataModel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getName() {
        return " Download File";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String getDescription() {
        return " Download File";
    }

    @Override
    public String getLabel() {
        return " Download File";
    }

    @Override
    public String getClassName() {
        return getClass().getName();
    }

    @Override
    public String getPropertyOptions() {
        return null;
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");

        // reads input file from an absolute path
        String absolutePath = SetupManager.getBaseDirectory()
                + "/app_formuploads/pfs_contract/" + "/" + id + "/boq.csv";
        ContractCSVGenerator contractCSVGenerator = new ContractCSVGenerator();
        contractCSVGenerator.createContractCSV(id, absolutePath, false);
        File downloadFile = new File(absolutePath);
        FileInputStream inStream = new FileInputStream(downloadFile);

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String mimeType = fileNameMap.getContentTypeFor(absolutePath);

        if (mimeType == null) {
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream";
        }
        System.out.println("MIME type: " + mimeType);

        // modifies response
        response.setContentType(mimeType);
        response.setContentLength((int) downloadFile.length());

        // forces download
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        response.setHeader(headerKey, headerValue);

        // obtains response's output stream
        OutputStream outStream = response.getOutputStream();

        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }

        inStream.close();
        outStream.close();

    }
}
