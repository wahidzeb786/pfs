/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import java.io.IOException;
import org.joget.common.Commons;

/**
 *
 * @author User
 */
public class TestNotificationService extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "Test Notification Api Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "Test Notification Api Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "Test Notification Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        performOperation(request, response);

    }

    private void performOperation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userId = request.getParameter("userId");
        String title = request.getParameter("title");
        String body = request.getParameter("body");

        Commons commons = new Commons();

        commons.sendNotification(userId, userId, title, body, "test", "test.com", "projectId", "");

    }
}
