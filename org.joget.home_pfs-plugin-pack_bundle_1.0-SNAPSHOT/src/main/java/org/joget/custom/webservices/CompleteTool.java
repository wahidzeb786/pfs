/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.util.List;
import java.util.Map;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;
import org.joget.common.UserWorkflowHelper;
import org.joget.plugin.base.DefaultApplicationPlugin;
import org.joget.workflow.model.WorkflowAssignment;
import org.joget.workflow.model.WorkflowVariable;
import org.joget.workflow.model.service.WorkflowManager;

/**
 *
 * @author Jahan Zaib
 */
public class CompleteTool extends DefaultApplicationPlugin {

    @Override
    public Object execute(Map map) {
        WorkflowAssignment wfAssignment = (WorkflowAssignment) map.get("workflowAssignment");
        String activityId = wfAssignment.getActivityId();
        String processId = wfAssignment.getProcessId();
        String activityName = wfAssignment.getActivityName();
        System.out.println("" + wfAssignment.getActivityDefId());
        System.out.println("" + wfAssignment.getProcessDefId());

        System.out.println("" + wfAssignment.getParticipant());
        WorkflowManager wfManager = (WorkflowManager) AppUtil.getApplicationContext().getBean("workflowManager");

        String processName = wfAssignment.getProcessName();

        System.out.println("\n\n\nactivityId : " + activityId);
        System.out.println("processId : " + processId);
        System.out.println("processName : " + processName);
        System.out.println("activityName : " + activityName);

        List<WorkflowVariable> vars = wfAssignment.getProcessVariableList();

        for (WorkflowVariable var : vars) {
            System.out.println("" + var.getId() + " = " + var.getName() + " = " + var.getVal());
        }

        String status;
        String id;
        String contractId = null;
        String originalAmount = null;
        String total = null;
        String totalVoClaim = null;
        String projectId = null;
        String formId;
        String tableOrganize;
        String formIdOrganize;
        String type;
        String assignTo;
        String estimationTimeImpact = "0";
        String contractType = null;
        String parentContractId = null;
        Boolean isContractAmendData = false;
        Boolean isContractAmendBOQ = false;

        if (processName.contains("Process 1")) {
            contractType = AppUtil.processHashVariable("#form.pfs_contract.type#", wfAssignment, null, null);

            if ("amend_data".equalsIgnoreCase(contractType)) {
                parentContractId = AppUtil.processHashVariable("#form.pfs_contract.parent_contract_id#", wfAssignment, null, null);

                isContractAmendData = true;
            } else if ("amend_boq".equalsIgnoreCase(contractType)) {
                parentContractId = AppUtil.processHashVariable("#form.pfs_contract.parent_contract_id#", wfAssignment, null, null);
                isContractAmendBOQ = true;
            }
            status = AppUtil.processHashVariable("#form.pfs_contract.status#", wfAssignment, null, null);
            originalAmount = AppUtil.processHashVariable("#form.pfs_contract.original_amount#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_contract.project_id#", wfAssignment, null, null);
            contractId = id = AppUtil.processHashVariable("#form.pfs_contract.id#", wfAssignment, null, null);
            type = "contract";
            formId = "contractForm";
            tableOrganize = "app_fd_approval_cntrct_flow";

        } else if ("VO Process".equalsIgnoreCase(processName)) {
            status = AppUtil.processHashVariable("#form.pfs_vo.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_vo.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_vo.contract_id#", wfAssignment, null, null);
            total = AppUtil.processHashVariable("#form.pfs_vo.total#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_vo.project_id#", wfAssignment, null, null);
            estimationTimeImpact = AppUtil.processHashVariable("#form.pfs_vo.estimation_time_impact#", wfAssignment, null, null);

            type = "vo";
            formId = "voForm";
            tableOrganize = "app_fd_approval_flow_vos";
        } else {
            status = AppUtil.processHashVariable("#form.pfs_claim.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_claim.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_claim.contract_id#", wfAssignment, null, null);
            total = AppUtil.processHashVariable("#form.pfs_claim.total#", wfAssignment, null, null);
            totalVoClaim = AppUtil.processHashVariable("#form.pfs_claim.total_vo_claim#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_claim.project_id#", wfAssignment, null, null);

            type = "claim";
            formId = "claimForm";
            tableOrganize = "app_fd_approval_flow_claims";
        }

        System.out.println("Status : " + status);
        System.out.println("Id: " + id);
        Commons common = new Commons();
        UserWorkflowHelper workflowHelper = new UserWorkflowHelper();
        switch (type) {
            case "contract":
                if (projectId != null && originalAmount != null && !isContractAmendData) {
                    /*String sql = "UPDATE app_fd_pfs_project "
                    + "SET c_contracted_amount = (c_contracted_amount+" + originalAmount + ") "
                    + "WHERE c_project_id= '" + projectId + "'";
                    
                    System.out.println(" SQL Update Project: " + sql);
                    System.out.println(" Result : " + common.executeDatabaseQuery(sql));
                     */
                    workflowHelper.updateProjectAmounts("0", "0", "0", originalAmount, projectId);
                }
                if (isContractAmendData) {
                    if (contractId != null) {
                        FormRowSet rowSetContractOrignal = common.getFormRows("contractForm", "parent_contract_id", contractId);
                        if (rowSetContractOrignal.size() > 0) {
                            FormRow rowContractUpdate = rowSetContractOrignal.get(0);

                            rowContractUpdate.setProperty("type", "");
                            rowContractUpdate.setProperty("status", "complete-amendment-approved");
                            rowContractUpdate.setProperty("status_integration", "complete-amendment-approved");
                            rowContractUpdate.setProperty("archive", "true");
                            common.insertForm("contractForm", rowContractUpdate);
                        }
                        String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                                + "SET c_type=''"
                                + " WHERE id= '" + contractId + "'";

                        System.out.println(" SQL Update Contract: " + sqlUpdateProject);

                        System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
                    }
                } else if (isContractAmendBOQ) {
                    if (parentContractId != null) {
                        FormRowSet rowSetContractOrignal = common.getFormRows("contractForm", "id", parentContractId);
                        if (rowSetContractOrignal.size() > 0) {
                            FormRow rowContractUpdate = rowSetContractOrignal.get(0);

                            rowContractUpdate.setProperty("type", "");
                            rowContractUpdate.setProperty("status", "complete-amendment-approved");
                            rowContractUpdate.setProperty("status_integration", "complete-amendment-approved");
                            rowContractUpdate.setProperty("archive", "true");
                            common.insertForm("contractForm", rowContractUpdate);
                        }
                        String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                                + "SET c_type=''"
                                + " WHERE id= '" + id + "'";

                        System.out.println(" SQL Update Contract: " + sqlUpdateProject);

                        System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
                    }
                }
                break;
            case "claim":
                /*FormRowSet rowSetClaimBoQ = common.getFormRows("boqEditForm", "claim_id", id);
                for (FormRow rowClaimBoQ : rowSetClaimBoQ) {
                contractId = rowClaimBoQ.getProperty("contract_id");
                String itemId = rowClaimBoQ.getProperty("item_id");
                String quantity = rowClaimBoQ.getProperty("quantity");

                String sql = "UPDATE app_fd_pfs_boq "
                + "SET c_quantity_available = (c_quantity_available-" + quantity + "), "
                + "c_quantity_last_month = (c_quantity_last_month+" + quantity + ") "
                + "WHERE c_contract_id='" + contractId + "' AND "
                + "c_item_id= '" + itemId + "'";
                
                System.out.println(" SQL : " + sql);
                System.out.println(" Result : " + common.executeDatabaseQuery(sql));
                
                
                }*/
                if (total == null) {
                    total = "0";
                }
                if (totalVoClaim == null) {
                    totalVoClaim = "0";
                }
                if (projectId != null) {
                    /*  String sqlUpdateProject = "UPDATE app_fd_pfs_project "
                    + "SET c_spent_amount = (c_spent_amount+" + total + ") "
                    + "WHERE c_project_id= '" + projectId + "'";
                    
                    System.out.println(" SQL Update Project: " + sqlUpdateProject);
                    System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
                     */

                    workflowHelper.updateProjectAmounts("0", totalVoClaim, total, "0", projectId);

                }
                if (contractId != null) {
                    String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                            + "SET c_total_claim_amount_completed = (c_total_claim_amount_completed+" + common.getDoubleValue(total) + "),"
                            + "c_total_claim_vo_amount_completed = (c_total_claim_vo_amount_completed+" + common.getDoubleValue(totalVoClaim) + ") "
                            + "WHERE id= '" + contractId + "'";

                    System.out.println(" SQL Update Contract Claim: " + sqlUpdateProject);
                    System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
                }
                //c_claimed_vo_amount
                break;
            case "vo":
                if (contractId != null && total != null) {
                    String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                            + "SET c_total_vo_amount_completed = (c_total_vo_amount_completed+" + common.getDoubleValue(total) + "),"
                            + "c_extension_of_time = (c_extension_of_time+" + estimationTimeImpact + ")"
                            + " WHERE id= '" + contractId + "'";

                    System.out.println(" SQL Update Contract VO: " + sqlUpdateProject);
                    System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
                    String updateBoqComplete = "UPDATE app_fd_pfs_boq_vo set c_status='complete' WHERE "
                            + "c_vo_id='" + id + "'";
                    System.out.println(" SQL Update VO Boq: " + updateBoqComplete);
                    System.out.println(" Result : " + common.executeDatabaseQuery(updateBoqComplete));

                    /*                String sqlUpdateTotalVOAmount = "UPDATE app_fd_pfs_project "
                    + "SET c_vo_amount = (c_vo_amount+" + total + ") "
                    + "WHERE c_project_id= '" + projectId + "'";
                    System.out.println(" SQL Update VO Project: " + sqlUpdateTotalVOAmount);
                    System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateTotalVOAmount));
                     */
                    workflowHelper.updateProjectAmounts(total, "0", "0", "0", projectId);
                }
                break;
            default:
                break;
        }
        FormRow row = new FormRow();
        row.setId(id);

        row.setProperty("status", "complete");

        common.insertForm(formId, row);

        return null;
    }

    public String getName() {
        return "Complete Tool";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "Complete Tool;";
    }

    public String getLabel() {
        return "Complete Tool";
    }

    public String getClassName() {
        return this.getClass().getName();
    }

    public String getPropertyOptions() {
        return AppUtil.readPluginResource(getClass().getName(), "/properties/WizardWebServiceTool.json", null, true, "message/WizardWebServiceTool");
    }
}
