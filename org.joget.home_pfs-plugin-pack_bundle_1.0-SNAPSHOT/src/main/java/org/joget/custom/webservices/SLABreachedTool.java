/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;
import org.joget.commons.util.UuidGenerator;
import org.joget.directory.model.User;
import org.joget.plugin.base.DefaultApplicationPlugin;
import org.joget.workflow.model.WorkflowAssignment;
import org.joget.workflow.model.WorkflowVariable;
import org.joget.workflow.model.service.WorkflowManager;

/**
 *
 * @author Jahan Zaib
 */
public class SLABreachedTool extends DefaultApplicationPlugin {
    
    @Override
    public Object execute(Map map) {
        
        WorkflowAssignment wfAssignment = (WorkflowAssignment) map.get("workflowAssignment");
        String activityId = wfAssignment.getActivityId();
        String processId = wfAssignment.getProcessId();
        String activityName = wfAssignment.getActivityName();
        System.out.println("" + wfAssignment.getActivityDefId());
        System.out.println("" + wfAssignment.getProcessDefId());
        
        System.out.println("" + wfAssignment.getParticipant());
        WorkflowManager wfManager = (WorkflowManager) AppUtil.getApplicationContext().getBean("workflowManager");
        
        String processName = wfAssignment.getProcessName();
        
        System.out.println("\n\n\nactivityId : " + activityId);
        System.out.println("processId : " + processId);
        System.out.println("processName : " + processName);
        System.out.println("activityName : " + activityName);
        
        List<WorkflowVariable> vars = wfAssignment.getProcessVariableList();
        
        for (WorkflowVariable var : vars) {
            System.out.println("" + var.getId() + " = " + var.getName() + " = " + var.getVal());
        }
        
        String status;
        String id;
        String formId;
        String type;
        String assignTo;
        String workflowTable;
        if (processName.contains("Process 1")) {
            status = AppUtil.processHashVariable("#form.pfs_contract.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_contract.id#", wfAssignment, null, null);
            assignTo = AppUtil.processHashVariable("#form.pfs_contract.assign_to#", wfAssignment, null, null);
            type = "contract";
            formId = "contractForm";
            workflowTable = "app_fd_approval_cntrct_flow";
            
        } else if ("VO Process".equalsIgnoreCase(processName)) {
            status = AppUtil.processHashVariable("#form.pfs_vo.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_vo.id#", wfAssignment, null, null);
            assignTo = AppUtil.processHashVariable("#form.pfs_vo.assign_to#", wfAssignment, null, null);
            type = "vo";
            formId = "voForm";
            workflowTable = "app_fd_approval_flow_vo_vo";
            
        } else {
            status = AppUtil.processHashVariable("#form.pfs_claim.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_claim.id#", wfAssignment, null, null);
            assignTo = AppUtil.processHashVariable("#form.pfs_claim.assign_to#", wfAssignment, null, null);
            type = "claim";
            formId = "claimForm";
            workflowTable = "app_fd_pfs_claim";
        }
        if ("contract".equals(type)) {
            updateAssignContract(id);
        } else if ("claim".equals(type)) {
            updateAssignClaim(id);
        } else if ("vo".equals(type)) {
            updateAssignVO(id);
        }
        Commons commons = new Commons();
        
        System.out.println("Status : " + status);
        System.out.println("Id: " + id);
        FormRow row = new FormRow();
        row.setId(id);
        String remarks;
        remarks = "Process is rejected because user :" + assignTo + " did not approved in required duration";
        
        row.setProperty("username", "");
        row.setProperty("fullname", "");
        
        row.setProperty("status", "reject");
        row.setProperty("status_integration", "reject");
        
        wfManager.activityVariable(activityId, "status", "reject");
        
        wfManager.activityVariable(activityId, "assign_to", "-");
        row.setProperty("assign_to", "-");
        
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, " userId " + assignTo);
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, " remarks " + remarks);
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, " status " + status);
        
        User user = commons.getUser(assignTo);
        String username;
        if (user != null) {
            username = user.getFirstName() + " " + user.getLastName();
        } else {
            username = "";
        }
        UuidGenerator uuid = UuidGenerator.getInstance();
        String key = uuid.getUuid();
        
        commons.insertAuditForm(key, id, assignTo, remarks, username, "reject_sla", "workflow");
        
        commons.insertForm(formId, row);

        /*
        NOTIFICATION_DURATION_COMPLETED
         */
        return null;
    }
    
    private void updateAssignVO(String voId) {
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "UPDATE app_fd_approval_flow_vo_vo set c_status='SLA Breached' where c_vo_id='" + voId + "' AND c_status='Assign'";
            try (Statement stmt = con.createStatement()) {
                
                System.out.println(" Update VO Result : " + stmt.executeUpdate(query));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowBrain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void updateAssignClaim(String claimId) {
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "UPDATE app_fd_approval_flow_claima set c_status='SLA Breached' where c_claim_id='" + claimId + "' AND c_status='Assign'";
            try (Statement stmt = con.createStatement()) {
                
                System.out.println(" Update Claim Result : " + stmt.executeUpdate(query));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowBrain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void updateAssignContract(String contractId) {
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "UPDATE app_fd_approval_cntrct_flow set c_status='SLA Breached' where c_contract_id='" + contractId + "' AND c_status='Assign'";
            try (Statement stmt = con.createStatement()) {
                System.out.println(" Update Contract Result : " + stmt.executeUpdate(query));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowBrain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getName() {
        return "SLA Breached Tool";
    }
    
    public String getVersion() {
        return "1.0";
    }
    
    public String getDescription() {
        return "SLA Breached Tool";
    }
    
    public String getLabel() {
        return "SLA Breached Tool";
    }
    
    public String getClassName() {
        return this.getClass().getName();
    }
    
    public String getPropertyOptions() {
        return AppUtil.readPluginResource(getClass().getName(), "/properties/WizardWebServiceTool.json", null, true, "message/WizardWebServiceTool");
    }
}
