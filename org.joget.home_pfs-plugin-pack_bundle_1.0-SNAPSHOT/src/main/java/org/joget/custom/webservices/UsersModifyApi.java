/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.*;
import java.io.IOException;
import org.joget.common.Commons;
import org.joget.common.UserWorkflowHelper;
import org.joget.directory.model.User;

/**
 *
 * @author User
 */
public class UsersModifyApi extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "Users Modify Api Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "Users Modify Api Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "Users Modify Api Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Commons common = new Commons();

        if (common.performAPIAuthorization(request)) {// || common.performAuthenticate(request)) {

        } else {
            response.sendError(401);
            return;
        }

        performOperation(request, response);

    }

    private void performOperation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (PrintWriter out = response.getWriter()) {
            JSONObject responseObject = new JSONObject();

            /*String authHeader = request.getHeader(AUTHORIZATION_HEADER_NAME);
            
            if (authHeader != null) {
                authHeader = authHeader.replaceFirst(BASIC_AUTHENTICATION_REGEX, EMPTY_STRING);
                authHeader = new String(Base64.getDecoder().decode(authHeader));

                if (StringUtils.countMatches(authHeader, SEPARATOR) != 1) {
                    {
                        responseObject.put("status", false);
                        responseObject.put("message", "API Login Fail");
                    }
                    out.println(responseObject);
                    return;
                }
            } else {
                {
                    responseObject.put("status", false);
                    responseObject.put("message", "API Login Fail");
                }
                out.println(responseObject);
                return;
            }*/
            StringBuilder jb = new StringBuilder();
            JSONObject jsonObject = null;
            String line = null;
            try {
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null) {
                    jb.append(line);
                }
                jsonObject = new JSONObject(jb.toString());
            } catch (IOException | JSONException ex) {
                Logger.getLogger(UsersModifyApi.class.getName()).log(Level.SEVERE, null, ex);
                {
                    responseObject.put("status", false);
                    responseObject.put("message", "Invalid JSON or Request");
                }
                out.println(responseObject);
            }
            Logger.getLogger(UsersModifyApi.class.getName()).log(Level.INFO, "********************{0}", jb.toString());

            JSONArray usersJsonArray = null;

            String requestIncompleteMessage = null;

            if (jsonObject.has("users")) {
                usersJsonArray = jsonObject.getJSONArray("users");
            } else {
                requestIncompleteMessage = "users array is required";
            }

            JSONArray responseArray = new JSONArray();

            if (requestIncompleteMessage == null && usersJsonArray != null) {

                Commons commons = new Commons();
                UserWorkflowHelper userWorkflowHelper = new UserWorkflowHelper();

                for (int i = 0; i < usersJsonArray.length(); i++) {
                    JSONObject responseObjectArray = new JSONObject();

                    JSONObject jsonUserObject = usersJsonArray.getJSONObject(i);
                    String userName = jsonUserObject.getString("userName");
                    String status = jsonUserObject.getString("status");

                    Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.INFO, "username : {0}", userName);
                    if ("remove".equals(status)) {
                        if (!userWorkflowHelper.isUserPartOfRunningWorkflow(userName)) {
                            if (commons.removeUser(userName)) {
                                responseObjectArray.put("userStatus", true);
                                responseObjectArray.put("reason", "User Removed");
                                commons.updateWorkflowProjectVariables(userName, null);

                            } else {
                                responseObjectArray.put("userStatus", false);
                                responseObjectArray.put("reason", "User is  Already Removed");
                            }
                        } else {
                            responseObjectArray.put("userStatus", false);
                            responseObjectArray.put("reason", "User is part of Running flow");
                        }

                    } else if ("inactive".equals(status)) {
                        if (!userWorkflowHelper.isUserPartOfRunningWorkflow(userName)) {
                            User user = commons.getUser(userName);
                            if (user != null) {
                                user.setActive(0);
                            }
                            if (user != null && commons.updateUser(user)) {
                                responseObjectArray.put("userStatus", true);
                                responseObjectArray.put("reason", "User Inactive");
                            } else {
                                responseObjectArray.put("userStatus", false);
                                responseObjectArray.put("reason", "Unable to update User");
                            }
                        } else {
                            responseObjectArray.put("userStatus", false);
                            responseObjectArray.put("reason", "User is part of Running flow");
                        }
                    } else if ("active".equals(status)) {
                        User user = commons.getUser(userName);
                        if (user != null) {
                            user.setActive(1);
                        }
                        if (user != null && commons.updateUser(user)) {
                            responseObjectArray.put("userStatus", true);
                            responseObjectArray.put("reason", "User Active");
                        } else {
                            responseObjectArray.put("userStatus", false);
                            responseObjectArray.put("reason", "Unable to update User");
                        }
                    }
                    responseObjectArray.put("userName", userName);

                    responseArray.put(responseObjectArray);
                }
                responseObject.put("status", true);
            } else {
                responseObject.put("status", false);
                responseObject.put("message", requestIncompleteMessage);
            }
            responseObject.put("users", responseArray);
            out.println(responseObject);

        } catch (JSONException ex) {

            Logger.getLogger(UserProjectAssignAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
