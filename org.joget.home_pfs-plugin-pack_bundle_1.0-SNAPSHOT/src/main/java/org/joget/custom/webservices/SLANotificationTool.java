/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

import java.util.List;
import java.util.Map;
import org.joget.apps.app.service.AppUtil;
import org.joget.common.Commons;
import org.joget.plugin.base.DefaultApplicationPlugin;
import org.joget.workflow.model.WorkflowAssignment;
import org.joget.workflow.model.WorkflowVariable;
import org.joget.workflow.model.service.WorkflowManager;

/**
 *
 * @author Jahan Zaib
 */
public class SLANotificationTool extends DefaultApplicationPlugin {

    @Override
    public Object execute(Map map) {
        WorkflowAssignment wfAssignment = (WorkflowAssignment) map.get("workflowAssignment");
        String activityId = wfAssignment.getActivityId();
        String processId = wfAssignment.getProcessId();
        String activityName = wfAssignment.getActivityName();
        System.out.println("" + wfAssignment.getActivityDefId());
        System.out.println("" + wfAssignment.getProcessDefId());

        System.out.println("" + wfAssignment.getParticipant());
        WorkflowManager wfManager = (WorkflowManager) AppUtil.getApplicationContext().getBean("workflowManager");

        String processName = wfAssignment.getProcessName();

        System.out.println("\n\n\nactivityId : " + activityId);
        System.out.println("processId : " + processId);
        System.out.println("processName : " + processName);
        System.out.println("activityName : " + activityName);

        List<WorkflowVariable> vars = wfAssignment.getProcessVariableList();

        for (WorkflowVariable var : vars) {
            System.out.println("" + var.getId() + " = " + var.getName() + " = " + var.getVal());
        }

        String status;
        String id;
        String contractId = null;
        String createdBy = null;
        String formId;
        String tableOrganize;
        String formIdOrganize;
        String type;
        String assignTo;
        String projectId = null;

        if (processName.contains("Process 1")) {
            status = AppUtil.processHashVariable("#form.pfs_contract.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_contract.id#", wfAssignment, null, null);
            createdBy = AppUtil.processHashVariable("#form.pfs_contract.created_by#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_contract.project_id#", wfAssignment, null, null);

            type = "contract";
            formId = "contractForm";
            tableOrganize = "app_fd_approval_cntrct_flow";

        } else if ("VO Process".equalsIgnoreCase(processName)) {
            status = AppUtil.processHashVariable("#form.pfs_vo.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_vo.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_vo.contract_id#", wfAssignment, null, null);
            createdBy = AppUtil.processHashVariable("#form.pfs_vo.created_by#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_vo.project_id#", wfAssignment, null, null);

            type = "vo";
            formId = "voForm";
            tableOrganize = "app_fd_approval_flow_vos";
        } else {
            status = AppUtil.processHashVariable("#form.pfs_claim.status#", wfAssignment, null, null);
            id = AppUtil.processHashVariable("#form.pfs_claim.id#", wfAssignment, null, null);
            contractId = AppUtil.processHashVariable("#form.pfs_claim.contract_id#", wfAssignment, null, null);
            createdBy = AppUtil.processHashVariable("#form.pfs_claim.created_by#", wfAssignment, null, null);
            projectId = AppUtil.processHashVariable("#form.pfs_claim.project_id#", wfAssignment, null, null);

            type = "claim";
            formId = "claimForm";
            tableOrganize = "app_fd_approval_flow_claims";
        }

        System.out.println("Status : " + status);
        System.out.println("Id: " + id);
        System.out.println("CreatedBy: " + createdBy);
        Commons commons = new Commons();

        commons.sendNotification(createdBy, createdBy, "A task has been assigned to you for more information", "A task has been assigned to you for more information", type, "URL", projectId, status);
        return null;
    }

    @Override
    public String getName() {
        return "SLA Notification Tool";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public String getDescription() {
        return "SLA Notification Tool;";
    }

    @Override
    public String getLabel() {
        return "SLA Notification Tool";
    }

    @Override
    public String getClassName() {
        return this.getClass().getName();
    }

    @Override
    public String getPropertyOptions() {
        return AppUtil.readPluginResource(getClass().getName(), "/properties/WizardWebServiceTool.json", null, true, "message/WizardWebServiceTool");
    }
}
