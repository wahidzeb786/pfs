/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.custom.webservices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.joget.plugin.base.PluginWebSupport;
import org.json.*;
import java.io.IOException;
import org.apache.commons.lang.StringUtils;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;
import org.joget.common.UserWorkflowHelper;

/**
 *
 * @author User
 */
public class PFSCleanupAPI extends DefaultPlugin implements PluginWebSupport {

    public String getName() {
        return "PFS Cleanup API Plugin";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getDescription() {
        return "PFS Cleanup API Plugin";
    }

    public PluginProperty[] getPluginProperties() {
        return null; // not relevant
    }

    public Object execute(Map properties) {
        return null; // not relevant
    }

    public String getLabel() {
        return "PFS Cleanup API Plugin";
    }

    public String getClassName() {
        return getClass().getName();
    }

    public String getPropertyOptions() {
        return "";
    }

    @Override
    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Commons common = new Commons();

        if (common.performAPIAuthorization(request)) {// || common.performAuthenticate(request)) {

        } else {
            response.sendError(401);
            return;
        }

        performOperation(request, response);

    }

    private void performOperation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (PrintWriter out = response.getWriter()) {
            JSONObject responseObject = new JSONObject();

            StringBuilder jb = new StringBuilder();

            String line = null;
            try {
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null) {
                    jb.append(line);
                }
            } catch (Exception ex) {
                Logger.getLogger(PFSCleanupAPI.class.getName()).log(Level.SEVERE, null, ex);
                {
                    responseObject.put("status", false);
                    responseObject.put("message", "Invalid JSON or Request");
                }
                out.println(responseObject);
            }

            JSONObject jsonObject = new JSONObject(jb.toString());

            String operation = null;
            String projectId = null;

            String requestIncompleteMessage = null;
            while (true) {

                if (jsonObject.has("operation")) {
                    operation = jsonObject.getString("operation");
                } else {
                    requestIncompleteMessage = "operation is required";
                    break;
                }

                if (jsonObject.has("projectId")) {
                    projectId = jsonObject.getString("projectId");
                } else {
                    requestIncompleteMessage = "projectId is required";
                    break;
                }

                break;
            }
            UserWorkflowHelper userWorkflowHelper = new UserWorkflowHelper();
            if (requestIncompleteMessage == null) {
                Commons commons = new Commons();
                Logger.getLogger(PFSCleanupAPI.class.getName()).log(Level.INFO, "projectId : {0}", projectId);
                Logger.getLogger(PFSCleanupAPI.class.getName()).log(Level.INFO, "operation : {0}", operation);
                if (userWorkflowHelper.isProjectPartOfRunningWorkflow(projectId)) {
                    responseObject.put("status", false);
                    responseObject.put("message", "Project is part of running flow");
                } else if ("delete".equalsIgnoreCase(operation)) {
                    commons.delateFormData("projectForm", "project_id", new String[]{projectId});
                    commons.delateFormData("userForm", "project_id", new String[]{projectId});
                    commons.delateFormData("fundingBudgetForm", "project_id", new String[]{projectId});
                    FormRowSet rowSetUsers = commons.getFormRows("userDepartmentSelectionForm", "project_id", new String[]{projectId});
                    for (FormRow rowUser : rowSetUsers) {
                        commons.deleteDepartmentToUser(rowUser.getId());
                    }
                    commons.delateFormData("userDepartmentSelectionForm", rowSetUsers);
                    commons.delateFormData("approvalFlowClaimContractForm", "project_id", new String[]{projectId});
                    commons.delateFormData("approvalFlowClaimForm", "project_id", new String[]{projectId});
                    commons.delateFormData("approvalFlowContractForm", "project_id", new String[]{projectId});
                    commons.delateFormData("approvalFlowForm", "project_id", new String[]{projectId});
                    commons.delateFormData("approvalFlowVOContractForm", "project_id", new String[]{projectId});
                    commons.delateFormData("approvalFlowVOForm", "project_id", new String[]{projectId});
                    commons.delateFormData("budgetForm", "project_id", new String[]{projectId});
                    commons.delateFormData("fundingForm", "project_id", new String[]{projectId});
                    commons.delateFormData("pfsFundingDataForm", "project_id", new String[]{projectId});
                    commons.delateFormData("claimForm", "project_id", new String[]{projectId});
                    commons.delateFormData("voForm", "project_id", new String[]{projectId});
                    FormRowSet rowSetContract = commons.getFormRows("contractForm", "project_id", new String[]{projectId});
                    for (FormRow rowContract : rowSetContract) {
                        commons.delateFormData("boqForm", "contract_id", new String[]{rowContract.getId()});
                    }
                    commons.delateFormData("contractForm", "project_id", new String[]{projectId});
                    responseObject.put("status", true);

                    responseObject.put("message", "Cleanup Completed");
                } else if ("archive".equalsIgnoreCase(operation) || "restore".equalsIgnoreCase(operation)) {
                    String status;
                    if ("archive".equalsIgnoreCase(operation)) {
                        status = "true";
                    } else {
                        status = "";
                    }
                    FormRowSet rowSetProject = commons.getFormRows("projectForm", "project_id", new String[]{projectId});
                    for (FormRow rowProject : rowSetProject) {
                        rowProject.put("archive", status);
                        commons.insertForm("projectForm", rowProject);
                    }
                    FormRowSet rowSetClaim = commons.getFormRows("claimForm", "project_id", new String[]{projectId});
                    for (FormRow rowClaim : rowSetClaim) {
                        rowClaim.put("archive", status);
                        commons.insertForm("claimForm", rowClaim);
                    }
                    FormRowSet rowSetVO = commons.getFormRows("voForm", "project_id", new String[]{projectId});
                    for (FormRow rowVO : rowSetVO) {
                        rowVO.put("archive", status);
                        commons.insertForm("voForm", rowVO);
                    }
                    FormRowSet rowSetContract = commons.getFormRows("contractForm", "project_id", new String[]{projectId});
                    for (FormRow rowContract : rowSetContract) {
                        rowContract.put("archive", status);
                        commons.insertForm("contractForm", rowContract);
                    }
                    responseObject.put("status", true);
                    responseObject.put("message", "Cleanup Completed");
                }
            } else {
                responseObject.put("status", false);
                responseObject.put("message", requestIncompleteMessage);
            }
            out.println(responseObject);

        } catch (JSONException ex) {
            Logger.getLogger(PFSCleanupAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
