package org.joget.home;

import java.util.ArrayList;
import java.util.Collection;
import org.joget.common.AmendContractStoreBinder;
import org.joget.common.AuditTrailStoreBinder;
import org.joget.common.TestStoreBinder;
import org.joget.custom.webservices.AbortRunningProcess;
import org.joget.custom.webservices.ApprovedTool;
import org.joget.custom.webservices.CSVProcessorTool;
import org.joget.custom.webservices.CalculationTool;
import org.joget.custom.webservices.CompleteTool;
import org.joget.custom.webservices.DownloadCustomFile;
import org.joget.custom.webservices.DownloadFile;
import org.joget.custom.webservices.GenericCUAPI;
import org.joget.custom.webservices.JsonApiPlugin;
import org.joget.custom.webservices.MobileReportApi;
import org.joget.custom.webservices.PFSCleanupAPI;
import org.joget.custom.webservices.ProcessBeginTool;
import org.joget.custom.webservices.RejectTool;
import org.joget.custom.webservices.SLABreachedTool;
import org.joget.custom.webservices.SLANotificationTool;
import org.joget.custom.webservices.TestNotificationService;
import org.joget.custom.webservices.UserProjectAssignAPI;
import org.joget.custom.webservices.UserRegistrationApi;
import org.joget.custom.webservices.UsersModifyApi;
import org.joget.custom.webservices.UsersRemoveApi;
import org.joget.custom.webservices.WorkflowBrain;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

    protected Collection<ServiceRegistration> registrationList;

    public void start(BundleContext context) {
        registrationList = new ArrayList<ServiceRegistration>();

        //Register plugin here
        // registrationList.add(context.registerService(CreateUserStoreBinder.class.getName(), new CreateUserStoreBinder(), null));
        // registrationList.add(context.registerService(VerifyEmailService.class.getName(), new VerifyEmailService(), null));
        // registrationList.add(context.registerService(NotifyOrderPushNotifications.class.getName(), new NotifyOrderPushNotifications(), null));
        // registrationList.add(context.registerService(StripPaymentGateway.class.getName(), new StripPaymentGateway(), null));
        //registrationList.add(context.registerService(CustomMultirowFormBinder.class.getName(), new CustomMultirowFormBinder(), null));
        //registrationList.add(context.registerService(JsonImagePlugin.class.getName(), new JsonImagePlugin(), null));
        //registrationList.add(context.registerService(JsonSaloonRegistrationPlugin.class.getName(), new JsonSaloonRegistrationPlugin(), null));
        //registrationList.add(context.registerService(UpdateSaloonProfile.class.getName(), new UpdateSaloonProfile(), null));
        registrationList.add(context.registerService(JsonApiPlugin.class.getName(), new JsonApiPlugin(), null));
        //registrationList.add(context.registerService(JsonCudApiPlugin.class.getName(), new JsonCudApiPlugin(), null));
        //registrationList.add(context.registerService(AuthenticateUser.class.getName(), new AuthenticateUser(), null));
        registrationList.add(context.registerService(GenericCUAPI.class.getName(), new GenericCUAPI(), null));
        registrationList.add(context.registerService(UserRegistrationApi.class.getName(), new UserRegistrationApi(), null));
        registrationList.add(context.registerService(AuditTrailStoreBinder.class.getName(), new AuditTrailStoreBinder(), null));
        registrationList.add(context.registerService(UserProjectAssignAPI.class.getName(), new UserProjectAssignAPI(), null));
        registrationList.add(context.registerService(UsersRemoveApi.class.getName(), new UsersRemoveApi(), null));
        //registrationList.add(context.registerService(UserProjectUnassignAPI.class.getName(), new UserProjectUnassignAPI(), null));
        registrationList.add(context.registerService(ProcessBeginTool.class.getName(), new ProcessBeginTool(), null));
        registrationList.add(context.registerService(WorkflowBrain.class.getName(), new WorkflowBrain(), null));
        registrationList.add(context.registerService(ApprovedTool.class.getName(), new ApprovedTool(), null));
        registrationList.add(context.registerService(SLABreachedTool.class.getName(), new SLABreachedTool(), null));
        registrationList.add(context.registerService(SLANotificationTool.class.getName(), new SLANotificationTool(), null));
        registrationList.add(context.registerService(CompleteTool.class.getName(), new CompleteTool(), null));
        registrationList.add(context.registerService(RejectTool.class.getName(), new RejectTool(), null));
        registrationList.add(context.registerService(CalculationTool.class.getName(), new CalculationTool(), null));
        registrationList.add(context.registerService(PFSCleanupAPI.class.getName(), new PFSCleanupAPI(), null));
        registrationList.add(context.registerService(MobileReportApi.class.getName(), new MobileReportApi(), null));
        registrationList.add(context.registerService(UsersModifyApi.class.getName(), new UsersModifyApi(), null));
        registrationList.add(context.registerService(TestNotificationService.class.getName(), new TestNotificationService(), null));
        registrationList.add(context.registerService(AbortRunningProcess.class.getName(), new AbortRunningProcess(), null));
        registrationList.add(context.registerService(DownloadFile.class.getName(), new DownloadFile(), null));
        registrationList.add(context.registerService(DownloadCustomFile.class.getName(), new DownloadCustomFile(), null));
        registrationList.add(context.registerService(CSVProcessorTool.class.getName(), new CSVProcessorTool(), null));
        registrationList.add(context.registerService(AmendContractStoreBinder.class.getName(), new AmendContractStoreBinder(), null));
        registrationList.add(context.registerService(TestStoreBinder.class.getName(), new TestStoreBinder(), null));
        //registrationList.add(context.registerService(RequestJob.class.getName(), new RequestJob(), null));
        //registrationList.add(context.registerService(AcceptJob.class.getName(), new AcceptJob(), null));
        //    registrationList.add(context.registerService(PickingUpJob.class.getName(), new PickingUpJob(), null));
        //registrationList.add(context.registerService(OnTheWayJob.class.getName(), new OnTheWayJob(), null));
        //registrationList.add(context.registerService(DeliveredItemJob.class.getName(), new DeliveredItemJob(), null));
        //registrationList.add(context.registerService(UpdateJobPaid.class.getName(), new UpdateJobPaid(), null));
        //registrationList.add(context.registerService(RateJob.class.getName(), new RateJob(), null));
        //registrationList.add(context.registerService(WebMessage.class.getName(), new WebMessage(), null));
        //registrationList.add(context.registerService(SubmitChatMessage.class.getName(), new SubmitChatMessage(), null));
        //registrationList.add(context.registerService(SendMessage.class.getName(), new SendMessage(), null));
        //registrationList.add(context.registerService(JsonMapLocationPlugin.class.getName(), new JsonMapLocationPlugin(), null));
        //registrationList.add(context.registerService(BroadcastMessage.class.getName(), new BroadcastMessage(), null));
        //registrationList.add(context.registerService(CreateChatMessage.class.getName(), new CreateChatMessage(), null));
        //registrationList.add(context.registerService(OwntimePaymentStoreBinder.class.getName(), new OwntimePaymentStoreBinder(), null));
        //registrationList.add(context.registerService(TopupWebService.class.getName(), new TopupWebService(), null));
        //registrationList.add(context.registerService(TopupStoreBinder.class.getName(), new TopupStoreBinder(), null));
        //registrationList.add(context.registerService(CountInventorySize.class.getName(), new CountInventorySize(), null));
        //registrationList.add(context.registerService(PostJob.class.getName(), new PostJob(), null));

    }

    public void stop(BundleContext context) {
        for (ServiceRegistration registration : registrationList) {
            registration.unregister();
        }
    }
}
