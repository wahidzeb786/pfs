/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

/**
 *
 * @author JAHANZAIB
 */
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joget.apps.form.lib.WorkflowFormBinder;
import org.joget.apps.form.model.Element;
import org.joget.apps.form.model.FormData;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;
import org.joget.commons.util.UuidGenerator;
import org.joget.custom.webservices.AbortRunningProcess;

/**
 *
 * @author Jahan Zaib
 */
public class AmendContractStoreBinder extends WorkflowFormBinder {

    @Override
    public FormRowSet store(Element element, FormRowSet rowSet, FormData formData) {

        Commons common = new Commons();

        String key = common.getUUID();
        FormRow rowContractAmend = rowSet.get(0);
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, rowContractAmend.toString());
        String orignalContractId = rowContractAmend.getProperty("orignal_contract_id");
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Orignal Contract UUID: {0}", orignalContractId);

        String type = rowContractAmend.getProperty("type");

        if ("amend_boq".equals(type)) {
            FormRowSet rowSetContractOrignal = common.getFormRows("contractForm", "id", orignalContractId);
            if (rowSetContractOrignal.size() > 0) {
                FormRow rowContractUpdate = rowSetContractOrignal.get(0);
                Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Orignal Contract Data: {0}", rowContractUpdate.toString());

                rowContractUpdate.setId(key);
                rowContractUpdate.setProperty("status", "complete-amendment-inprogress");
                rowContractUpdate.setProperty("status_integration", "complete-amendment-inprogress");
                common.insertForm("contractForm", rowContractUpdate);
            }

            ProcessReject abortProcess = new ProcessReject();

            abortProcess.rejectProcess(orignalContractId);
            rowContractAmend.put("parent_contract_id", key);

            FormRowSet rowSetBoq = common.getFormRows("boqForm", "contract_id", orignalContractId);
            for (FormRow rowBoq : rowSetBoq) {
                rowBoq.setId(common.getUUID());
                rowBoq.setProperty("contract_id", key);
                common.insertForm("boqForm", rowBoq);
            }

            FormRowSet rowContractFlow = common.getFormRows("approvalFlowContractForm", "contract_id", orignalContractId);
            for (FormRow row : rowContractFlow) {
                row.setId(common.getUUID());
                row.setProperty("contract_id", key);
                common.insertForm("approvalFlowContractForm", row);
            }

            FormRowSet rowClaimContractFlow = common.getFormRows("approvalFlowClaimContractForm",
                    "contract_id", orignalContractId);
            for (FormRow row : rowClaimContractFlow) {
                row.setId(common.getUUID());
                row.setProperty("contract_id", key);
                common.insertForm("approvalFlowClaimContractForm", row);
            }

            FormRowSet rowVOContractFlow = common.getFormRows("approvalFlowVOContractForm", "contract_id", orignalContractId);
            for (FormRow row : rowVOContractFlow) {
                row.setId(common.getUUID());
                row.setProperty("contract_id", key);
                common.insertForm("approvalFlowVOContractForm", row);
            }
            rowSet.clear();
            rowSet.add(rowContractAmend);
        }
        FormRowSet rows = super.store(element, rowSet, formData);

        return rows;
    }

    @Override
    public String getName() {
        return ("Amend Contract Store Binder");
    }

    @Override
    public String getVersion() {
        return ("1.0.1");
    }

    @Override
    public String getDescription() {
        return ("Amend Contract Store Binder");
    }

    @Override
    public String getLabel() {
        return ("Amend Contract Store Binder");
    }

    @Override
    public String getClassName() {
        return getClass().getName();
    }
}
