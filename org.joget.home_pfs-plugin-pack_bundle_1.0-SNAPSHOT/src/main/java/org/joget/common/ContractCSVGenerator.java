/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;

/**
 *
 * @author Jahan Zaib
 */
public class ContractCSVGenerator {
    
    public boolean createContractCSV(String contractId, String absolutePath, boolean omission) {
        Commons common = new Commons();
        
        FormRowSet contractRows = common.getFormRows("boqForm", "contract_id", contractId);
        
        CSVHelper csvHelper = new CSVHelper(absolutePath);
        if (omission) {
            csvHelper.addRow(new String[]{"System_BOQ_ID", "Item ID", "Description", "Schedule ID", "Quantity", "Unit Price", "Unit", "Item Type"});
        } else {
            csvHelper.addRow(new String[]{"System_BOQ_ID", "Item ID", "Description", "Schedule ID", "Quantity", "Unit Price", "Unit"});
        }
        for (FormRow row : contractRows) {
            //csvHelper.addRow(new String[]{row.getId(), row.getProperty("item_id"), row.getProperty("description"), row.getProperty("schedule_id"), row.getProperty("quantity"), row.getProperty("unit_price"), row.getProperty("unit"), row.getProperty("item_type")});
            csvHelper.addRow(new String[]{row.getId(), row.getProperty("item_id"), row.getProperty("description"), row.getProperty("schedule_id"), row.getProperty("quantity"), row.getProperty("unit_price"), row.getProperty("unit")});
        }
        try {
            csvHelper.createCsvFile();
        } catch (IOException ex) {
            Logger.getLogger(ContractCSVGenerator.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
