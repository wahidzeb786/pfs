/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.workflow.model.service.WorkflowManager;

/**
 *
 * @author Jahan Zaib
 */
public class ProcessReject {

    public void rejectProcess(String contractId) {
        Commons common = new Commons();
        WorkflowManager workflowManager = (WorkflowManager) AppUtil.getApplicationContext().getBean("workflowManager");

        FormRowSet rowSetVO = common.getFormRows("voForm", "contract_id", contractId);
        for (FormRow rowVOUpdate : rowSetVO) {

            String status = rowVOUpdate.getProperty("status");
            if ("In Progress".equalsIgnoreCase(status) || "New".equalsIgnoreCase(status)) {
                rowVOUpdate.setProperty("status", "reject");
                String processId = rowVOUpdate.getProperty("process_id");

                doProceed(rowVOUpdate.getId(), "reject", "vo",
                        rowVOUpdate.getProperty("project_id"), rowVOUpdate.getProperty("contract_id"),
                        rowVOUpdate.getProperty("total"), "");

                common.insertForm("voForm", rowVOUpdate);
                workflowManager.removeProcessInstance(processId);
            }
        }
        FormRowSet rowSetClaim = common.getFormRows("claimForm", "contract_id", contractId);
        for (FormRow rowClaimUpdate : rowSetClaim) {
            String status = rowClaimUpdate.getProperty("status");
            if ("In Progress".equalsIgnoreCase(status) || "New".equalsIgnoreCase(status)) {

                rowClaimUpdate.setProperty("status", "reject");
                String processId = rowClaimUpdate.getProperty("process_id");

                doProceed(rowClaimUpdate.getId(), "reject", "claim",
                        rowClaimUpdate.getProperty("project_id"), rowClaimUpdate.getProperty("contract_id"),
                        rowClaimUpdate.getProperty("total"), rowClaimUpdate.getProperty("total_vo_claim"));

                common.insertForm("claimForm", rowClaimUpdate);
                workflowManager.removeProcessInstance(processId);
            }
        }
    }

    public void doProceed(String id, String status, String type, String projectId,
            String contractId, //String originalAmount, String parentContractId, boolean isContractAmendData,
            String total, String totalVO) {

        Commons common = new Commons();
        UserWorkflowHelper workflowHelper = new UserWorkflowHelper();

        if ("claim".equals(type)) {
            FormRowSet rowSetClaimBoQ = common.getFormRows("boqEditForm", "claim_id", id);
            for (FormRow rowClaimBoQ : rowSetClaimBoQ) {
                contractId = rowClaimBoQ.getProperty("contract_id");
                String itemId = rowClaimBoQ.getProperty("item_id");
                String quantity = rowClaimBoQ.getProperty("quantity");

                String sql = "UPDATE app_fd_pfs_boq "
                        + "SET c_quantity_available = (c_quantity_available+" + quantity + "), "
                        + "c_quantity_last_month = (c_quantity_last_month-" + quantity + ") "
                        + "WHERE c_contract_id='" + contractId + "' AND "
                        + "c_item_id= '" + itemId + "'";

                System.out.println(" SQL : " + sql);
                System.out.println(" Result : " + common.executeDatabaseQuery(sql));

            }

            // Update VO Boq
            FormRowSet rowSetClaimVoBoQ = common.getFormRows("boqClaimVoForm", "claim_id", id);
            for (FormRow rowClaimVOBoQ : rowSetClaimVoBoQ) {
                String voId = rowClaimVOBoQ.getProperty("vo_id");
                String itemId = rowClaimVOBoQ.getProperty("item_id");
                String quantity = rowClaimVOBoQ.getProperty("quantity");

                String sql = "UPDATE app_fd_pfs_boq_vo "
                        + "SET c_quantity_available = (c_quantity_available+" + quantity + "), "
                        + "c_quantity_last_month = (c_quantity_last_month-" + quantity + ") "
                        + "WHERE c_vo_id='" + voId + "' AND "
                        + "c_item_id= '" + itemId + "'";

                System.out.println(" SQL VO: " + sql);
                System.out.println(" Result : " + common.executeDatabaseQuery(sql));

            }

            if (contractId != null && total != null) {
                if (totalVO == null || totalVO.isEmpty()) {
                    totalVO = "0";
                }
                String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                        + "SET c_total_claim_amount = (c_total_claim_amount-" + common.getDoubleValue(total) + ") "
                        + ", c_total_vo_amount = (c_total_vo_amount-" + common.getDoubleValue(totalVO) + ") "
                        + "WHERE id= '" + contractId + "'";

                System.out.println(" SQL Update Contract Claim: " + sqlUpdateProject);
                System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
            }
        } else if ("vo".equals(type)) {
            if (contractId != null && total != null) {

                String sqlUpdateProject = "UPDATE app_fd_pfs_contract "
                        + "SET c_total_vo_amount = (c_total_vo_amount-" + common.getDoubleValue(total) + ") "
                        + "WHERE id= '" + contractId + "'";

                System.out.println(" SQL Update Contract VO: " + sqlUpdateProject);
                System.out.println(" Result : " + common.executeDatabaseQuery(sqlUpdateProject));
            }

        }

    }

}
