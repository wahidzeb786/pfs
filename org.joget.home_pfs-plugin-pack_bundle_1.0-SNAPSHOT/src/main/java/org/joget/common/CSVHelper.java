/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Jahan Zaib
 */
public class CSVHelper {

    public CSVHelper(String CSV_FILE_NAME) {
        this.CSV_FILE_NAME = CSV_FILE_NAME;
    }

    public void addRow(String[] row) {
        this.rows.add(row);
    }

    private final List<String[]> rows = new ArrayList<>();
    private final String CSV_FILE_NAME;

    public String convertToCSV(String[] data) {
        return Stream.of(data)
                .map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    public void createCsvFile() throws IOException {
        File csvOutputFile = new File(CSV_FILE_NAME);
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            rows.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        }
    }

    public String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

    public ArrayList<BOQObject> getCSVToObject() {
        ArrayList<BOQObject> arrayList = new ArrayList<>();
        String line = "";
        String splitBy = ",";
        try {
//parsing a CSV file into BufferedReader class constructor  
            boolean skipFirst = true;
            BufferedReader br = new BufferedReader(new FileReader(CSV_FILE_NAME));
            while ((line = br.readLine()) != null) //returns a Boolean value  
            {
                if (skipFirst) {
                    skipFirst = false;
                    continue;
                }
                String[] record = line.split(splitBy);    // use comma as separator  
                arrayList.add(new BOQObject(record[0], record[5]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

}
