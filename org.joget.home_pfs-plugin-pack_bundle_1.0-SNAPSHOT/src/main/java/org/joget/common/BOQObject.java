/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

/**
 *
 * @author Jahan Zaib
 */
public class BOQObject {

    public BOQObject(String id, String price) {
        this.id = id;
        this.price = price;
    }

    private String id;
    private String price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BOQObject{" + "id=" + id + ", price=" + price + '}';
    }

}
