/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Base64;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import org.apache.commons.httpclient.NameValuePair;
import org.joget.apps.app.dao.AppDefinitionDao;
import org.joget.apps.app.dao.EnvironmentVariableDao;
import org.joget.apps.app.dao.FormDefinitionDao;
import org.joget.apps.app.dao.PluginDefaultPropertiesDao;
import org.joget.apps.app.lib.EmailTool;
import org.joget.apps.app.model.AppDefinition;
import org.joget.apps.app.model.EnvironmentVariable;
import org.joget.apps.app.model.FormDefinition;
import org.joget.apps.app.model.PluginDefaultProperties;
import org.joget.apps.app.service.AppService;
import org.joget.apps.app.service.AppUtil;
import org.joget.apps.form.dao.FormDataDao;
import org.joget.apps.form.model.Form;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.apps.form.service.FormService;
import org.joget.apps.form.service.FormUtil;
import org.joget.commons.util.LogUtil;
import org.joget.commons.util.PluginThread;
import org.joget.commons.util.UuidGenerator;
import org.joget.directory.dao.DepartmentDao;
import org.joget.directory.dao.EmploymentDao;
import org.joget.directory.dao.OrganizationDao;
import org.joget.directory.dao.UserDao;
import org.joget.directory.model.Department;
import org.joget.directory.model.Employment;
import org.joget.directory.model.Organization;
import org.joget.directory.model.Role;
import org.joget.directory.model.User;
import org.joget.plugin.property.service.PropertyUtil;
import org.joget.workflow.model.service.WorkflowManager;
import org.joget.workflow.model.service.WorkflowUserManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

/**
 *
 * @author Mahmoor
 */
public class Commons {

    public boolean performAPIAuthorization(HttpServletRequest httpRequest)
            throws ServletException, IOException {
        final String authorization = httpRequest.getHeader("API_KEY");
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "authorization: {0}", authorization);
        if (authorization != null) {
            String apiKey = getEnvVariables("pfs", "API_KEY");
            return authorization.equals(apiKey);
        }
        return false;

    }

    public static boolean performAuthenticate(HttpServletRequest httpRequest)
            throws ServletException, IOException {
        final String authorization = httpRequest.getHeader("Authorization");
        if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
            // Authorization: Basic base64credentials
            String base64Credentials = authorization.substring("Basic".length()).trim();
            byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
            String credentials = new String(credDecoded, StandardCharsets.UTF_8);
            // credentials = username:password
            final String[] values = credentials.split(":", 2);
            if (values.length >= 1) {
                Logger.getLogger(Commons.class.getName()).log(Level.INFO, "values: {0}", values[0] + " = " + values.length);
            }
            if (values.length >= 2) {
                Logger.getLogger(Commons.class.getName()).log(Level.INFO, "values: {0}", values[1] + " = " + values.length);
                //Authentication request = new UsernamePasswordAuthenticationToken(values[0], values[1]);
                // Authentication result = authenticationManager.authenticate(request);
            }
            WorkflowUserManager wum = (WorkflowUserManager) AppUtil.getApplicationContext().getBean("workflowUserManager");

            Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Username: " + wum.getCurrentUsername());
            Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Username: " + wum.getCurrentThreadUser());
            return true;
        } else {
            return false;
        }
    }

    public double getDoubleValue(String value) {
        if (value == null || value.isEmpty()) {
            return 0;
        }
        //StringUtil.removeNumberFormat(value, null, null, null);
        if (value.contains(",")) {
            NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
            Number number = null;
            try {
                number = format.parse(value);
            } catch (ParseException ex) {
                Logger.getLogger(Commons.class.getName()).log(Level.SEVERE, null, ex);
            }
            return number.doubleValue();
        } else {
            return Double.parseDouble(value);
        }
    }

    public NameValuePair[] getParams(Map<String, String> map) {

        NameValuePair[] pairs = new NameValuePair[map.size()];

        int i = 0;
        for (String key : map.keySet()) {
            if (key != null) {

                try {
                    if (map.get(key) != null) {
                        pairs[i] = new NameValuePair(URLEncoder.encode(key, "UTF-8"), map.get(key));
                    } else {
                        pairs[i] = new NameValuePair(URLEncoder.encode(key, "UTF-8"), "");
                    }
                } catch (UnsupportedEncodingException e) {
                    Logger.getLogger(Commons.class.getName()).log(Level.SEVERE, null, e);
                }

                i++;
            }
        }
        return pairs;
    }

    public String getUUID() {
        UuidGenerator uuidGen = UuidGenerator.getInstance();
        String uuid = uuidGen.getUuid();
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "UUID: {0}", uuid);
        return uuid;
    }

    public String streamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            Logger.getLogger(Commons.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                in.close();
            } catch (Exception e) {
                Logger.getLogger(Commons.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return sb.toString();
    }

    public String getServerUrl(String appId, String appName) {
        String result = "";

        AppDefinitionDao add = (AppDefinitionDao) AppUtil.getApplicationContext().getBean("appDefinitionDao");
        AppDefinition appDef = new AppDefinition();
        appDef.setId(appId);
        appDef.setName(appName);
        appDef.setVersion(add.getPublishedVersion(appId));

        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Get Published Version : {0}", appDef.getVersion());

        EnvironmentVariableDao environmentVariableDao = (EnvironmentVariableDao) AppUtil.getApplicationContext().getBean("environmentVariableDao");
        EnvironmentVariable evServerURL = environmentVariableDao.loadById("serverURL", appDef);
        result = evServerURL != null ? evServerURL.getValue() : "";

        return result;
    }

    public String getEnvVariables(String appid, String variableName) {
        EnvironmentVariableDao environmentVariableDao = (EnvironmentVariableDao) AppUtil.getApplicationContext().getBean("environmentVariableDao");
        EnvironmentVariable envVariable = environmentVariableDao.loadById(variableName, getAppDefinition(appid));
        return envVariable != null ? envVariable.getValue() : "";
    }

    public String getServerUrlForProcess(String appId, String appName) {
        String result = "";

        AppDefinitionDao add = (AppDefinitionDao) AppUtil.getApplicationContext().getBean("appDefinitionDao");
        AppDefinition appDef = new AppDefinition();
        appDef.setId(appId);
        appDef.setName(appName);
        appDef.setVersion(add.getPublishedVersion(appId));

        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Get Published Version : {0}", appDef.getVersion());

        EnvironmentVariableDao environmentVariableDao = (EnvironmentVariableDao) AppUtil.getApplicationContext().getBean("environmentVariableDao");
        EnvironmentVariable evServerURL = environmentVariableDao.loadById("serverURLForProcess", appDef);
        result = evServerURL != null ? evServerURL.getValue() : "";

        return result;
    }

    public void sendNotification(String userId, String email, String subject,
            String body, String type, String url, String projectId, String status) {

        sendPushNotification(userId, subject, body, type, url, projectId, status);
        sendEmail(userId, email, subject, body);

        // handle subject and body remove html tag
    }

    public void sendPushNotification(final String userId, String mobileSubject, String mobileBody,
            final String type, String url, final String projectId, final String status) {
        String body = Jsoup.parse(mobileBody).text();
        String subject = Jsoup.parse(mobileSubject).text();
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, " --> status={0}", status);

        if ("reject".equals(status) || "complete".equalsIgnoreCase(status)) {
            url = "";
        }
        String urlFinal = url.replace("_inbox", "_inbox_mobile");

        FormRowSet formRows = getFormRows("userNotificationForm", "id", userId);
        if (formRows.size() > 0) {
            String serverKey = getEnvVariables("pfs", "serverKey");
            //Logger.getLogger(Commons.class.getName()).log(Level.INFO, " --> serverKey={0}", serverKey);
            Logger.getLogger(Commons.class.getName()).log(Level.INFO, " --> userid={0}", userId);

            FormRow formRow = formRows.get(0);
            final String fcm = formRow.getProperty("fcm");
            final String deviceType = formRow.getProperty("type");
            //Logger.getLogger(Commons.class.getName()).log(Level.INFO, " --> Notification Row: ", formRow.toString());

            if (!"".equals(fcm)) {
                FormRow row = new FormRow();
                row.setProperty("type", "push");
                row.setProperty("user_id", Commons.getEmpty(userId));
                row.setProperty("subject", Commons.getEmpty(subject));
                row.setProperty("payload", Commons.getEmpty(body));
                row.setProperty("project_id", Commons.getEmpty(projectId));
                row.setProperty("url", Commons.getEmpty(url));
                row.setProperty("fcm_id", Commons.getEmpty(fcm));
                row.setProperty("device_type", Commons.getEmpty(deviceType));
                row.setProperty("notification_type", Commons.getEmpty(type));

                insertForm("notificationDataForm", row);

                Logger.getLogger(Commons.class.getName()).log(Level.INFO, " --> Sending Notification ");
                //MobileNotification.sendFCMNotification2(fcm, serverKey, subject, body);
                final String orgId = getEnvVariables("pfs", "orgId");
                Thread emailThread = new PluginThread(new Runnable() {

                    public void run() {
                        try {
                            MobileNotification mobileNotification = new MobileNotification();
                            mobileNotification.sendFCMNotification(fcm, serverKey, subject, body,
                                    type, urlFinal, projectId, userId, orgId, deviceType);
                        } catch (Exception ex) {
                            LogUtil.error(EmailTool.class.getName(), ex, "");
                        }
                    }
                });
                emailThread.setDaemon(true);
                emailThread.start();

            } else {
                Logger.getLogger(Commons.class.getName()).log(Level.INFO, " --> FCM Information is empty for userId={0}", userId);
            }
        } else {
            Logger.getLogger(Commons.class.getName()).log(Level.INFO, " --> NO FCM Information for userId={0}", userId);
        }
    }

    public static String getEmpty(String val) {
        if (val == null) {
            return "";
        }
        return val;
    }

    public void sendEmail(String userId, String id, String emailSubject, String emailBody) {
        FormRow row = new FormRow();
        row.setProperty("type", "email");
        row.setProperty("user_id", userId);
        row.setProperty("email", id);
        row.setProperty("subject", emailSubject);
        row.setProperty("payload", emailBody);

        insertForm("notificationDataForm", row);

        try {
            EmailTool et = new EmailTool();
            Map properties = getEmailDefaultSettings();
            properties.put("toSpecific", id);
            properties.put("subject", emailSubject);
            properties.put("message", emailBody);
            et.execute(properties);

            Logger.getLogger(Commons.class.getName()).log(Level.INFO, " --> Email Sent.");
        } catch (Exception e) {
            Logger.getLogger(Commons.class.getName()).log(Level.INFO, e.getMessage());
        }
    }

    public Map getEmailDefaultSettings() {
        PluginDefaultPropertiesDao dao = (PluginDefaultPropertiesDao) AppUtil.getApplicationContext().getBean("pluginDefaultPropertiesDao");
        PluginDefaultProperties pluginDefaultProperties = dao.loadById("org.joget.apps.app.lib.EmailTool", AppUtil.getCurrentAppDefinition());
        Map emailSettings = PropertyUtil.getPropertiesValueFromJson(pluginDefaultProperties.getPluginProperties());
        return emailSettings;
    }

    /*
    public String getAllUserInGroupFCM(String groupName) {
        GroupDao gd = (GroupDao) AppUtil.getApplicationContext().getBean("groupDao");

        Group group = gd.getGroup(groupName);
        Set<User> set = group.getUsers();

        String fcmIds = null;
        for (User u : set) {
            String userId = u.getUsername();
            String fcmId = getFCMId(userId);
            if (fcmId != null) {
                if (fcmIds == null) {
                    fcmIds = fcmId;
                } else {
                    fcmIds = ";" + fcmId;
                }
            }
        }
        return fcmIds;
    }*/
    public void insertAuditForm(String key, String parentId, String userId, String remarks,
            String userName, String status, String submitType) {

        String formAuditId = "auditForm";

        FormRow row = new FormRow();
        row.setId(key);

        row.put("form_uuid", parentId);
        row.put("username", userId);
        row.put("remarks", remarks);
        row.put("user_fname_lname", userName);
        row.put("field_changed", status);
        row.put("submit_type", submitType);

        insertForm(formAuditId, row);

    }

    public boolean addOrganization(String id, String name, String desc) {

        OrganizationDao organizationDao = (OrganizationDao) AppUtil.getApplicationContext().getBean("organizationDao");
        Organization organization = new Organization();
        organization.setId(id);
        organization.setName(name);
        organization.setDescription(desc);
        boolean status = false;

        if (organizationDao.getOrganization(id) != null) {
        } else {
            status = organizationDao.addOrganization(organization);

            addDepartment("ProjectManager", "Project Manager", organization);
            addDepartment("FinanceOfficer", "Finance Officer", organization);
            addDepartment("FinanceHead", "Finance Head", organization);
            addDepartment("DocController", "Doc Controller", organization);
            addDepartment("QAQCEngineer", "QAQC Engineer", organization);
            addDepartment("Director", "Director", organization);
            addDepartment("ConstructionEngineer", "Construction Engineer", organization);
            addDepartment("SafteyOfficer", "Saftey Officer", organization);
            addDepartment("PlanningEngineer", "Planning Engineer", organization);
            addDepartment("ConsultantCRE", "Consultant CRE", organization);
            addDepartment("ConsultantRE", "Consultant RE", organization);
            addDepartment("ContractorPM", "Contractor PM", organization);
            addDepartment("ContractorEngineer", "Contractor Engineer", organization);
            addDepartment("MACommittee", "MA Committee", organization);
            addDepartment("TechnicalReviewer", "Technical Reviewer", organization);
            addDepartment("RiskEngineer", "Risk Engineer", organization);

        }
        return status;
    }

    public boolean addEmployment(String username, String organizationId) {
        EmploymentDao employmentDao = (EmploymentDao) AppUtil.getApplicationContext().getBean("employmentDao");

        Employment employment = new Employment();
        employment.setUserId(username);
        employment.setEmployeeCode(username);
        employment.setOrganizationId(organizationId);
        return employmentDao.updateEmployment(employment);
    }

    public boolean assignDepartmentToUser(String username, String departmentId, String uniqueId) {
        EmploymentDao employmentDao = (EmploymentDao) AppUtil.getApplicationContext().getBean("employmentDao");

        String organizationId = getUserOrganization(username);
        Employment employment = new Employment();
        employment.setId(uniqueId);
        employment.setUserId(username);
        employment.setEmployeeCode(username);
        employment.setDepartmentId(departmentId);
        employment.setOrganizationId(organizationId);
        return employmentDao.updateEmployment(employment);

        //return employmentDao.assignUserToDepartment(username, departmentId);
        //return false;
    }

    public boolean unassignDepartmentToUser(String username, String departmentId, String uniqueId) {
        EmploymentDao employmentDao = (EmploymentDao) AppUtil.getApplicationContext().getBean("employmentDao");

        /*String organizationId = getUserOrganization(username);
        Employment employment = new Employment();
        employment.setId(uniqueId);
        employment.setUserId(username);
        employment.setEmployeeCode(username);
        employment.setDepartmentId(departmentId);
        employment.setOrganizationId(organizationId);
         */
        return employmentDao.unassignUserFromDepartment(username, departmentId);

        //return employmentDao.assignUserToDepartment(username, departmentId);
        //return false;
    }

    public boolean addDepartment(String departmentId, String departmentName, Organization organization) {
        try {
            DepartmentDao departmentDao = (DepartmentDao) AppUtil.getApplicationContext().getBean("departmentDao");

            boolean isUpdate = true;
            Department department = departmentDao.getDepartment(departmentId);
            if (department == null) {
                isUpdate = false;
                department = new Department();
            }
            department.setId(departmentId);
            department.setName(departmentName);
            department.setOrganization(organization);
            if (isUpdate) {
                //return departmentDao.updateDepartment(department);
            } else {
                return departmentDao.addDepartment(department);
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean userExist(String userId) {
        return getUser(userId) != null;
    }

    public User getUser(String userId) {
        UserDao ud = (UserDao) AppUtil.getApplicationContext().getBean("userDao");

        return ud.getUserById(userId);
    }

    public Boolean removeUser(String userId) {
        UserDao ud = (UserDao) AppUtil.getApplicationContext().getBean("userDao");

        return ud.deleteUser(userId);
    }

    public String getUserOrganization(String username) {
        UserDao ud = (UserDao) AppUtil.getApplicationContext().getBean("userDao");
        User user = ud.getUserById(username);

        Employment employment = (Employment) user.getEmployments().iterator().next();
        if (employment != null) {
            return employment.getOrganizationId();
        }
        return null;
    }

    public boolean createJogetUser(String userId, String email, String password, String fName, String lName,
            String userStatus, boolean isUpdate) {
        UserDao ud = (UserDao) AppUtil.getApplicationContext().getBean("userDao");

        User user;
        if (isUpdate) {
            user = ud.getUserById(userId);
        } else {
            user = new User();
        }

        user.setId(userId);
        user.setUsername(userId); // set all the required values here like username, password, first name, last name etc
        if (password != null) {
            user.setPassword(password);
            user.setConfirmPassword(password);
        }
        if (fName != null) {
            user.setFirstName(fName);
        }
        if (lName != null) {
            user.setLastName(lName);
        }
        if (email != null) {
            user.setEmail(email);
        }

        if (userStatus != null) {
            if ("active".equalsIgnoreCase(userStatus)) {
                user.setActive(1);
            } else {
                user.setActive(0);
            }
        }

        HashSet setRole = new HashSet();
        Role role = new Role();
        role.setId("ROLE_USER");
        setRole.add(role);
        user.setRoles(setRole);


        /*Group group = new Group();
        HashSet setGroups = new HashSet();

        if (userRole.equalsIgnoreCase("manager")) {
            group.setId("managerGroup");
            setGroups.add(group);
        } else if (userRole.equalsIgnoreCase("customer")) {
            group.setId("customerGroup");
            setGroups.add(group);
        } else if (userRole.equalsIgnoreCase("engineer")) {
            group.setId("engineerGroup");
            setGroups.add(group);
        }
        user.setGroups(setGroups);*/
        if (isUpdate) {
            return ud.updateUser(user);
        } else {
            return ud.addUser(user);
        }
    }

    public boolean updateUser(User user) {
        UserDao ud = (UserDao) AppUtil.getApplicationContext().getBean("userDao");
        return ud.updateUser(user);
    }

    public String getFormTableName(String formId) {
        return getFormDataDao().getFormTableName(getForm(formId)).replaceFirst("app_fd_", "").trim();
    }

    public Form getForm(String formId) {
        Form form = null;
        String formJson = getFormDefinition(formId).getJson();

        if (formJson != null) {
            form = (Form) getFormService().createElementFromJson(formJson);
        }
        return form;
    }

    public FormDefinition getFormDefinition(String formId) {
        return getFormDefDao().loadById(formId, getAppDefinition("pfs"));

    }

    public AppDefinition getAppDefinition(String appId) {
        if (appDef == null) {
            AppService appService = (AppService) AppUtil.getApplicationContext().getBean("appService");
            Long appVersion = appService.getPublishedVersion(appId);
            appDef = appService.getAppDefinition(appId, appVersion.toString());
        }
        return appDef;
    }

    public boolean insertForm(String formId, FormRow row) {
        Form form = getForm(formId);
        if (form != null) {
            try {
                java.util.Date dt = new java.util.Date();

                FormRowSet rowSet = new FormRowSet();
                if (row.getDateCreated() == null) {
                    row.setDateCreated(dt);
                }
                row.setDateModified(dt);
                if (row.getId() == null) {
                    UuidGenerator uuid = UuidGenerator.getInstance();
                    row.setId(uuid.getUuid());
                }
                rowSet.add(row);

                getFormDataDao().saveOrUpdate(form, rowSet);
            } catch (Exception ex) {
                Logger.getLogger(Commons.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return true;
    }

    public String getFormPropertyName(String propertyName) {
        if (propertyName != null && !propertyName.isEmpty()) {
            if (!FormUtil.PROPERTY_ID.equals(propertyName)) {
                propertyName = FormUtil.PROPERTY_CUSTOM_PROPERTIES + "." + propertyName;
            }
        }
        return propertyName;
    }

    public void delateFormData(String formId, String[] ids) {

        Form form = getForm(formId);

        if (form != null) {
            try {
                getFormDataDao().delete(form, ids);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, ex.getMessage());
            }
        }

    }

    public FormRowSet getFormRows(String formId, String key, String id) {
        return getFormRows(formId, key, new String[]{id});
    }

    public FormRowSet getFormRows(String formId, String key, String[] ids) {
        return getFormRows(formId, key, ids, null);
    }

    public FormRowSet getFormRows(String formId, String key, String[] ids, String orderBy) {
        FormRowSet rows = null;

        try {
            String foreignKeyFilter = getFormPropertyName(key);
            String condition = (foreignKeyFilter != null && !foreignKeyFilter.isEmpty()) ? " WHERE " + foreignKeyFilter + " = ?" : "";
            if (orderBy != null) {
                condition += " order by " + getFormPropertyName(orderBy) + " asc ";
            }
            String tableName = getFormTableName(formId);
            String formDefId = getFormDefinition(formId).getId();
            if (key != null) {
                rows = getFormDataDao().find(formDefId, tableName, condition, ids, null, null, null, null);
            } else {
                rows = getFormDataDao().find(formDefId, tableName, null, null, null, null, null, null);
            }
            if (rows != null) {
                Logger.getLogger(Commons.class.getName()).log(Level.INFO, " formId : {0} - tableName : {1} - Total Rows : {2}", new Object[]{formDefId, tableName, rows.size()});
            }
        } catch (Exception ex) {
            Logger.getLogger(Commons.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rows;
    }

    public void delateFormData(String formId, FormRowSet rowSet) {
        String tableName = getFormTableName(formId);

        try {
            getFormDataDao().delete(formId, tableName, rowSet);
        } catch (Exception ex) {
            Logger.getLogger(Commons.class.getName()).log(Level.INFO, ex.getMessage());
        }
    }

    public void delateFormData(String formId, String key, String id) {
        delateFormData(formId, key, new String[]{id});
    }

    public void delateFormData(String formId, String key, String[] ids) {

        try {
            delateFormData(formId, getFormRows(formId, key, ids));
        } catch (Exception ex) {
            Logger.getLogger(Commons.class.getName()).log(Level.INFO, ex.getMessage());
        }

    }

    public boolean addDepartmentToUser(String id, String userId, String departmentId, String organizationId) {

        String queryInsert = "INSERT INTO dir_employment (id, employeeCode, userId, departmentId, organizationId) "
                + "VALUES ('" + id + "','" + userId + "','" + userId + "','" + departmentId + "','" + organizationId + "')";
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "************\n\n\n{0}\n\n\n**************\n\n\n", queryInsert);

        return executeDatabaseQuery(queryInsert) != 0;
    }

    public boolean deleteDepartmentToUser(String id) {

        String queryDelete = "DELETE FROM dir_employment where id=" + "'" + id + "'";
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "************Del\n\n\n{0}\n\n\n**************\n\n\n", queryDelete);
        return executeDatabaseQuery(queryDelete) != 0;
    }

    public void updateWorkflowProjectVariables(String userName, String projectId) {
        String whereClause = " WHERE c_username LIKE ('%" + userName + "%') ";

        if (projectId != null) {
            whereClause += " AND c_project_id='" + projectId + "'";
        }

        String contractProjectQuery = "UPDATE app_fd_approval_flow "
                + "set c_username=REPLACE(c_username,'" + userName + "' ,'" + userName + "(__USER__REMOVED__)')";
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "************UPDATE Contract Project Flow \n\n\n{0}\n\n\n**************\n\n\n", contractProjectQuery + whereClause);
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Result {0}", executeDatabaseQuery(contractProjectQuery + whereClause));

        String contractQuery = "UPDATE app_fd_approval_cntrct_flow "
                + "set c_username=REPLACE(c_username,'" + userName + "' ,'" + userName + "(__USER__REMOVED__)')";
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "************UPDATE Contract Flow \n\n\n{0}\n\n\n**************\n\n\n", contractQuery + whereClause);
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Result {0}", executeDatabaseQuery(contractQuery + whereClause));

        String claimContractProjectQuery = "UPDATE app_fd_approval_flow_claim "
                + "set c_username=REPLACE(c_username,'" + userName + "' ,'" + userName + "(__USER__REMOVED__)')";
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "************UPDATE Project Claim Flow \n\n\n{0}\n\n\n**************\n\n\n", claimContractProjectQuery + whereClause);
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Result {0}", executeDatabaseQuery(claimContractProjectQuery + whereClause));

        String claimContractQuery = "UPDATE app_fd_approval_flow_claims "
                + "set c_username=REPLACE(c_username,'" + userName + "' ,'" + userName + "(__USER__REMOVED__)')";
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "************UPDATE Contract Claim Flow \n\n\n{0}\n\n\n**************\n\n\n", claimContractQuery + whereClause);
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Result {0}", executeDatabaseQuery(claimContractQuery + whereClause));

        String claimQuery = "UPDATE app_fd_approval_flow_claima "
                + "set c_username=REPLACE(c_username,'" + userName + "' ,'" + userName + "(__USER__REMOVED__)')";
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "************UPDATE Claim Flow \n\n\n{0}\n\n\n**************\n\n\n", claimQuery + whereClause);
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Result {0}", executeDatabaseQuery(claimQuery + whereClause));

        String voContractProjectQuery = "UPDATE app_fd_approval_flow_vo "
                + "set c_username=REPLACE(c_username,'" + userName + "' ,'" + userName + "(__USER__REMOVED__)')";
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "************UPDATE Project VO Flow \n\n\n{0}\n\n\n**************\n\n\n", voContractProjectQuery + whereClause);
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Result {0}", executeDatabaseQuery(voContractProjectQuery + whereClause));

        String voContractQuery = "UPDATE app_fd_approval_flow_vos "
                + "set c_username=REPLACE(c_username,'" + userName + "' ,'" + userName + "(__USER__REMOVED__)')";
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "************UPDATE Contract VO Flow \n\n\n{0}\n\n\n**************\n\n\n", voContractQuery + whereClause);
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Result {0}", executeDatabaseQuery(voContractQuery + whereClause));

        String voQuery = "UPDATE app_fd_approval_flow_vo_vo "
                + "set c_username=REPLACE(c_username,'" + userName + "' ,'" + userName + "(__USER__REMOVED__)')";
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "************UPDATE VO Flow \n\n\n{0}\n\n\n**************\n\n\n", voQuery + whereClause);
        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Result {0}", executeDatabaseQuery(voQuery + whereClause));

    }

    public int executeDatabaseQuery(String query) {
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            try (Statement stmt = con.createStatement()) {
                int result = stmt.executeUpdate(query);
                return result;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Commons.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    private AppDefinition appDef = null;

    private FormDefinitionDao formDefinitionDao = null;

    private FormDefinitionDao getFormDefDao() {
        if (formDefinitionDao == null) {
            formDefinitionDao = (FormDefinitionDao) AppUtil.getApplicationContext().getBean("formDefinitionDao");
        }
        return formDefinitionDao;
    }

    private FormService formService = null;

    private FormService getFormService() {
        if (formService == null) {
            formService = (FormService) AppUtil.getApplicationContext().getBean("formService");
        }
        return formService;
    }

    private FormDataDao formDataDao = null;

    private FormDataDao getFormDataDao() {
        if (formDataDao == null) {
            formDataDao = (FormDataDao) AppUtil.getApplicationContext().getBean("formDataDao");
        }
        return formDataDao;
    }

    public String getOrganizationName(String organizationId) {
        OrganizationDao organizationDao = (OrganizationDao) AppUtil.getApplicationContext().getBean("organizationDao");
        if ("".equals(organizationId)) {
            return null;
        }
        Organization organization = organizationDao.getOrganization(organizationId);

        if (organization != null) {
            System.out.println("--->" + organizationId + " For " + organization.getName());
            return organization.getName();
        }
        return null;
    }

}
