/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.joget.apps.app.service.AppUtil;

/**
 *
 * @author Jahan Zaib
 */
public class UserWorkflowHelper {

    public boolean isUserPartOfRunningWorkflow(String username) {
        return isUserPartOfRunningWorkflow(username, null);
    }

    public boolean isUserPartOfRunningWorkflow(String username, String projectId) {
        if (!checkUserInVOFlow(username, projectId).isEmpty()) {
            return Boolean.TRUE;
        }
        if (!checkUserInClaimFlow(username, projectId).isEmpty()) {
            return Boolean.TRUE;
        }
        if (!checkUserInContractFlow(username, projectId).isEmpty()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public boolean isProjectPartOfRunningWorkflow(String projectId) {
        if (!checkProjectInVOFlow(projectId).isEmpty()) {
            return Boolean.TRUE;
        }
        if (!checkProjectInClaimFlow(projectId).isEmpty()) {
            return Boolean.TRUE;
        }
        if (!checkProjectInContractFlow(projectId).isEmpty()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private ArrayList<String> checkUserInVOFlow(String username, String projectId) {
        ArrayList<String> voList = new ArrayList<>();
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "SELECT a.c_vo_id FROM app_fd_pfs_vo a, app_fd_approval_flow_vo_vo b "
                    + " WHERE a.c_status NOT IN ('complete', 'reject', 'draft', 'Upload BoQ' ,'SLA Breached') AND a.id = b.c_vo_id AND b.c_username='" + username + "'";
            if (projectId != null) {
                query += " AND a.c_project_id='" + projectId + "'";
            }
            try (PreparedStatement stmt = con.prepareStatement(query)) {

                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        voList.add(rSet.getString(1));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Username {0} is part of vos {1}", new Object[]{username, voList.toString()});
        return voList;
    }

    private ArrayList<String> checkProjectInVOFlow(String projectId) {
        ArrayList<String> voList = new ArrayList<>();
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "SELECT a.c_vo_id FROM app_fd_pfs_vo a "
                    + " WHERE a.c_status NOT IN ('complete', 'reject', 'draft', 'Upload BoQ' ,'SLA Breached')"
                    + " AND a.c_project_id='" + projectId + "'";
            try (PreparedStatement stmt = con.prepareStatement(query)) {

                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        voList.add(rSet.getString(1));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project {0} is part of vos {1}", new Object[]{projectId, voList.toString()});
        return voList;
    }

    private ArrayList<String> checkUserInClaimFlow(String username, String projectId) {
        ArrayList<String> claimList = new ArrayList<>();
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "SELECT a.c_claim_id FROM app_fd_pfs_claim a, app_fd_approval_flow_claima b WHERE "
                    + " a.c_status NOT IN ('complete', 'reject', 'draft', 'Upload BoQ', 'SLA Breached') AND a.id = b.c_claim_id AND b.c_username='" + username + "'";
            if (projectId != null) {
                query += " AND a.c_project_id='" + projectId + "'";
            }
            try (PreparedStatement stmt = con.prepareStatement(query)) {

                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        claimList.add(rSet.getString(1));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Username {0} is part of claim {1}", new Object[]{username, claimList.toString()});
        return claimList;
    }

    private ArrayList<String> checkProjectInClaimFlow(String projectId) {
        ArrayList<String> claimList = new ArrayList<>();
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "SELECT a.c_claim_id FROM app_fd_pfs_claim a WHERE "
                    + " a.c_status NOT IN ('complete', 'reject', 'draft', 'Upload BoQ' ,'SLA Breached')"
                    + " AND a.c_project_id='" + projectId + "'";
            try (PreparedStatement stmt = con.prepareStatement(query)) {

                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        claimList.add(rSet.getString(1));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project {0} is part of claim {1}", new Object[]{projectId, claimList.toString()});
        return claimList;
    }

    private ArrayList<String> checkUserInContractFlow(String username, String projectId) {
        ArrayList<String> contractList = new ArrayList<>();
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "SELECT a.c_contract_id FROM app_fd_pfs_contract a, app_fd_approval_cntrct_flow b WHERE "
                    + " a.c_status NOT IN ('complete', 'complete-amendment-approved', 'reject', 'draft', 'Upload BoQ' ,'SLA Breached') AND a.id = b.c_contract_id AND b.c_username='" + username + "'";
            if (projectId != null) {
                query += " AND a.c_project_id='" + projectId + "'";
            }
            try (PreparedStatement stmt = con.prepareStatement(query)) {

                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        contractList.add(rSet.getString(1));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Username {0} is part of contract {1}", new Object[]{username, contractList.toString()});
        return contractList;
    }

    private ArrayList<String> checkProjectInContractFlow(String projectId) {
        ArrayList<String> contractList = new ArrayList<>();
        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query
                    = "SELECT a.c_contract_id FROM app_fd_pfs_contract a"
                    + " WHERE "
                    + " a.c_status NOT IN ('complete', 'complete-amendment-approved', 'reject', 'draft', 'Upload BoQ' ,'SLA Breached')"
                    + " AND a.c_project_id='" + projectId + "'";
            try (PreparedStatement stmt = con.prepareStatement(query)) {

                try (ResultSet rSet = stmt.executeQuery(query)) {
                    while (rSet.next()) {
                        contractList.add(rSet.getString(1));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project {0} is part of contract {1}", new Object[]{projectId, contractList.toString()});
        return contractList;
    }

    public void updateProjectAmounts(String voAmount, String claimedVoAmount,
            String spentAmount, String contractedAmount, String projectId) {
        Commons common = new Commons();
        String query
                = "UPDATE app_fd_pfs_project "
                + "SET "
                + "c_vo_amount = (COALESCE(c_vo_amount,0)+" + common.getDoubleValue(voAmount) + "), "
                + "c_claimed_vo_amount = (COALESCE(c_claimed_vo_amount,0)+" + common.getDoubleValue(claimedVoAmount) + "), "
                + "c_spent_amount = (COALESCE(c_spent_amount,0)+" + common.getDoubleValue(spentAmount) + "), "
                + "c_contracted_amount = (COALESCE(c_contracted_amount,0)+" + common.getDoubleValue(contractedAmount) + ") "
                + "WHERE "
                + "c_project_id= '" + projectId + "'";

        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project Update Query : {0} ", query);

        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project Update Query Result Count : {0} ", common.executeDatabaseQuery(query));
    }

    public boolean isUserPartOfProject(String projectId, String username) {
        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project Manager Check  : {0}{1}", new Object[]{projectId, username});

        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_username "
                    + "from app_fd_user_dep_project where c_project_id='" + projectId + "'"
                    + " and c_username='" + username + "'";
            Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project Part Check query : {0}", query);

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    if (rSet.next()) {
                        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project Part Check  Response: true");
                        return true;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean isUserProjectManager(String projectId, String username) {
        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project Manager Check  : {0}{1}", new Object[]{projectId, username});

        DataSource ds = (DataSource) AppUtil.getApplicationContext().getBean("setupDataSource");
        try (Connection con = ds.getConnection()) {
            String query = "SELECT c_username "
                    + "from app_fd_user_dep_project where c_project_id='" + projectId + "'"
                    + " and c_department='Project Manager'"
                    + " and c_username='" + username + "'";
            Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project Manager Check query : {0}", query);

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                try (ResultSet rSet = stmt.executeQuery(query)) {
                    if (rSet.next()) {
                        Logger.getLogger(UserWorkflowHelper.class.getName()).log(Level.INFO, "Project Manager Check  Response: true");
                        return true;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
