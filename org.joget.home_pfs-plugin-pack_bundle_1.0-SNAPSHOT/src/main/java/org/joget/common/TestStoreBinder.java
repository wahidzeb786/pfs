/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

/**
 *
 * @author JAHANZAIB
 */
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joget.apps.form.lib.WorkflowFormBinder;
import org.joget.apps.form.model.Element;
import org.joget.apps.form.model.FormData;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;
import org.joget.commons.util.UuidGenerator;
import org.joget.custom.webservices.AbortRunningProcess;

/**
 *
 * @author Jahan Zaib
 */
public class TestStoreBinder extends WorkflowFormBinder {

    @Override
    public FormRowSet store(Element element, FormRowSet rowSet, FormData formData) {

        Logger.getLogger(Commons.class.getName()).log(Level.INFO, "---> Test Store Binder <---");
        if (rowSet != null && !rowSet.isEmpty()) {
            Commons common = new Commons();
            FormRow rowContractAmend = rowSet.get(0);
            Logger.getLogger(Commons.class.getName()).log(Level.INFO, rowContractAmend.toString());
            String key = common.getUUID();
            String orignalContractId = rowContractAmend.getId();

            FormRowSet rowSetContractOrignal = common.getFormRows("contractForm", "id", orignalContractId);
            if (rowSetContractOrignal.size() > 0) {
                FormRow rowContractUpdate = rowSetContractOrignal.get(0);
                Logger.getLogger(Commons.class.getName()).log(Level.INFO, "Orignal Contract Data: {0}", rowContractUpdate.toString());

                rowContractUpdate.setProperty("parent_contract_id", orignalContractId);
                rowContractUpdate.setId(key);
                rowContractUpdate.setProperty("status", "complete-amendment-inprogress");
                rowContractUpdate.setProperty("status_integration", "complete-amendment-inprogress");
                common.insertForm("contractForm", rowContractUpdate);
            }

            ProcessReject abortProcess = new ProcessReject();

            abortProcess.rejectProcess(orignalContractId);
            rowContractAmend.setProperty("parent_contract_id", key);

            FormRowSet rowSetBoq = common.getFormRows("boqForm", "contract_id", orignalContractId);
            for (FormRow rowBoq : rowSetBoq) {
                rowBoq.setId(common.getUUID());
                rowBoq.setProperty("contract_id", key);
                common.insertForm("boqForm", rowBoq);
            }

            FormRowSet rowContractFlow = common.getFormRows("approvalFlowContractForm", "contract_id", orignalContractId);
            for (FormRow row : rowContractFlow) {
                row.setId(common.getUUID());
                row.setProperty("contract_id", key);
                common.insertForm("approvalFlowContractForm", row);
            }

            FormRowSet rowClaimContractFlow = common.getFormRows("approvalFlowClaimContractForm",
                    "contract_id", orignalContractId);
            for (FormRow row : rowClaimContractFlow) {
                row.setId(common.getUUID());
                row.setProperty("contract_id", key);
                common.insertForm("approvalFlowClaimContractForm", row);
            }

            FormRowSet rowVOContractFlow = common.getFormRows("approvalFlowVOContractForm", "contract_id", orignalContractId);
            for (FormRow row : rowVOContractFlow) {
                row.setId(common.getUUID());
                row.setProperty("contract_id", key);
                common.insertForm("approvalFlowVOContractForm", row);
            }
            rowSet.clear();
            rowSet.add(rowContractAmend);

            common.insertForm("contractForm", rowContractAmend);
        }

        FormRowSet rows = super.store(element, rowSet, formData);

        return rows;
    }

    @Override
    public String getName() {
        return ("TEST Store Binder");
    }

    @Override
    public String getVersion() {
        return ("1.0.1");
    }

    @Override
    public String getDescription() {
        return ("Test Store Binder");
    }

    @Override
    public String getLabel() {
        return ("Test Store Binder");
    }

    @Override
    public String getClassName() {
        return getClass().getName();
    }
}
