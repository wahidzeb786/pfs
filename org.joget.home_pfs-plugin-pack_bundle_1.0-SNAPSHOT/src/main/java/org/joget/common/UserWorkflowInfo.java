/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

/**
 *
 * @author Jahan Zaib
 */
public class UserWorkflowInfo {

    public UserWorkflowInfo(String username, String role, String duration) {
        this.username = username;
        this.role = role;
        this.duration = duration;
    }

    private String username;
    private String role;
    private String duration;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        if (role == null) {
            return "";
        }
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "UserWorkflowInfo{" + "username=" + username + ", role=" + role + ", duration=" + duration + '}';
    }

}
