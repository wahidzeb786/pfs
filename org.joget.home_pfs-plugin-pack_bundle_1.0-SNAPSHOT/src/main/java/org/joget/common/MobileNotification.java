/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jahan Zaib
 */
public class MobileNotification {

    final static private String FCM_URL = "https://fcm.googleapis.com/fcm/send";

    /**
     *
     * Method to send push notification to Android FireBased Cloud messaging
     * Server.
     *
     * @param tokenId Generated and provided from Android Client Developer
     * @param server_key Key which is Generated in FCM Server
     * @param message which contains actual information.
     *
     */
    public void sendFCMNotification2(String tokenId, String serverKey,
            String subject, String message) {
        try {

            // 1. URL
            URL url = new URL("https://fcm.googleapis.com/fcm/send");

            // 2. Open connection
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // 3. Specify POST method
            conn.setRequestMethod("POST");

            // 4. Set the headers
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + serverKey);

            //conn.setRequestProperty("Authorization", "key=" + apiKey);
            conn.setDoOutput(true);

            // 5. Add JSON data into POST request body 
            //`5.1 Use Jackson object mapper to convert Contnet object into JSON
            // 5.2 Get connection output stream
            JSONObject dataPayload = new JSONObject();
            JSONObject dataPayloadObject = new JSONObject();

            dataPayloadObject.put("to", tokenId);
            dataPayloadObject.put("content_available", false);
            dataPayload.put("title", subject);
            dataPayload.put("body", message);
            dataPayload.put("apns_code", "test");
            dataPayloadObject.put("priority", "high");
            dataPayload.put("ticket_payload", "");

            dataPayloadObject.put("data", dataPayload);
            System.out.println(" --> " + serverKey);

            System.out.println(" --> " + dataPayloadObject);
            // 5.3 Copy Content "JSON" into
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                // 5.3 Copy Content "JSON" into
                wr.write(dataPayloadObject.toString().getBytes());
                // 5.4 Send the request
                wr.flush();
                // 5.5 close
            }

            // 6. Get the response
            int responseCode = conn.getResponseCode();

            StringBuilder response;
            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()))) {
                String inputLine;
                response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }

            // 7. Print result
            System.out.println(response.toString());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException ex) {
            Logger.getLogger(MobileNotification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendFCMNotification(String tokenId, String serverKey, String subject,
            String message, String type, String urlLink, String projectId,
            String username, String orgId, String deviceType) {

        try {
// Create URL instance.
            URL url = new URL(FCM_URL);
// create connection.
            HttpURLConnection conn;
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
//set method as POST or GET
            conn.setRequestMethod("POST");
//pass FCM server key
            conn.setRequestProperty("Authorization", "key=" + serverKey);
//Specify Message Format
            conn.setRequestProperty("Content-Type", "application/json");
//Create JSON Object & pass value
            JSONObject infoJson = new JSONObject();

            infoJson.put("title", subject);
            infoJson.put("body", message);
            infoJson.put("type", type);
            infoJson.put("url", urlLink);
            infoJson.put("sound", "default");
            infoJson.put("username", username);
            infoJson.put("projectId", projectId);
            infoJson.put("orgId", orgId);

            JSONObject json = new JSONObject();
            json.put("to", tokenId.trim());

            if ("ios".equalsIgnoreCase(deviceType)) {
                json.put("notification", infoJson);
            }

            json.put("username", username);
            json.put("projectId", projectId);
            json.put("orgId", orgId);

            json.put("data", infoJson);
            json.put("sound", "default");

            System.out.println("json :" + json.toString());
            System.out.println("infoJson :" + infoJson.toString());
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(json.toString());
            wr.flush();
            int status = 0;
            if (null != conn) {
                status = conn.getResponseCode();
            }
            System.out.println("Status" + status);
            if (status != 0) {

                if (status == 200) {
//SUCCESS message
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    System.out.println("Android Notification Response : " + reader.readLine());
                } else if (status == 401) {
//client side error
                    System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
                } else if (status == 501) {
//server side error
                    System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
                } else if (status == 503) {
//server side error
                    System.out.println("Notification Response : FCM Service is Unavailable TokenId : " + tokenId);
                }
            }
        } catch (MalformedURLException mlfexception) {
// Prototcal Error
            System.out.println("Error occurred while sending push Notification!.." + mlfexception.getMessage());
        } catch (IOException | JSONException mlfexception) {
//URL problem
            System.out.println("Reading URL, Error occurred while sending push Notification!.." + mlfexception.getMessage());
        }

    }

    public void sendFCMNotificationMulti(List<String> putIds2, String tokenId,
            String server_key, String message) {
        try {
            // Create URL instance.
            URL url = new URL(FCM_URL);
            // create connection.
            HttpURLConnection conn;
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            //set method as POST or GET
            conn.setRequestMethod("POST");
            //pass FCM server key
            conn.setRequestProperty("Authorization", "key=" + server_key);
            //Specify Message Format
            conn.setRequestProperty("Content-Type", "application/json");
            //Create JSON Object & pass value

            JSONArray regId = null;
            JSONObject objData = null;
            JSONObject data = null;
            JSONObject notif = null;

            regId = new JSONArray();
            for (int i = 0; i < putIds2.size(); i++) {
                regId.put(putIds2.get(i));
            }
            data = new JSONObject();
            data.put("message", message);
            notif = new JSONObject();
            notif.put("title", "Alankit Universe");
            notif.put("text", message);

            objData = new JSONObject();
            objData.put("registration_ids", regId);
            objData.put("data", data);
            objData.put("notification", notif);
            System.out.println("!_@rj@_group_PASS:>" + objData.toString());

            System.out.println("json :" + objData.toString());
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(objData.toString());
            wr.flush();
            int status = 0;
            if (null != conn) {
                status = conn.getResponseCode();
            }
            if (status != 0) {

                if (status == 200) {
                    //SUCCESS message
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    System.out.println("Android Notification Response : " + reader.readLine());
                } else if (status == 401) {
                    //client side error
                    System.out.println("Notification Response : TokenId : " + tokenId + "       Error occurred :");
                } else if (status == 501) {
                    //server side error
                    System.out.println("Notification Response : [ errorCode=ServerError ]     TokenId : " + tokenId);
                } else if (status == 503) {
                    //server side error
                    System.out.println("Notification Response : FCM Service is Unavailable TokenId : " + tokenId);
                }
            }
        } catch (MalformedURLException mlfexception) {
            // Prototcal Error
            System.out.println("Error occurred while sending push Notification!.." + mlfexception.getMessage());
        } catch (IOException mlfexception) {
            //URL problem
            System.out.println("Reading URL, Error occurred while sending push   Notification!.." + mlfexception.getMessage());
        } catch (Exception exception) {
            //General Error or exception.
            System.out.println("Error occurred while sending push Notification!.." + exception.getMessage());
        }

    }
}
