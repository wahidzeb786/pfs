/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

import java.util.HashMap;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;

/**
 *
 * @author Jahan Zaib
 */
public class TestCode {

    public void doa(FormRowSet rows) {

        HashMap<String, Double> map = new HashMap<String, Double> ();

        for (FormRow row : rows) {
            String fundingAmount = row.getProperty("funding_amount");
            String budgetAmount = row.getProperty("budget_amount");
            String budgetCategory = row.getProperty("budget_category");
            if (!map.containsKey(budgetCategory)) {
                double remaingingAmount = Double.parseDouble(fundingAmount.replace(",", "")) - Double.parseDouble(budgetAmount);
                map.put(budgetCategory, remaingingAmount);
                if (remaingingAmount < 0) {
                   // formData.addFormError(id, "Funding Source: " + budgetCategory + " has only allowed value upto " + fundingAmount);
                }
            } else {
                double remaingingAmount = map.get(budgetCategory) - Double.parseDouble(budgetAmount);
                map.put(budgetCategory, remaingingAmount);
                if (remaingingAmount < 0) {
                    //formData.addFormError(id, "Funding Source: " + budgetCategory + " has only allowed value upto " + fundingAmount);
                }
            }
        }
    }
}
