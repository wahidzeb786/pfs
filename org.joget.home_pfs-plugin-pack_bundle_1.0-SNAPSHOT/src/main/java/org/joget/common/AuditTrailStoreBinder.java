/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.joget.common;

/**
 *
 * @author JAHANZAIB
 */
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joget.apps.form.lib.WorkflowFormBinder;
import org.joget.apps.form.model.Element;
import org.joget.apps.form.model.FormData;
import org.joget.apps.form.model.FormRow;
import org.joget.apps.form.model.FormRowSet;
import org.joget.common.Commons;
import org.joget.commons.util.UuidGenerator;

/**
 *
 * @author Jahan Zaib
 */
public class AuditTrailStoreBinder extends WorkflowFormBinder {

    @Override
    public FormRowSet store(Element element, FormRowSet rowSet, FormData formData) {

        FormRowSet rows = super.store(element, rowSet, formData);

        if (!rows.isMultiRow()) {
            FormRow row = rows.get(0);
            String id = row.getId();

            Logger.getLogger(this.getClass().getName()).log(Level.INFO, " -- ID : " + id);
            String userId = row.getProperty("username");
            String remarks = "";
            if (row.containsKey("remarks")) {
                remarks = row.getProperty("remarks");
            } else {
                remarks = "";
            }
            String userName = row.getProperty("fullname");
            String status = row.getProperty("status");

            String submitType = "";
            if (row.containsKey("form_submit_type")) {
                submitType = row.getProperty("form_submit_type");
            } else {
                submitType = "";
            }

            Logger.getLogger(this.getClass().getName()).log(Level.INFO, " userId " + userId);
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, " remarks " + remarks);
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, " userName " + userName);
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, " status " + status);

            UuidGenerator uuid = UuidGenerator.getInstance();
            String key = uuid.getUuid();

            Commons common = new Commons();
            common.insertAuditForm(key, id, userId, remarks, userName, status, submitType);
        }

        return rows;
    }

    @Override
    public String getName() {
        return ("Form Audit Trail Store Binder");
    }

    @Override
    public String getVersion() {
        return ("1.0.1");
    }

    @Override
    public String getDescription() {
        return ("Form Audit Trail Store Binder");
    }

    @Override
    public String getLabel() {
        return ("Form Audit Trail Store Binder");
    }

    @Override
    public String getClassName() {
        return getClass().getName();
    }
}
