package com.pfs;

import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.HttpMethod;
import com.microsoft.azure.functions.HttpRequestMessage;
import com.microsoft.azure.functions.HttpResponseMessage;
import com.microsoft.azure.functions.HttpStatus;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;

import java.util.Optional;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Azure Functions with HTTP Trigger.
 */
public class Function {

    /**
     * This function listens at endpoint "/api/HttpExample". Two ways to invoke
     * it using "curl" command in bash: 1. curl -d "HTTP Body" {your
     * host}/api/HttpExample 2. curl "{your
     * host}/api/HttpExample?name=HTTP%20Query"
     */
    @FunctionName("HttpPFSFunction")
    public HttpResponseMessage run(
            @HttpTrigger(
                    name = "req",
                    methods = {HttpMethod.GET, HttpMethod.POST},
                    authLevel = AuthorizationLevel.ANONYMOUS) HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context) throws JSONException {
        context.getLogger().info("Java HTTP trigger processed a request.");

        // Parse query parameter
        final String query = request.getQueryParameters().get("name");
        final String name = request.getBody().orElse(query);

        if (name == null) {
            return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("Please pass a name on the query string or in the request body").build();
        } else {
            JSONObject responseObject = new JSONObject();
            if ("pfs".equals(name)) {
                responseObject.put("url", "https://jogetx.reveronconsulting.com/jw");
                //return request.createResponseBuilder(HttpStatus.OK).body("{\"url\":\"https://jogetx.reveronconsulting.com/jw\"}").build();
            } else if ("reveron".equals(name)) {
                responseObject.put("url", "https://joget.reveronconsulting.com/jw");
                //return request.createResponseBuilder(HttpStatus.OK).body("{\"url\":\"https://joget.reveronconsulting.com/jw\"}").build();
            } else if ("jogetk".equals(name)) {
                responseObject.put("url", "https://jogetk.reveronconsulting.com/jw");
                //return request.createResponseBuilder(HttpStatus.OK).body("{\"url\":\"https://joget.reveronconsulting.com/jw\"}").build();
            }else {
                responseObject.put("url", "-");
                //return request.createResponseBuilder(HttpStatus.OK).body("{\"url\":\"-\"}").build();
            }
            return request.createResponseBuilder(HttpStatus.OK).body(responseObject.toString()).build();
        }
    }
}
